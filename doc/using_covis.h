/** \page using_covis Using \covis in an external project
 * \brief This page describes how to use \covis from an external project.
 * 
 * The following guide assumes that you have compiled \covis by following this page: \ref installation.
 *
 * \section from_the_command_line From the command-line
 * \covis is linked into a single library <b>covis</b>.
 * On a typical linux platform with GCC,
 * include headers and this library are installed under <b>/usr/local</b>.
 * Therefore, it is possible to compile your source file using simply:
\verbatim
g++ -lcovis -o my_executable my_executable.cpp
\endverbatim
 * If you want to link to \covis in a location not included in the standard
 * compiler search paths (e.g. from the build location),
 * use the following command:
\verbatim
g++ -I/path/to/covis/src -L/path/to/covis/build/lib -lcovis -o my_executable my_executable.cpp
\endverbatim
 * 
 * \note Since \covis has many external dependencies (OpenCV, Eigen etc.),
 * you may have to explicitly add include/linker flags for these libraries to
 * the command-line. Alternatively, use CMake as described below, which will
 * link to all necessary libraries.
 * 
 * 
 * \section using_cmake Using CMake
 * In order to use \covis in an external CMake project, you first need to build
 * \covis using (see \ref installation). If you installed \covis using <b>make install</b>,
 * you can immediately use it in another project as follows:
\code{.cmake}
find_package(COVIS)

add_definitions(${COVIS_DEFINITIONS})
include_directories(${COVIS_INCLUDE_DIRS})

find_package(VTK REQUIRED)
include(${VTK_USE_FILE})

add_executable(my_executable my_executable.cpp)
target_link_libraries(my_executable ${COVIS_LIBRARIES})
\endcode
 * 
 * 
 * \subsection using_build_libraries Using build libraries
 * It is also possible to link to \covis without installing it. For this,
 * the build system provides a config script which refers to the locations
 * of the compiled libraries in the source tree.
 * Assume your source tree is in
 * \b /path/to/covis, and you have made an out of source build inside
 * \b /path/to/covis/build, then you can hint the CMake find statement as follows:
\code{.cmake}
find_package(COVIS HINTS /path/to/covis/build)
\endcode
*/
