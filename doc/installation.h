/** \page installation Installation
 * \brief This page describes how to build and install \covis
 *
 * When you have successfully followed this guide, you may go to this page: \ref using_covis.
 *
 * \tableofcontents
 *
 *
 * \section dependencies Dependencies
 * In order to download and configure \covis,
 * these are the <b>required applications</b>:
 *  - Git (http://git-scm.com)
 *  - CMake >= 2.8 (http://cmake.org)
 *
 * \covis can then be compiled with a C++-compliant <b>compiler</b>. We have tested the
 * compilation on Ubuntu Linux with the following compilers:
 *  - GCC >= 4.7 (http://gcc.gnu.org)
 *  - clang >= 3 (http://clang.llvm.org)
 *
 * \covis depends on a number of third-party libraries. The following
 * are <b>mandatory libraries</b>:
 *  - OpenCV >= 2.3 (http://opencv.org)
 *  - Eigen >= 3 (http://eigen.tuxfamily.org)
 *  - Boost >= 1.46 (http://www.boost.org)
 *  - PCL >= 1.7 (http://pointclouds.org)
 *  - VTK (http://www.vtk.org)
 *
 * The following are <b>optional libraries</b>:
 *  - CUDA (http://nvidida.com/cuda)
 *  - OpenMP (http://openmp.org)
 *
 * Additionally, the following <b>optional applications</b>
 * may come in handy (see \ref coding_guidelines):
 *  - Doxygen (http://www.doxygen.org)
 *  - Astyle (http://astyle.sourceforge.net)
 *
 *
 * \section installing_pcl Installing PCL
 * Although it is possible to install PCL from the PPA, at the time of writing it is preferable to install PCL from
 * source, since the PPA is not up to date. Open a terminal, cd to a folder of your choice
 * and download from source for the PCL 1.8 release:
\verbatim
git clone --branch pcl-1.8.0 https://github.com/PointCloudLibrary/pcl.git
\endverbatim
 * Now go to the PCL folder, create a build subfolder, configure and build in Release mode:
\verbatim
cd pcl
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
\endverbatim
 * At this point, you can optionally install PCL using <b>make install</b>.
 *
 *
 * \section installing_covis Installing \covis
 * \covis is also located on a Git server.
 * Use the following command in a terminal to clone \covis to your local file system:
\verbatim
git clone https://gitlab.com/caro-sdu/covis.git
\endverbatim
 *
 * Similar to PCL, the recommended approach is
 * to create an out of source build, e.g. by creating a subfolder called
 * \b build inside the \covis source tree and run the configuration from there.
 * First we create the build folder inside the source tree:
\verbatim
cd covis
mkdir build
cd build
\endverbatim
 *
 * If you followed the guide above to compile PCL from source, and chose not to install PCL using <b>make install</b>,
 * you now need to direct CMake to the location where you built PCL:
\verbatim
cmake -DPCL_DIR=/path/to/pcl/build ..
\endverbatim
 * where you replace \b /path/to with the appropriate path.
 *
 * Conversely, if you installed PCL from the PPA, or if you used <b>make install</b>,
 * CMake should be able to automatically detect PCL without the extra command line argument:
\verbatim
cmake ..
\endverbatim
 *
 * Finally, you are ready to build the main \covis library file:
\verbatim
make covis
\endverbatim
 *

 * 
 * 
 * \subsection build_flags Build flags
 * \covis automatically adds required flags to the build.
 * These are stored in the advanced CMake cache variable <b>COVIS_DEFINITIONS</b>.
 * If you want to add your own flags to the build, add them to the cache variable
 * <b>COVIS_DEFINITIONS_EXTRA</b>, e.g. from the command-line:
\verbatim
cmake -DCOVIS_DEFINITIONS_EXTRA="-ansi;-pedantic-errors" ..
\endverbatim
 *
 *
 * \subsection static_build Static build
 * You can do a static build by changing the CMake command as follows:
\verbatim
cmake -DCOVIS_BUILD_STATIC=ON ..
\endverbatim
 *
 *
 * \subsection generating_documentation Generating documentation
 * To generate the documentation (requires Doxygen, see \ref documentation), run
 * the following:
\verbatim
make doc
\endverbatim
 * The documentation will appear inside \b build/doc with the index file
 * \b build/doc/html/index.html.
 *
 *
 * \subsection source_code_styling Source code styling
 * To apply formatting to source files (requires Astyle, see
 * \ref automatic_styling), use:
\verbatim
make style
\endverbatim
 *
 *
 * \subsection library_installation Library installation
 * If you want to install the \covis libraries, just run (as super user):
\verbatim
make install
\endverbatim
 * The root path of the installation can be changed in the CMake variable
 * \b CMAKE_INSTALL_PREFIX.
 * 
 * The \b install target installs all headers and libraries into the \b include
 * and \b lib (\b bin for DLLs) subdirectories of the \b CMAKE_INSTALL_PREFIX
 * path. Additionally, a find script \b FindCOVIS.cmake is installed into the
 * CMake module path, making it possible to use your \covis installation from
 * another project (see \ref using_covis). Finally, if documentation was
 * generated, this will appear inside the subdirectory \b share/doc/covis of
 * \b CMAKE_INSTALL_PREFIX.
*/
