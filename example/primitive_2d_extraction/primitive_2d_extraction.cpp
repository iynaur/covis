// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "covis/core/texlet_2d.h"
#include "covis/feature/texlet_2d_extraction.h"
#include "covis/core/line_segment_2d.h"
#include "covis/feature/line_segment_2d_extraction.h"
#include "covis/core/program_options.h"

//OpenCV
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//PCL
#include <pcl/console/print.h>

using namespace covis;

int main(int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("imgfile", "image file");
    po.addOption("id0", 0.0, "id0");
    po.addOption("id1", 0.6, "id1");
    po.addOption("id2", 0.3, "id2");

    // Parse
    if(!po.parse(argc, argv))
        return 1;

    const double id0 = po.getValue<double>("id0");
    const double id1 = po.getValue<double>("id1");
    const double id2 = po.getValue<double>("id2");

    // Load image
    cv::Mat img = cv::imread(po.getValue("imgfile"));
    COVIS_ASSERT_MSG(!img.empty(), "Empty input image!");

    // output images
    cv::Mat_<cv::Vec3b> texletOutput = cv::Mat_<cv::Vec3b>::zeros(img.size());
    cv::Mat_<cv::Vec3b> lineSegmentOutput = cv::Mat_<cv::Vec3b>::zeros(img.size());

    // extract texlets
    feature::Texlet2DExtraction t2d(core::Ids(id0, id1, id2));
    std::vector<core::Texlet2D> tex = t2d.extract2DTexlets(img);

    // display extracted texlets
    for (std::vector<core::Texlet2D>::iterator it = tex.begin() ; it != tex.end(); ++it)
    {
        core::Texlet2D t = *it;
        cv::Point xy = cv::Point(t.x, t.y);
        texletOutput[xy.y][xy.x] = cv::Vec3b(t.r, t.g, t.b);
    }
    pcl::console::print_info("done with texlets\n");
    //extract line segments
    feature::LineSegment2DExtraction ls2d(core::Ids(id0, id1, id2));
    std::vector<core::LineSegment2D> ls = ls2d.extract2DLineSegments(img);

    //dispaly extracted line segments
    for (std::vector<core::LineSegment2D>::iterator it = ls.begin() ; it != ls.end(); ++it)
    {
        core::LineSegment2D t = *it;
        cv::Point xy = cv::Point(t.x, t.y);
        lineSegmentOutput[xy.y][xy.x] = cv::Vec3b(t.r, t.g, t.b);
    }
    pcl::console::print_info("done with line segments\n");
    pcl::console::print_value("amount of texlets: %d \n", tex.size());
    pcl::console::print_value("amount of line segments: %d \n", ls.size());

    cv::imshow("line segments", lineSegmentOutput);
    cv::imshow("texlets", texletOutput);





    cv::waitKey();

    return 0;
}
