Pose voting and clustering
==========================

This example contains example code for the pose voting and clustering method described in the following work:
<br><br>
Buch, A.G., Kiforenko, L. and Kraft, D. "Rotational Subgroup Voting and Pose Clustering for Robust 3D Object Recognition." *ICCV'17*.
<br><br>
First, make sure you compiled this example by first following the general covis compilation guidelines [here](../../README.md). Then do the following from the build folder:
```sh
make example_pose_voting
```

You can now get an example object model [here](http://staffhome.ecm.uwa.edu.au/~00053650/recogData/models/chef/chef_High.zip) and a scene model [here](http://staffhome.ecm.uwa.edu.au/~00053650/recogData/scenes/rs1.zip) - both are from the well-known UWA object recognition dataset also addressed in the paper above. Remember to unpack the files.
<br><br>
You should now be able to run the example, setting appropriate parameters, as follows:
```sh
bin/example_pose_voting /path/to/cheff.ply /path/to/rs1.ply --resolution=1 --visualize
```
