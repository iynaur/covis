// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
using namespace covis::core;
using namespace covis::feature;

#include <pcl/features/normal_3d_omp.h>
#include <pcl/filters/filter.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>

/*
 * Main entry point
 */
int main(int argc, const char** argv) {
    // Setup program options
    ProgramOptions po;
    po.addPositional("pcdfile", "point cloud file");
    po.addOption("radius-normal", 0.01, "normal estimation radius");
    po.addOption("knn-correction", 10, "normal correction k-NN value");
    po.addOption("level", 10, "show every level'th normal only");
    po.addOption("length", 0.01, "normal vector lengths");
    
    // Parse
    if(!po.parse(argc, argv))
        return 1;
    
    const float nrad = po.getValue<float>("radius-normal");
    const size_t knn = po.getValue<size_t>("knn-correction");
    const size_t level = po.getValue<size_t>("level");
    const float length = po.getValue<float>("length");
    
    // Load point cloud
    pcl::PointCloud<pcl::PointNormal> cloud;
    COVIS_ASSERT(pcl::io::loadPCDFile<pcl::PointNormal>(po.getValue("pcdfile"), cloud) == 0);
    std::vector<int> dummy;
    pcl::removeNaNFromPointCloud(cloud, cloud, dummy);
    COVIS_ASSERT(!cloud.empty());
    
    {
        // Compute normals
        ScopedTimer t("Normals");
        pcl::NormalEstimationOMP<pcl::PointNormal, pcl::PointNormal> ne;
        ne.setInputCloud(cloud.makeShared());
        ne.setRadiusSearch(nrad);
        ne.compute(cloud);
    }
    
    // Take a copy for correction
    pcl::PointCloud<pcl::PointNormal> copy = cloud;
    {
        // Perform normal correction
        ScopedTimer t("Normal correction");
        NormalCorrectionManifold<pcl::PointNormal> ncm;
        ncm.setK(knn);
        ncm.compute(copy);
    }
    
    // Setup a point cloud visualizer
    pcl::visualization::PCLVisualizer visuCloud("Input normals");
    pcl::visualization::PCLVisualizer visuCopy("Corrected normals");
    visuCloud.addPointCloud<pcl::PointNormal>(cloud.makeShared(),
            pcl::visualization::PointCloudColorHandlerRandom<pcl::PointNormal>(cloud.makeShared()));
    visuCopy.addPointCloud<pcl::PointNormal>(copy.makeShared(),
            pcl::visualization::PointCloudColorHandlerRandom<pcl::PointNormal>(copy.makeShared()));
    visuCloud.addPointCloudNormals<pcl::PointNormal>(cloud.makeShared(), level, length, "normals");
    visuCopy.addPointCloudNormals<pcl::PointNormal>(copy.makeShared(), level, length, "normals");
    
    // Start
    while(!visuCloud.wasStopped() && !visuCopy.wasStopped()) {
        visuCloud.spinOnce();
        visuCopy.spinOnce();
    }
    
    return 0;
}
