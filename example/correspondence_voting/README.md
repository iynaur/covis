Correspondence voting
=====================

This example contains example code for the correspondence voting method documented in the following paper:
<br><br>
Buch, A.G., Yang, Y., Krüger, N. and Petersen, H.G. "In search of inliers: 3d correspondence by local and global voting." *CVPR'14*.
<br><br>
First, make sure you compiled this example by first following the general covis compilation guidelines 
[here](../../README.md). Then do the following from the build folder:
```sh
make example_correspondence_voting
```

You can now get an example object model [here](http://staffhome.ecm.uwa.edu.au/~00053650/recogData/models/chef/chef_High.zip) and a scene model [here](http://staffhome.ecm.uwa.edu.au/~00053650/recogData/scenes/rs1.zip) - both are from the well-known UWA object recognition dataset also addressed in the paper above. Remember to unpack the files.
<br><br>
You should now be able to run the example, setting appropriate parameters, as follows:
```sh
/path/to/example_correspondence_voting /path/to/cheff.ply /path/to/rs1.ply --resolution=2 --radius-normal=10 --radius-feature=15
```
