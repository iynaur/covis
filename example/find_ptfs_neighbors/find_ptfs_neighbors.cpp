// Copyright (c) 2016, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

//Own
#include "covis/core/ptf.h"
#include "covis/core/program_options.h"
#include "covis/core/correspondence.h"
#include "covis/core/progress_display.h"
#include "covis/detect/detect_base.h"
#include "covis/detect/feature_search.h"

//PCL
#include <pcl/io/pcd_io.h>
#include <pcl/search/impl/flann_search.hpp>


std::string findshortnameforTarget ( std::string path ) {
    std::string object = path;
    const size_t ii = object.find_last_of ( "/" );
    if ( ii != std::string::npos ) {
        object = ( object.substr ( ii+1 ).c_str() );
    }

    object = object.substr ( 0, object.size()-4 ).c_str();
    return object;
}

typedef covis::core::PTF PointN;

// --------------------------------------------------------------------------------------------------------------
int main(int argc, const char **argv) {

    /// Find neighbors for each object feature in object file
    /// The result is saved as {object}_neigh.txt

    covis::core::ProgramOptions po;
    po.addPositional("object", "object point cloud, the result is saved as {object}_neigh.txt");
    po.addPositional("radius", "radius for neighbors search");

    // Parse
    if (!po.parse(argc, argv))
        return 1;

    pcl::PointCloud<PointN>::Ptr object_features(new pcl::PointCloud<PointN>());
    pcl::io::loadPCDFile<PointN>(po.getValue("object"), *object_features);
    COVIS_ASSERT(object_features && !object_features->empty());

    std::ofstream file_out;
    std::stringstream name;
    name << findshortnameforTarget(po.getValue("object")) << "_neigh.txt";
    file_out.open(name.str().c_str());

    COVIS_MSG_INFO("Used radius: " << po.getValue<float>("radius"));

    covis::detect::FeatureSearch<covis::core::PTF> fs(1);

    fs.setChecks(512);
    fs.setTarget(object_features);
    COVIS_MSG_INFO("File loaded, using 512 checks for kd-tree");
    covis::core::ProgressDisplay::Ptr pd;
    pd.reset(new covis::core::ProgressDisplay(object_features->size()));

    for (size_t i = 0; i < object_features->size(); i++)
    {
        PointN tripod = (*object_features)[i];
                covis::core::Correspondence foundCorrespondences = fs.radius(tripod, po.getValue<float>("radius"));
        int found_size = foundCorrespondences.size();
        found_size = found_size - 1;
        ++(*pd);

        file_out << found_size << "\n";
        file_out.flush();
    }

    COVIS_MSG_WARN("Output is saved as: " << name.str());

    file_out.close();


    return 0;
}
