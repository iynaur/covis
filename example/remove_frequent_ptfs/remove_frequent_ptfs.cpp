// Copyright (c) 2016, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

//Own
#include "covis/core/ptf.h"
#include "covis/core/program_options.h"
#include "covis/core/progress_display.h"
#include "covis/core/range.h"

//PCL
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>

//STD
#include <fstream>

typedef covis::core::PTF PointN;
typedef covis::core::PTF_Index PointNIndex;


bool checkIfContains(pcl::PointCloud<pcl::PointXYZ> cloud, pcl::PointXYZ p) {
    for (size_t i = 0; i < cloud.size(); i++) {
        pcl::PointXYZ cloud_p = (cloud)[i];
        if (cloud_p.x == p.x && cloud_p.y == p.y && cloud_p.z == p.z)
            return true;
    }
    return false;
}

std::string findshortnameforTarget(std::string path) {
    std::string object = path;
    const size_t ii = object.find_last_of("/");
    if (ii != std::string::npos) {
        object = (object.substr(ii + 1).c_str());
    }

    object = object.substr(0, object.size() - 4).c_str();
    return object;
}

// --------------------------------------------------------------------------------------------------------------
int main(int argc, const char **argv) {

    covis::core::ProgramOptions po;
    po.addPositional("object", "query point cloud");
    po.addPositional("object-idx", "files with object idx's");
    po.addPositional("neigh", "files with neighborhs");
    po.addPositional("points", "amount of less frequent features");

    po.addOption("cloud", "", "path to the cloud");

    po.addFlag('n', "useAmountOfNeight", "enable useAmountOfNeight instead of procentage");

    // Parse
    if (!po.parse(argc, argv))
        return 1;

    bool useAmountOfNeight = po.getFlag("useAmountOfNeight");


    pcl::PointCloud<PointN>::Ptr object_features(new pcl::PointCloud<PointN>());
    pcl::io::loadPCDFile<PointN>(po.getValue("object"), *object_features);
    COVIS_ASSERT(object_features && !object_features->empty());

    COVIS_MSG_INFO("Object read - line size: " << object_features->size());

    pcl::PointCloud<PointNIndex>::Ptr object_features_idx(new pcl::PointCloud<PointNIndex>());
    pcl::io::loadPCDFile<PointNIndex>(po.getValue("object-idx"), *object_features_idx);
    COVIS_ASSERT(object_features_idx && !object_features_idx->empty());

    COVIS_MSG_INFO("Object idx read - line size: " << object_features_idx->size());

    //read neigh file
    std::vector<int> amount_of_neig;
    std::string fileName = po.getValue("neigh");
    std::ifstream infile(fileName.c_str(), std::ifstream::in);
    std::string line;
    int line_number = 0;
    while (std::getline(infile, line)) {
        std::istringstream iss(line);
        int n;
        std::vector<int> v;

        while (iss >> n) {
            v.push_back(n);
        }
        line_number++;
        amount_of_neig.push_back(v[0]);
        // do something useful with v
    }
    std::cout << "Amount of line is neigh file: " << line_number << std::endl;
    std::cout << "Amount of neigh: " << amount_of_neig.size() << std::endl;
    infile.close();

    //sort neighborhs
    COVIS_MSG_INFO("-------------");
    std::vector<size_t> order = covis::core::sort<int>(amount_of_neig, true);

    //save output
    pcl::PointCloud<PointN> object_features_out;
    pcl::PointCloud<PointNIndex> object_features_idx_out;

    if (useAmountOfNeight) {
        int thr = po.getValue<int>("points");
        for (size_t i = 0; i < object_features->size(); i++) {
            if (amount_of_neig[i] <= thr) {
                PointN point = (*object_features)[order[i]];
                PointNIndex point_idx = (*object_features_idx)[order[i]];
                object_features_out.push_back(point);
                object_features_idx_out.push_back(point_idx);
            }
        }
    } else {

        size_t maxmax = (size_t) (((object_features->size()) * po.getValue<size_t>("points")) / 100);
        COVIS_MSG_INFO("We decided to keep " << maxmax << " best triplets");
        size_t i = 0;
        while (object_features_idx_out.size() < maxmax && i < order.size()) {

            PointN point = (*object_features)[order[i]];
            PointNIndex point_idx = (*object_features_idx)[order[i]];
            object_features_out.push_back(point);
            object_features_idx_out.push_back(point_idx);

            i++;

        }
        COVIS_MSG_INFO("The min amount of neigh: " << amount_of_neig[0]);
        COVIS_MSG_INFO("The max amount of neigh: " << maxmax << " - " << amount_of_neig[maxmax - 1] << " i: " << i);
    }


    std::cout << "The size of tripod out: " << object_features_out.size() << std::endl;
    std::stringstream sname, sname_idx;
    sname << findshortnameforTarget(po.getValue("object")) << "_" << po.getValue<int>("points") << ".pcd";
    sname_idx << findshortnameforTarget(po.getValue("object")) << "_" << po.getValue<int>("points") << "_index.pcd";
    pcl::io::savePCDFileBinary(sname.str(), object_features_out);
    pcl::io::savePCDFileBinary(sname_idx.str(), object_features_idx_out);
    COVIS_MSG_INFO("Scene saved as: " << sname.str() << ", " << sname_idx.str());

    return 0;
}


