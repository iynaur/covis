RANSAC and prerejective RANSAC
==============================

This example contains example code for RANSAC pose estimation as well as the prerejective RANSAC presented in the following paper:
<br><br>
Buch, A.G., Kraft, D., Kämäräinen, J.-K., Petersen, H.G. and Krüger, N. "Pose estimation using local structure-specific shape and appearance context." *ICRA'13*.
<br><br>
First, make sure you compiled this example by first following the general covis compilation guidelines 
[here](../../README.md). Then do the following from the build folder:
```sh
make example_ransac
```

You can now get an example object model [here](http://staffhome.ecm.uwa.edu.au/~00053650/recogData/models/chef/chef_High.zip) and a scene model [here](http://staffhome.ecm.uwa.edu.au/~00053650/recogData/scenes/rs1.zip) - both are from the well-known UWA object recognition dataset also addressed in the paper above. Remember to unpack the files.
<br><br>
You should now be able to run the example, setting appropriate parameters, as follows:
```sh
bin/example_ransac /path/to/cheff.ply /path/to/rs1.ply --resolution=1 --iterations=10000 --visualize
```
You might want to try introducing prerejection for speedup:
```sh
bin/example_ransac /path/to/cheff.ply /path/to/rs1.ply --resolution=1 --iterations=10000 --visualize --prerejection
```