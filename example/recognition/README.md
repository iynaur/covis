Object recognition
==================

This example contains example code for both the prerejective RANSAC method described here:
<br><br>
Buch, A.G., Kraft, D., Kämäräinen, J.-K., Petersen, H.G. and Krüger, N. "Pose estimation using local structure-specific shape and appearance context." *ICRA'13*.
<br><br>
and the pose voting and clustering method described in the following work:
<br><br>
Buch, A.G., Kiforenko, L. and Kraft, D. "Rotational Subgroup Voting and Pose Clustering for Robust 3D Object Recognition." *ICCV'17*.
<br><br>
First, make sure you compiled this example by first following the general covis compilation guidelines [here](../../README.md). Then do the following from the build folder:
```sh
make example_recognition
```
<br><br>
You can now get two example object models [here](http://staffhome.ecm.uwa.edu.au/~00053650/recogData/models/chef/chef_High.zip) and [here](http://staffhome.ecm.uwa.edu.au/~00053650/recogData/models/chicken/chicken_high.zip), and a scene model [here](http://staffhome.ecm.uwa.edu.au/~00053650/recogData/scenes/rs1.zip) - all from the well-known UWA object recognition dataset also addressed in the latest paper above. Remember to unpack the files.
<br><br>
You should now be able to run the example as follows:
```sh
bin/example_recognition /path/to/cheff.ply,/path/to/chicken_high.ply /path/to/rs1.ply --resolution-surface=1 --iterations-ransac=10000 --verbose --visualize
```
<br><br>
In the default mode, the example uses RANSAC for generating candidate detections and we needed to up the number of itereations to detect both objects. As an alternative, you can now try change to the pose voting detector like this:
```sh
bin/example_recognition /path/to/cheff.ply,/path/to/chicken_high.ply /path/to/rs1.ply --resolution-surface=1 --detector=voting --verbose --visualize
```
