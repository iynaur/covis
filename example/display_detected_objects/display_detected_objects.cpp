// Copyright (c) 2016, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

//Own
#include <covis/covis.h>

//PCL
#include "pcl/io/pcd_io.h"
#include "pcl/io/ply_io.h"
#include "pcl/io/vtk_lib_io.h"
#include "pcl/point_types.h"
#include "pcl/common/transforms.h"

typedef pcl::PointXYZRGBA PointT;
typedef pcl::PointCloud<PointT> CloudT;

pcl::visualization::PCLVisualizer visu;


inline vtkSmartPointer<vtkLODActor> getMeshActor(const std::string &label = "mesh") {
    return (*visu.getCloudActorMap())[label].actor;
}


inline void addMesh(pcl::PolygonMesh::ConstPtr mesh, const std::string &label = "mesh", int obj = 0) {
    visu.addPolygonMesh(*mesh, label);
    if (obj == -1) getMeshActor(label)->GetProperty()->SetColor(1, 1, 1);
    else if (obj == 0) getMeshActor(label)->GetProperty()->SetColor(1, 0, 0);
    else if (obj == 1) getMeshActor(label)->GetProperty()->SetColor(0, 1, 0);
    else if (obj == 2) getMeshActor(label)->GetProperty()->SetColor(0, 1, 1);
    else if (obj == 3) getMeshActor(label)->GetProperty()->SetColor(0, 0, 1);
    else if (obj == 4) getMeshActor(label)->GetProperty()->SetColor(1, 1, 0);
    else if (obj == 5) getMeshActor(label)->GetProperty()->SetColor(0.1, 1, 0.3);
    else if (obj == 6) getMeshActor(label)->GetProperty()->SetColor(0, 0.5, 0);
    else if (obj == 7) getMeshActor(label)->GetProperty()->SetColor(0.8, 0.4, 0.5);
    else if (obj == 8) getMeshActor(label)->GetProperty()->SetColor(0, 0.8, 0.2);
    else if (obj == 9) getMeshActor(label)->GetProperty()->SetColor(0.9, 0.02, 0.45);
    else if (obj == 10) getMeshActor(label)->GetProperty()->SetColor(0.5, 0.5, 0.9);
    else getMeshActor(label)->GetProperty()->SetColor(0, 0, 1);
    getMeshActor(label)->GetProperty()->SetOpacity(0.5);
}

void transformPolygonMesh(pcl::PolygonMesh::Ptr &inMesh, Eigen::Matrix4f &transform) {
    //Important part starts here
    pcl::PointCloud<pcl::PointXYZ> cloud, cloud_out;
    pcl::fromPCLPointCloud2(inMesh->cloud, cloud);
    pcl::transformPointCloud(cloud, cloud_out, transform);
    pcl::toPCLPointCloud2(cloud_out, inMesh->cloud);
}

std::vector<Eigen::Matrix4f> readManyPoses(int amount_of_poses, bool rowMajor, std::string filename){

    std::vector<Eigen::Matrix4f> matrices;

    std::ifstream ifs(filename.c_str(), std::ios::in);

    for (int i = 0; i < amount_of_poses; i++) {
        Eigen::Matrix4f m;
        for(typename Eigen::MatrixBase<Eigen::Matrix4f>::Index r = 0; r < 4; ++r) {
            for(typename Eigen::MatrixBase<Eigen::Matrix4f>::Index c = 0; c < 4; ++c) {
                if(rowMajor)
                    ifs >> m.derived()(r,c);
                else ifs >> m.derived()(c,r);

            }
        }
        std::cout << m << std::endl;
        matrices.push_back(m);
    }
    return matrices;
}


int main(int argc, const char **argv) {

    covis::core::ProgramOptions po;

    po.addPositional("object", "object");
    po.addPositional("scene", "scene");
    po.addPositional("poses", "file with poses, multiple poses should be in one file");
    po.addPositional("amount_of_poses_to_display", "amount of poses to display");

    po.addFlag('m', "meters", "meters");
    // Parse
    if (!po.parse(argc, argv))
        return 1;

    std::vector<pcl::PolygonMesh::Ptr> meshq;

    pcl::PolygonMesh::Ptr meshScene(new pcl::PolygonMesh);
    pcl::io::loadPLYFile(po.getValue("scene"), *meshScene);

    visu.setBackgroundColor(255, 255, 255);

    //read file with poses
    std::vector<Eigen::Matrix4f> m = readManyPoses(po.getValue<int>("amount_of_poses_to_display"), true, po.getValue("poses"));

    for (size_t i = 0; i < po.getValue<size_t>("amount_of_poses_to_display"); i++) {
        pcl::PolygonMesh::Ptr mesht(new pcl::PolygonMesh);
        pcl::io::loadPLYFile(po.getValue("object"), *mesht);
        std::stringstream n;
        n << po.getValue("object") << i;
        Eigen::Matrix4f pose = m[i];
        transformPolygonMesh(mesht, pose);
        addMesh(mesht, n.str(), i);
    }
    addMesh(meshScene, "scene", -1);
    visu.spin();

    return 0;
}