#include "covis_python_common.h"

#include <covis/filter/point_cloud_preprocessing.h>

using namespace boost::python;

/*
 * point_cloud_preprocessing.h
 */

PointCloud preprocessMesh(const Mesh& mesh,
                          float scale,
                          bool removeNan,
                          float far,
                          float resolution,
                          float normalRadius,
                          bool orientNormals,
                          bool concave,
                          bool verbose) {
    PointCloudT pc = *covis::filter::preprocess<PointT>(boost::make_shared<Mesh>(mesh),
                                                        scale,
                                                        removeNan,
                                                        far,
                                                        resolution,
                                                        normalRadius,
                                                        orientNormals,
                                                        concave,
                                                        verbose);
    PointCloud result;
    pcl::toPCLPointCloud2(pc, result);
    return result;
}

PointCloud preprocessCloud(const PointCloud& cloud,
                           float scale,
                           bool removeNan,
                           float far,
                           float resolution,
                           float normalRadius,
                           bool orientNormals,
                           bool concave,
                           bool verbose) {
    PointCloudT::Ptr pc(new PointCloudT);
    pcl::fromPCLPointCloud2(cloud, *pc);
    pc = covis::filter::preprocess<PointT>(pc,
                                           scale,
                                           removeNan,
                                           far,
                                           resolution,
                                           normalRadius,
                                           orientNormals,
                                           concave,
                                           verbose);
    PointCloud result;
    pcl::toPCLPointCloud2(*pc, result);
    return result;
}

PointCloud removeDominantPlane(const PointCloud& cloud, float threshold, size_t iterations, bool invert) {
    PointCloudT::Ptr pc(new PointCloudT);
    pcl::fromPCLPointCloud2(cloud, *pc);
    pc = covis::filter::removeDominantPlane<PointT>(pc, threshold, iterations, invert);

    PointCloud result;
    pcl::toPCLPointCloud2(*pc, result);
    return result;
}

PointCloud removeFarPoints(const PointCloud& cloud, float threshold) {
    PointCloudT::Ptr pc(new PointCloudT);
    pcl::fromPCLPointCloud2(cloud, *pc);
    pc = covis::filter::removeFarPoints<PointT>(pc, threshold);

    PointCloud result;
    pcl::toPCLPointCloud2(*pc, result);
    return result;
}

PointCloud downsample(const PointCloud& cloud, float resolution) {
    PointCloudT::Ptr pc(new PointCloudT);
    pcl::fromPCLPointCloud2(cloud, *pc);
    pc = covis::filter::downsample<PointT>(pc, resolution);

    PointCloud result;
    pcl::toPCLPointCloud2(*pc, result);
    return result;
}

/*
 * Module
 */
BOOST_PYTHON_MODULE(filter) {
    // Suppress numpy warning about unused function
    _import_array();
    docstring_options local_docstring_options(true, true, false);

    /*
     * point_cloud_preprocessing.h
     */
    def("preprocess", preprocessMesh,
        (arg("mesh"), arg("scale")=1, arg("removeNan")=true, arg("far")=0, arg("resolution")=0, arg("normalRadius")=0, arg("orientNormals")=false, arg("concave")=false, arg("verbose")=false));
    def("preprocess", preprocessCloud,
        (arg("cloud"), arg("scale")=1, arg("removeNan")=true, arg("far")=0, arg("resolution")=0, arg("normalRadius")=0, arg("orientNormals")=false, arg("concave")=false, arg("verbose")=false));
    def("removeDominantPlane", removeDominantPlane, (arg("cloud"), arg("threshold")=0, arg("iterations")=0, arg("invert")=false));
    def("removeFarPoints", removeFarPoints, (arg("cloud"), arg("threshold")));
    def("downsample", downsample, args("cloud", "resolution"));
}
