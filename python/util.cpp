#include "covis_python_common.h"

#include <covis/core/detection.h>
#include <covis/util/dataset_loader.h>
#include <covis/util/eigen_io.h>
#include <covis/util/loader_3d.h>

#include <pcl/io/pcd_io.h>
#include <pcl/io/vtk_lib_io.h>

using namespace boost::python;

/*
 * dataset_loader.h
 */
covis::util::DatasetLoader loadDataset(const std::string& rootPath,
                                       const std::string& objectDir,
                                       const std::string& sceneDir,
                                       const std::string& poseDir,
                                       const std::string& objectExt,
                                       const std::string& sceneExt,
                                       const std::string& poseExt,
                                       const std::string& poseSep,
                                       const std::string& objectRegex,
                                       const std::string& sceneRegex) {
    covis::util::DatasetLoader dataset(rootPath, objectDir, sceneDir, poseDir,
                                       objectExt, sceneExt, poseExt, poseSep);

    dataset.setRegexObject(objectRegex);
    dataset.setRegexScene(sceneRegex);
    dataset.parse();

    return dataset;
}

bool empty(const covis::util::DatasetLoader& dataset, size_t i) {
    return dataset.empty(i);
}

covis::util::DatasetLoader::SceneEntry at(const covis::util::DatasetLoader& dataset, size_t i) {
    return dataset.at(i);
}

list getObjects(covis::util::DatasetLoader& dataset) {
    std::vector<pcl::PolygonMesh::Ptr> objects = dataset.getObjects();
    std::vector<pcl::PolygonMesh> obj;
    for(auto o : objects)
        obj.push_back(*o);
    return toList(obj);
}

list getObjectLabels(covis::util::DatasetLoader& dataset) {
    return toList(dataset.getObjectLabels());
}

list getSceneLabels(covis::util::DatasetLoader& dataset) {
    return toList(dataset.getSceneLabels());
}

list getPoseLabels(covis::util::DatasetLoader& dataset) {
    return toList(dataset.getPoseLabels());
}

list sceneEntryObjectMask(covis::util::DatasetLoader::SceneEntry& scene) {
    return toList(scene.objectMask);
}

list sceneEntryPoses(covis::util::DatasetLoader::SceneEntry& scene) {
    return toList(scene.poses);
}

Mesh sceneEntryScene(covis::util::DatasetLoader::SceneEntry& scene) {
    return *scene.scene;
}

/*
 * eigen_io.h
 */
covis::core::Detection::MatrixT loadPose(const std::string& filename) {
    covis::core::Detection::MatrixT m;
    covis::util::loadEigen<covis::core::Detection::MatrixT>(filename, m);
    return m;
}

/*
 * loader_3d.h
 */
Mesh load(const std::string& filename) {
    Mesh result;
    COVIS_ASSERT(covis::util::load(filename, result));
    return result;
}

/*
 * pcd_io.h
 */
void save(const PointCloud& cloud, const std::string& filename, bool binary) {
    PointCloudT pc;
    pcl::fromPCLPointCloud2(cloud, pc);
    pcl::io::savePCDFile<PointT>(filename, pc, binary);
}

/*
 * vtk_lib_io.h
 */
void save(const Mesh& mesh, const std::string& filename, bool binary) {
    pcl::io::savePolygonFilePLY(filename, mesh, binary);
}

/*
 * Module
 */
BOOST_PYTHON_MODULE(util) {
    // Suppress numpy warning about unused function
    _import_array();
    docstring_options local_docstring_options(true, true, false);

    class_<covis::util::DatasetLoader>("Dataset")
            .def_readonly("size", &covis::util::DatasetLoader::size)
            .add_property("objects", getObjects)
            .add_property("objectLabels", getObjectLabels)
            .add_property("sceneLabels", getSceneLabels)
            .add_property("poseLabels", getPoseLabels)
            .def("empty", empty)
            .def("at", at)
            ;

    class_<covis::util::DatasetLoader::SceneEntry>("SceneEntry")
            .def_readonly("label", &covis::util::DatasetLoader::SceneEntry::label)
            .add_property("objectMask", sceneEntryObjectMask)
            .add_property("poses", sceneEntryPoses)
            .def_readonly("empty", &covis::util::DatasetLoader::SceneEntry::empty)
            .add_property("scene", sceneEntryScene)
            ;

    def("loadDataset", loadDataset, (arg("rootPath")=".",
                                     arg("objectDir")="",
                                     arg("sceneDir")="",
                                     arg("poseDir")="",
                                     arg("objectExt")="",
                                     arg("sceneExt")="",
                                     arg("poseExt")="",
                                     arg("poseSep")="",
                                     arg("objectRegex")="",
                                     arg("sceneRegex")="")
        );

    def("loadPose", loadPose, arg("filename"));
    def("load", load, arg("filename"));

    def("save", (void(*)(const PointCloud&, const std::string&, bool))save, (arg("cloud"),
                                                                             arg("filename"),
                                                                             arg("binary")=false));
    def("save", (void(*)(const Mesh&, const std::string&, bool))save, (arg("mesh"),
                                                                       arg("filename"),
                                                                       arg("binary")=false));
}
