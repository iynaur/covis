#include "covis_python_common.h"

using namespace boost::python;

object toNumpyArray(const Matrix& m) {
    // Python uses row-major storage order and Eigen uses column-major, this is just one way to transpose the data
    Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> mt = m;
    return toNumpyArray(m.rows(), m.cols(), const_cast<float*>(mt.data()));
}

// Inspiration here: https://stackoverflow.com/questions/10701514/how-to-return-numpy-array-from-boostpython
object toNumpyArray(npy_intp rows, npy_intp cols, float* data) {
#ifdef BOOST_NUMPY_ENABLED
    numpy::initialize();
    numpy::ndarray arr = numpy::from_data(data,
                                          numpy::dtype::get_builtin<float>(),
                                          make_tuple(rows, cols), // Dimensions
                                          make_tuple(cols*sizeof(float), sizeof(float)), // Strides
                                          object());
#else
    _import_array();
    numeric::array::set_module_and_type("numpy", "ndarray");

    npy_intp size[] = {rows, cols};

    PyObject* obj = PyArray_SimpleNewFromData(2, size, NPY_FLOAT, data);
    handle<> h(obj);
    numeric::array arr(h);
#endif

    return arr.copy();
}

Matrix fromNumpyArray(const boost::python::object& array) {
    _import_array();

    PyArrayObject* arr = (PyArrayObject*)PyArray_FromAny(array.ptr(), 0, 0, 0, 0, 0);
    const int type = PyArray_TYPE(arr);

    const int ndim = PyArray_NDIM(arr);
    COVIS_ASSERT(ndim == 1 || ndim == 2);

    npy_intp* dims = PyArray_DIMS(arr);
    const int rows = dims[0];
    const int cols = (ndim == 2 ? dims[1] : 1);
    const int dim = rows * cols;

    typedef Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> MatRowMajorT;
    MatRowMajorT result(rows, cols);
    if(type == NPY_FLOAT) {
        float* data = reinterpret_cast<float*>(PyArray_DATA(arr));
        std::copy(data, data + dim, result.data());
    } else if(type == NPY_DOUBLE) {
        double* ddata = reinterpret_cast<double*>(PyArray_DATA(arr));
        std::copy(ddata, ddata + dim, result.data());
    } else {
        COVIS_THROW("Can only take in float/double numpy arrays!");
    }

    Py_DECREF(arr);

    return result;
}
