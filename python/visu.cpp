#include "covis_python_common.h"

#include <covis/visu/corr_visu.h>
#include <covis/visu/detection_visu.h>
#include <covis/visu/visu_3d.h>

#include <boost/make_shared.hpp>
using namespace boost::python;

/*
 * corr_visu.h
 */
void showCorrespondences(const PointCloud& query,
                         const PointCloud& target,
                         list corr,
                         size_t level,
                         const std::string title,
                         bool separateModels) {
    PointCloudT::Ptr pquery(new PointCloudT);
    PointCloudT::Ptr ptarget(new PointCloudT);
    pcl::fromPCLPointCloud2<PointT>(query, *pquery);
    pcl::fromPCLPointCloud2<PointT>(target, *ptarget);

    covis::visu::CorrVisu<PointT> cv;
    cv.setQuery(pquery);
    cv.setTarget(ptarget);
    cv.setCorr(fromList<covis::core::Correspondence::Vec>(corr));
    cv.setLevel(level);
    cv.setMode(covis::visu::LINES);
    cv.setTitle(title);
    cv.setSeparate(separateModels);
    cv.show();
}

/*
 * detection_visu.h
 */
void showDetections1(const list& query, const Mesh& target, const list& detections, const std::string& title) {
    std::vector<Mesh::Ptr> queries(len(query));
    for(ssize_t i = 0; i < len(query); ++i)
        queries[i] = boost::make_shared<Mesh>(extract<Mesh>(query[i]));

    covis::core::Detection::Vec d = fromList<covis::core::Detection::Vec>(detections);
    covis::visu::showDetections(queries, boost::make_shared<Mesh>(target), d, title);
}

void showDetections2(const list& query, const PointCloud& target, const list& detections, const std::string& title) {
    std::vector<PointCloudT::Ptr> queries(len(query));
    for(ssize_t i = 0; i < len(query); ++i) {
        queries[i].reset(new PointCloudT);
        PointCloud p = extract<PointCloud>(query[i]);
        pcl::fromPCLPointCloud2(p, *queries[i]);
    }

    PointCloudT::Ptr t(new PointCloudT);
    pcl::fromPCLPointCloud2(target, *t);

    covis::core::Detection::Vec d = fromList<covis::core::Detection::Vec>(detections);
    covis::visu::showDetections<PointT>(queries, t, d, title);
}

/*
 * visu_3d.h
 */

void addPointCloud(covis::visu::Visu3D& visu, const PointCloud& cloud, const std::string& label) {
    visu.addPointCloud(boost::make_shared<PointCloud>(cloud), label);
}

void addText(covis::visu::Visu3D& visu, const std::string& text, unsigned char r, unsigned char g, unsigned char b) {
    visu.addText(text, r, g, b);
}

void setBackgroundColor(covis::visu::Visu3D& visu, unsigned char r, unsigned char g, unsigned char b) {
    visu.setBackgroundColor(r, g, b);
}

void showPointCloud(const PointCloud& cloud, const std::string& title) {
    covis::visu::showPointCloud(boost::make_shared<PointCloud>(cloud), title);
}

void showMesh(const Mesh& mesh, const std::string& title) {
    covis::visu::showMesh(boost::make_shared<Mesh>(mesh), title);
}

void showNormals(const PointCloud& cloud, size_t level, float scale, const std::string& title) {
    covis::visu::showNormals(boost::make_shared<PointCloud>(cloud), level, scale, title);
}

void showScalarField(const PointCloud& cloud, const list& field, const std::string& title) {
    const std::vector<float> vfield = fromList<std::vector<float>>(field);
    PointCloudT::Ptr pcloud(new PointCloudT);
    pcl::fromPCLPointCloud2(cloud, *pcloud);
    covis::visu::showScalarField<PointT,float>(pcloud, vfield, title);
}

void showScalarField(const PointCloud& cloud, const object& field, const std::string& title) {
    const Matrix mfield = fromNumpyArray(field);
    const std::vector<float> vfield(mfield.data(), mfield.data() + mfield.size());
    PointCloudT::Ptr pcloud(new PointCloudT);
    pcl::fromPCLPointCloud2(cloud, *pcloud);
    covis::visu::showScalarField<PointT,float>(pcloud, vfield, title);
}

/*
 * Module
 */
BOOST_PYTHON_MODULE(visu) {
    // Suppress numpy warning about unused function
    _import_array();
    docstring_options local_docstring_options(true, true, false);

    /*
    * corr_visu.h
    */
    def("showCorrespondences", showCorrespondences,
        (arg("query"), arg("target"), arg("corr"), arg("level")=1, arg("title")="Correspondence visualization", arg("separateModels")=true));

    /*
     * detection_visu.h
     */
    def("showDetections", showDetections1, (arg("query"), arg("target"), arg("detections"), arg("title")="Detections"));
    def("showDetections", showDetections2, (arg("query"), arg("target"), arg("detections"), arg("title")="Detections"));

    /*
     * visu_3d.h
     */

    class_<covis::visu::Visu3D>("Visu3D")
            .def(init<optional<const std::string&>>())
            .def("addPointCloud", addPointCloud, (arg("cloud"), arg("label")="cloud"))
            .def("addText", addText, args("text", "r", "g", "b"))
            .def("show", &covis::visu::Visu3D::show)
            ;
    def("showPointCloud", showPointCloud, (arg("cloud"), arg("title")="Point cloud visualization"));
    def("showMesh", showMesh, (arg("mesh"), arg("title")="Mesh visualization"));
    def("showNormals", showNormals, (arg("cloud"), arg("level")=1, arg("scale")=0, arg("title")="Normal visualization"));

    void (*showScalarField1)(const PointCloud&, const list&, const std::string&) = &showScalarField;
    void (*showScalarField2)(const PointCloud&, const object&, const std::string&) = &showScalarField;
    def("showScalarField", showScalarField1, (arg("cloud"), arg("field"), arg("title")="Point cloud visualization"));
    def("showScalarField", showScalarField2, (arg("cloud"), arg("field"), arg("title")="Point cloud visualization"));
}
