// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_UTIL_LOADER_3D_H
#define COVIS_UTIL_LOADER_3D_H

// Own
#include "ply_loader.h"
#include "../core/io.h"
#include "../core/macros.h"

// PCL
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PolygonMesh.h>
#include <pcl/io/auto_io.h>
#include <pcl/io/vtk_lib_io.h>

// OpenCV
#include <opencv2/highgui/highgui.hpp>

namespace covis {
    namespace util {
        /**
         * @ingroup util
         * @brief Load a file from a known 3D format (PCD, PLY, OBJ, STL, VTK, XYZ) into a polygon mesh
         * @note for non-polygon formats (PCD, XYZ), no face information is loaded
         * @param filename file path
         * @param mesh output mesh
         * @return true if loading succeeded
         */
        bool load(const std::string& filename, pcl::PolygonMesh& mesh);

        /**
         * @ingroup util
         * @brief Load a file from a known 3D format (PCD, PLY, OBJ, STL, VTK, XYZ) into a binary point cloud
         * @note For polygon formats (PLY, OBJ, STL, VTK), all face information is discarded
         * @param filename file path
         * @param cloud output point cloud
         * @return true if loading succeeded
         */
        bool load(const std::string& filename, pcl::PCLPointCloud2& cloud);

        /**
         * @ingroup util
         * @brief Load a file from a known 3D format (PCD, PLY, OBJ, STL, VTK) into a templated point cloud
         * @note For polygon formats (PLY, OBJ, STL, VTK), all face information is discarded
         * @param filename file path
         * @param cloud output point cloud
         * @tparam PointT output point cloud type
         * @return true if loading succeeded
         */
        template<typename PointT>
        inline bool load(const std::string& filename, pcl::PointCloud<PointT>& cloud) {
            const std::string ext = core::extension(filename);
            if(ext == "pcd")
                return (pcl::io::loadPCDFile<PointT>(filename, cloud) == 0); // Returns 0 on success
            else if (ext == "ply")
                return loadPLYFile<PointT>(filename, cloud);
            else if(ext == "obj") {
                pcl::PolygonMesh mesh;
                if(pcl::io::loadPolygonFileOBJ(filename, mesh) == 0) // Returns number of points loaded
                    return false;
                pcl::fromPCLPointCloud2<PointT>(mesh.cloud, cloud);
                return true;
            } else if(ext == "stl") {
                pcl::PolygonMesh mesh;
                if(pcl::io::loadPolygonFileSTL(filename, mesh) == 0) // Returns number of points loaded
                    return false;
                pcl::fromPCLPointCloud2<PointT>(mesh.cloud, cloud);
                return true;
            } else if(ext == "vtk") {
                pcl::PolygonMesh mesh;
                if(pcl::io::loadPolygonFileVTK(filename, mesh) == 0) // Returns number of points loaded
                    return false;
                pcl::fromPCLPointCloud2<PointT>(mesh.cloud, cloud);
                return true;
            } else if(ext == "xyz") {
                pcl::PCLPointCloud2 blob;
                if(!load(filename, blob))
                    return false;
                pcl::fromPCLPointCloud2<PointT>(blob, cloud);
                return true;
            } else
                COVIS_THROW("Invalid file extension for file: " << filename << "!");
        }
    }
}

#endif
