// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_UTIL_PLY_LOADER_H
#define COVIS_UTIL_PLY_LOADER_H

// STL
#include <fstream>

// PCL
#include <pcl/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PolygonMesh.h>

namespace covis {
    namespace util {
        /**
         * @class PLYLoader
         * @ingroup util
         * @brief Class for loading PLY files
         * 
         * This class loads ASCII/binary PLY files and returns them as a PCL mesh. Thus, vertex and face information
         * can be loaded using this class. The only state information maintained by this class is a verbosity flag.
         *
         * @attention Currently, this class handles the following elements (and their named properties)
         *   - vertex (x, y, z, nx, ny, nz, red, green, blue)
         *   - face (vertex_indices or vertex_index)
         *   
         * @todo Handle different endianness in the PLY file than the host system
         * 
         * @author Anders Glent Buch
         */
        class PLYLoader {
            public:
                /**
                 * Default constructor
                 */
                PLYLoader() : _verbose(false) {}
                
                /**
                 * Return the PLY file contents as a @b pcl::PolygonMesh. The @b cloud member of the polygon mesh will
                 * be filled with the vertex information and the @b polygons member will be filled with the face
                 * information
                 * @param filename PLY file to load
                 * @return PLY file contents, vertex and face information
                 */
                pcl::PolygonMesh::Ptr load(const std::string& filename) const;
                
                /**
                 * Set the verbose flag
                 * @param verbose verbose flag
                 */
                inline void setVerbose(bool verbose) {
                    _verbose = verbose;
                }
                
            private:
                /// Print status messages during loading
                bool _verbose;
                
                /// Byte orderings
                enum FORMAT {ASCII, BINARY_LITTLE_ENDIAN, BINARY_BIG_ENDIAN};
                
                /// A property of which a PLY element can have one or more
                struct Property {
                        /** Property datatype, one of the following:
                         * char uchar short ushort int   uint   float   double
                         * int8 uint8 int16 uint16 int32 uint32 float32 float64
                         * list
                         */
                        std::string type;
                        
                        /// Name of field such as 'x' or 'vertex_indices'
                        std::string name;
                        
                        /// Only for list properties: datatypes for length and list entries, e.g. uchar and int
                        std::pair<std::string,std::string> ltypes;
                };

                /// A PLY element
                struct Element {
                        /// Type of element, e.g. 'vertex' and 'face'
                        std::string type;
                        
                        /// Number of elements
                        size_t count;
                        
                        /// Properties of each element
                        std::vector<Property> props;
                };

                /**
                 * Parse the header of a PLY file, opened in the input stream
                 * @param ifs file stream, must be opened as ASCII and at the start of the header
                 * @param elements PLY file information as a collection of elements
                 * @param fmt PLY file data format (ASCII or binary)
                 */
                void readHeader(std::ifstream& ifs, std::vector<Element>& elements, FORMAT& fmt) const;
                
                /**
                 * Read the actual contents (non-header part) of a PLY file
                 * @param filename file name, only used for re-opening in case of a binary file
                 * @param ifs file stream, must be opened as ASCII and at the end of the header
                 * @param elements output PLY elements
                 * @param fmt PLY file data format (ASCII or binary)
                 * @return PCL mesh
                 */
                pcl::PolygonMesh::Ptr readContents(const std::string& filename,
                        std::ifstream& ifs,
                        const std::vector<Element>& elements,
                        FORMAT fmt) const;
        };
        
        /**
         * @ingroup util
         * @brief Load a PLY file as a polygon mesh
         * @param filename input file
         * @param mesh output mesh
         * @return true if the loading succeeded
         */
        inline bool loadPLYFile(const std::string& filename, pcl::PolygonMesh& mesh) {
            PLYLoader reader;
            pcl::PolygonMesh::Ptr tmp = reader.load(filename);
            if(tmp) {
                mesh = *tmp;
                return true;
            } else {
                return false;
            }
        }
        
        /**
         * @ingroup util
         * @brief Load a PLY file as a point cloud
         * @param filename input file
         * @param cloud output point cloud
         * @return true if the loading succeeded
         */
        inline bool loadPLYFile(const std::string& filename, pcl::PCLPointCloud2& cloud) {
            PLYLoader reader;
            pcl::PolygonMesh::Ptr mesh = reader.load(filename);
            if(mesh) {
                cloud = mesh->cloud;
                return true;
            } else {
                return false;
            }
        }
        
        /**
         * @ingroup util
         * @brief Load a PLY file as a templated point cloud
         * @param filename input file
         * @param cloud output point cloud
         * @tparam PointT point type
         * @return true if the loading succeeded
         */
        template<typename PointT>
        inline bool loadPLYFile(const std::string& filename, pcl::PointCloud<PointT>& cloud) {
            pcl::PCLPointCloud2 blob;
            if(loadPLYFile(filename, blob)) {
                pcl::fromPCLPointCloud2(blob, cloud);
                return true;
            } else {
                return false;
            }
        }
    }
}

#endif
