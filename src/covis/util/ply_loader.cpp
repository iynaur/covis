// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.


// Own
#include "ply_loader.h"

// Boost
#include <boost/algorithm/string.hpp>
#include <boost/any.hpp>
namespace al=boost::algorithm;

// PCL
#include <pcl/PCLPointCloud2.h>
#include <pcl/common/io.h>

// Covis
#include "../core/macros.h"

namespace {
    // Scalar types, coded as enum
    enum {CHAR, UCHAR, SHORT, USHORT, INT, UINT, FLOAT, DOUBLE, LIST};
    
    // String to enum
    inline int typeOf(const std::string& type) {
        if(type == "char" || type == "int8") {
            return CHAR;
        } else if(type == "uchar" || type == "uint8") {
            return UCHAR;
        } else if(type == "short" || type == "int16") {
            return SHORT;
        } else if(type == "ushort" || type == "uint16") {
            return USHORT;
        } else if(type == "int" || type == "int32") {
            return INT;
        } else if(type == "uint" || type == "uint32") {
            return UINT;
        } else if(type == "float" || type == "float32") {
            return FLOAT;
        } else if(type == "double" || type == "float64") {
            return DOUBLE;
        } else if(type == "list") {
            return LIST;
        } else {
            COVIS_THROW("No such type: " << type << "!");
        }
    }

    // Any value type
    union Value {
            int8_t vchar; uint8_t vuchar;
            int16_t vshort; uint16_t vushort;
            int32_t vint; uint32_t vuint;
            float vfloat; double vdouble;
    };
    
    // Cast a value
    template<typename T>
    inline T as(const Value& value, int type) {
        switch(type) {
            case CHAR:
                return static_cast<T>(value.vchar);
                break;
            case UCHAR:
                return static_cast<T>(value.vuchar);
                break;
            case SHORT:
                return static_cast<T>(value.vshort);
                break;
            case USHORT:
                return static_cast<T>(value.vushort);
                break;
            case INT:
                return static_cast<T>(value.vint);
                break;
            case UINT:
                return static_cast<T>(value.vuint);
                break;
            case FLOAT:
                return static_cast<T>(value.vfloat);
                break;
            case DOUBLE:
                return static_cast<T>(value.vdouble);
                break;
            default:
                COVIS_THROW("No such type: " << type << "!");
        }
        
    }

    // Read next line from a stream, skipping lines starting with "comment" or "obj_info"
    inline void nextLine(std::istream& is, std::string& line) {
        do {
            std::getline(is, line);
            COVIS_ASSERT(is.good());
            // Remove CR
            if(!line.empty() && line[line.size() - 1] == '\r')
                line = line.substr(0, line.size() - 1);
        } while(line.substr(0, 7) == "comment" || line.substr(0, 8) == "obj_info");
    }
    
    // Read next ASCII value from file stream, formatted
    inline void getASCIIValue(std::istream& is, Value& value, int type) {
        // Cannot stream directly to (u)char because that will cause conversion from number to ASCII codes
        switch(type) {
            case CHAR: {
                int tmp;
                is >> tmp;
                value.vchar = static_cast<int8_t>(tmp);
                break;
            } case UCHAR: {
                int tmp;
                is >> tmp;
                value.vuchar = static_cast<uint8_t>(tmp);
                break;
            } case SHORT:
                is >> value.vshort;
                break;
            case USHORT:
                is >> value.vushort;
                break;
            case INT:
                is >> value.vint;
                break;
            case UINT:
                is >> value.vuint;
                break;
            case FLOAT:
                is >> value.vfloat;
                break;
            case DOUBLE:
                is >> value.vdouble;
                break;
            default:
                COVIS_THROW("No such type: " << type << "!");
        }
    }
    
    // Get the complete contents of a binary file
    // Here's some speed tests: http://insanecoding.blogspot.dk/2011/11/how-to-read-in-file-in-c.html
    inline void getBinaryContents(const std::string& filename, std::ifstream::pos_type offset, std::string& contents) {
        // Open in binary mode
        std::ifstream ifs(filename.c_str(), std::ifstream::in | std::ifstream::binary);
        ifs.seekg(0, std::ifstream::end);
        
        // Allocate file contents
        contents.resize(ifs.tellg() - offset);
        
        // Read
        ifs.seekg(offset);
        ifs.read(&contents[0], contents.size());
        ifs.close();
    }
    
    // Read next binary value from a buffer, increment the offset
    inline void getBinaryValue(std::string& contents, size_t& offset, Value& value, int type) {
        switch(type) {
            case CHAR: {
                value.vchar = *reinterpret_cast<int8_t*>(&contents[offset]);
                offset += sizeof(int8_t);
                break;
            } case UCHAR: {
                value.vuchar = *reinterpret_cast<uint8_t*>(&contents[offset]);
                offset += sizeof(uint8_t);
                break;
            } case SHORT:
                value.vshort = *reinterpret_cast<int16_t*>(&contents[offset]);
                offset += sizeof(int16_t);
                break;
            case USHORT:
                value.vushort = *reinterpret_cast<uint16_t*>(&contents[offset]);
                offset += sizeof(uint16_t);
                break;
            case INT:
                value.vint = *reinterpret_cast<int32_t*>(&contents[offset]);
                offset += sizeof(int32_t);
                break;
            case UINT:
                value.vuint = *reinterpret_cast<uint32_t*>(&contents[offset]);
                offset += sizeof(uint32_t);
                break;
            case FLOAT:
                value.vfloat = *reinterpret_cast<float*>(&contents[offset]);
                offset += sizeof(float);
                break;
            case DOUBLE:
                value.vdouble = *reinterpret_cast<double*>(&contents[offset]);
                offset += sizeof(double);
                break;
            default:
                COVIS_THROW("No such type: " << type << "!");
        }
    }
}

using namespace covis::util;

pcl::PolygonMesh::Ptr PLYLoader::load(const std::string& filename) const {
    if(_verbose)
        COVIS_MSG("Loading PLY file \"" << filename << "\"...");
    
    // Open as ASCII
    std::ifstream ifs(filename.c_str(), std::ifstream::in);
    COVIS_ASSERT_MSG(ifs.good(), "Failed to load PLY file: " << filename << "!");
    
    // Parse header
    FORMAT fmt;
    std::vector<Element> elements;
    readHeader(ifs, elements, fmt);
    
    // Read and return contents
    return readContents(filename, ifs, elements, fmt);
}

void PLYLoader::readHeader(std::ifstream& ifs, std::vector<Element>& elements, FORMAT& fmt) const {
    /*
     * Read header
     * http://paulbourke.net/dataformats/ply/
     * https://en.wikipedia.org/wiki/PLY_(file_format)
     */

    // Magic number
    std::string line;
//    std::getline(ifs, line);
    nextLine(ifs, line);
    COVIS_ASSERT_MSG(line == "ply", "Wrong magic number: " << line);

    // Get next line, skipping comments
    nextLine(ifs, line);

    // Format line
    if(_verbose)
        COVIS_MSG("Got format line: \"" << line << "\"");
    
    if(line == "format ascii 1.0") {
        fmt = ASCII;
    } else if(line == "format binary_little_endian 1.0") {
        fmt = BINARY_LITTLE_ENDIAN;
    } else if(line == "format binary_big_endian 1.0") {
        fmt = BINARY_BIG_ENDIAN;
    } else {
        COVIS_THROW("Wrong format string: " << line);
    }

    // Get next line, skipping comments
    nextLine(ifs, line);
    
    elements.clear();

    // Find all elements until we meet the end of the header
    while(line != "end_header") {
        if(line.substr(0, 7) == "element") {
            if(_verbose)
                COVIS_MSG("Got element line: \"" << line << "\"");

            // Now get element type and count
            Element e;
            std::istringstream iss(line);
            iss >> e.type; // Dummy, the 'element'
            iss >> e.type;
            iss >> e.count;
            if(_verbose)
                COVIS_MSG("\t" << e.count << " element(s) of type \"" << e.type << "\"");

            // Get next line, skipping comments
            nextLine(ifs, line);

            // Handle property line for current element
            while(line.substr(0, 8) == "property") {
                std::istringstream iss(line);
                Property p;
                iss >> p.type; // Dummy, the 'property'
                iss >> p.type;
                if(p.type == "list") {
                    std::string ltypelen, ltypesize;
                    iss >> p.ltypes.first; // Type of list length variable, often uchar
                    iss >> p.ltypes.second; // Type of list entry, often int
                    iss >> p.name;
                    if(_verbose)
                        COVIS_MSG("\t\tList property of length/entry types \"" <<
                                p.ltypes.first << "\"/\"" << p.ltypes.second <<
                                "\" with name \"" << p.name << "\"");
                } else {
                    iss >> p.name;
                    if(_verbose)
                        COVIS_MSG("\t\tScalar property of type \"" << p.type <<
                                "\" with name \"" << p.name << "\"");
                }
                e.props.push_back(p);

                // Get next line, skipping comments
                nextLine(ifs, line);
            }

            // Add element
            elements.push_back(e);
        } else {
            COVIS_THROW("Illegal line: \"" << line << "\"!");
        } // End element
    } // End header

    if(_verbose)
        COVIS_MSG("Finished reading header!");
}

pcl::PolygonMesh::Ptr PLYLoader::readContents(const std::string& filename,
        std::ifstream& ifs,
        const std::vector<Element>& elements,
        FORMAT fmt) const {
    // See if we have relevant vertex information
    bool hasxyz[3] = {0,0,0};
    bool hasn[3] = {0,0,0};
    bool hasrgb[3] = {0,0,0};
    
    // Find all vertex fields
    for(size_t i = 0; i < elements.size(); ++i) {
        const Element& e = elements[i];
        for(size_t j = 0; j < e.props.size(); ++j) {
            const Property& p = e.props[j];
            if(e.type == "vertex") {
                if(p.name == "x")
                    hasxyz[0] = true;
                if(p.name == "y")
                    hasxyz[1] = true;
                if(p.name == "z")
                    hasxyz[2] = true;
                // TODO: The normal_* are non-standard
                if(p.name == "nx" || p.name == "normal_x")
                    hasn[0] = true;
                if(p.name == "ny" || p.name == "normal_y")
                    hasn[1] = true;
                if(p.name == "nz" || p.name == "normal_z")
                    hasn[2] = true;
                if(p.name == "red")
                    hasrgb[0] = true;
                if(p.name == "green")
                    hasrgb[1] = true;
                if(p.name == "blue")
                    hasrgb[2] = true;
            }
        }
    }
    
    // TODO: Ugly: we use the first entry from now
    hasxyz[0] = (hasxyz[0] && hasxyz[1] && hasxyz[2]);
    hasn[0] = (hasn[0] && hasn[1] && hasn[2]);
    hasrgb[0] = (hasrgb[0] && hasrgb[1] && hasrgb[2]);

    // Prepare outputs
    pcl::PointCloud<pcl::PointXYZ> pcxyz;
    pcl::PointCloud<pcl::Normal> pcn;
    pcl::PointCloud<pcl::RGB> pcrgb;
    std::vector<pcl::Vertices> face;
    
    // Verbosity
    if(_verbose) {
        if(fmt == ASCII)
            COVIS_MSG("Reading ASCII contents...");
        else
            COVIS_MSG("Reading binary contents...");
    }
    
    // In the binary case, close the file and read the rest as binary
    std::string contents;
    size_t offset = 0;
    if(fmt == BINARY_BIG_ENDIAN || fmt == BINARY_LITTLE_ENDIAN) {
        // Get current position for end of header
        const std::ifstream::pos_type pos = ifs.tellg();
        ifs.close();
        
        // Read binary file contents from current position (end of header)
        getBinaryContents(filename, pos, contents);
    }
    
    // Loop over elements
    for(size_t i = 0; i < elements.size(); ++i) {
        // Get element
        const Element& e = elements[i];
        
        // Some verbosity
        if(_verbose) {
            if(e.type == "vertex") {
                COVIS_MSG("Loading vertex information...");
            } else if(e.type == "face") {
                COVIS_MSG("Loading face information...");                    
            } else {
                COVIS_MSG_WARN("Elements of type \"" << e.type << "\" not supported! Skipping...");
            }
        }
        
        // Get enum types of properties because it would be slow to do it in the loop below
        std::vector<int> types(e.props.size());
        // In the list case, also get the enum types for the length and entries
        std::vector<std::pair<int,int> > ltypes(e.props.size(), std::make_pair<int,int>(-1,-1));
        for(size_t j = 0; j < e.props.size(); ++j) {
            types[j] = typeOf(e.props[j].type);
            if(types[j] == LIST)
                ltypes[j] = std::make_pair<int,int>(typeOf(e.props[j].ltypes.first),typeOf(e.props[j].ltypes.second));
        }
        
        // Loop over element values
        for(size_t j = 0; j < e.count; ++j) {
            // Prepare PCL data for current element value
            pcl::PointXYZ pxyz;
            pcl::Normal pn;
            pcl::RGB prgb;
            
            // Loop over props for this element
            for(size_t k = 0; k < e.props.size(); ++k) {
                // Get prop
                const Property& p = e.props[k];
                // Switch between lists and scalars
                if(types[k] == LIST) {
                    // First get list length, convert to int
                    Value vlen;
                    if(fmt == ASCII)
                        getASCIIValue(ifs, vlen, ltypes[k].first);
                    else
                        getBinaryValue(contents, offset, vlen, ltypes[k].first);
                    const int vlenint = as<int>(vlen, ltypes[k].first);
                    // Then get the list entries
                    std::vector<Value> vlist(vlenint);
                    for(int l = 0; l < vlenint; ++l) {
                        if(fmt == ASCII)
                            getASCIIValue(ifs, vlist[l], ltypes[k].second);
                        else
                            getBinaryValue(contents, offset, vlist[l], ltypes[k].second);
                    }
                    
                    // Convert list to PCL format
                    if(e.type == "face" && (p.name == "vertex_indices" || p.name == "vertex_index")) {
                        pcl::Vertices vpcl;
                        vpcl.vertices.resize(vlenint);
                        for(int l = 0; l < vlenint; ++l)
                            vpcl.vertices[l] = as<uint32_t>(vlist[l], ltypes[k].second);
                        face.push_back(vpcl);
                    }
                } else {
                    // Extract scalar value
                    Value value;
                    if(fmt == ASCII)
                        getASCIIValue(ifs, value, types[k]);
                    else
                        getBinaryValue(contents, offset, value, types[k]);
                    
                    // Convert scalar to PCL format
                    if(e.type == "vertex") {
                        if(p.name == "x")
                            pxyz.x = as<float>(value, types[k]);
                        if(p.name == "y")
                            pxyz.y = as<float>(value, types[k]);
                        if(p.name == "z")
                            pxyz.z = as<float>(value, types[k]);
                        // TODO: The normal_* names are non-standard
                        if(p.name == "nx" || p.name == "normal_x")
                            pn.normal_x = as<float>(value, types[k]);
                        if(p.name == "ny" || p.name == "normal_y")
                            pn.normal_y = as<float>(value, types[k]);
                        if(p.name == "nz" || p.name == "normal_z")
                            pn.normal_z = as<float>(value, types[k]);
                        if(p.name == "red")
                            prgb.r = as<uint8_t>(value, types[k]);
                        if(p.name == "green")
                            prgb.g = as<uint8_t>(value, types[k]);
                        if(p.name == "blue")
                            prgb.b = as<uint8_t>(value, types[k]);
                    }
                }
            } // End loop over properties
            
            // Store PCL format
            if(e.type == "vertex") {
                if(hasxyz[0])
                    pcxyz.push_back(pxyz);
                if(hasn[0])
                    pcn.push_back(pn);
                if(hasrgb[0])
                    pcrgb.push_back(prgb);
            }
        } // End loop over element values
    } // End loop over elements
    
    // Close file if still open
    if(fmt == ASCII)
        ifs.close();
    
    /*
     * Concatenate all vertex fields into one cloud
     */
    // TODO: All of this could be written directly into a blob in the loop above
    pcl::PCLPointCloud2 blob;
    if(hasxyz[0]) {
        pcl::toPCLPointCloud2(pcxyz, blob);
        if(hasn[0]) {
            pcl::PCLPointCloud2 tmp;
            pcl::toPCLPointCloud2(pcn, tmp);
            COVIS_ASSERT(tmp.width == blob.width && tmp.height == blob.height);
            
            pcl::PCLPointCloud2 tmp2;
            pcl::concatenateFields(blob, tmp, tmp2);
            std::swap(blob, tmp2);
        }
        if(hasrgb[0]) {
            pcl::PCLPointCloud2 tmp;
            pcl::toPCLPointCloud2(pcrgb, tmp);
            COVIS_ASSERT(tmp.width == blob.width && tmp.height == blob.height);

            pcl::PCLPointCloud2 tmp2;
            pcl::concatenateFields(blob, tmp, tmp2);
            std::swap(blob, tmp2);
        }
    } else {
        // I cannot see a use case for a mesh where you'd want no XYZ
        COVIS_MSG_WARN("No XYZ information found for vertex elements!");
    }
    
    // Produce the final PCL mesh output
    pcl::PolygonMesh::Ptr mpcl(new pcl::PolygonMesh);
    mpcl->cloud = blob;
    mpcl->polygons = face;
    
    return mpcl;
}
