// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_DETECT_POINT_SEARCH_IMPL_HPP
#define COVIS_DETECT_POINT_SEARCH_IMPL_HPP

// PCL
#include <pcl/point_representation.h>

namespace covis {
    namespace detect {
        template<typename PointT, typename DistT>
        void PointSearch<PointT,DistT>::index() {
            pcl::DefaultPointRepresentation<PointT> pr;
            _dim = pr.getNumberOfDimensions();

            if(_mdata)
                delete[] _mdata;

            _mdata = new float[_target->size() * _dim];
            for(size_t i = 0; i < _target->size(); ++i)
                pr.copyToFloatArray(_target->points[i], &_mdata[i*_dim]);

            flann::Matrix<float> m(_mdata, _target->size(), _dim);

            tree = TreeT(m, flann::KDTreeSingleIndexParams(_leaf));
            tree.buildIndex();
        }
        
        template<typename PointT, typename DistT>
        void PointSearch<PointT,DistT>::doKnn(const PointT& query, size_t k, core::Correspondence& result) const {
            result.match.resize(k);
            result.distance.resize(k);
            flann::SearchParams param;
            param.checks = -1;
            param.sorted = _sort;
            flann::Matrix<size_t> mmatch(&result.match[0], 1, k);
            flann::Matrix<float> mdist(&result.distance[0], 1, k);
            tree.knnSearch(flann::Matrix<float>(const_cast<float*>(reinterpret_cast<const float*>(&query)), 1, _dim),
                           mmatch,
                           mdist,
                           k,
                           param);
        }
        
        template<typename PointT, typename DistT>
        void PointSearch<PointT,DistT>::doRadius(const PointT& query, float r, core::Correspondence& result) const {
            flann::SearchParams param;
            param.checks = -1;
            param.max_neighbors = -1;
            param.sorted = _sort;
            std::vector<std::vector<size_t> > idxtmp;
            std::vector<std::vector<float> > disttmp;
            tree.radiusSearch(flann::Matrix<float>(const_cast<float*>(reinterpret_cast<const float*>(&query)), 1, _dim),
                              idxtmp,
                              disttmp,
                              r * r,
                              param);

            result.match = idxtmp[0];
            result.distance = disttmp[0];
        }
    }
}

#endif
