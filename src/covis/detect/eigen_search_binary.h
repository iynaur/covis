// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_DETECT_EIGEN_SEARCH_BINARY_H
#define COVIS_DETECT_EIGEN_SEARCH_BINARY_H

// Own
#include "../core/correspondence.h"
#include "../feature/feature_binarization.h"

// Eigen
#include <eigen3/Eigen/Dense>

// FLANN
#include <flann/flann.hpp>

namespace covis {
    namespace detect {
        /**
         * @ingroup detect
         * @class EigenSearchBinary
         * @brief Search both high-dimensional binary data in an Eigen matrix using FLANN's Hamming distance
         * 
         * @note This class returns Hamming distances as floats, each corresponding to the popcount between any two
         * binarized features
         * @todo Support LSH index for approximate search
         *
         * @sa @ref feature::FeatureBinarization
         * @author Anders Glent Buch
         */
        class EigenSearchBinary {
            public:
                /// Pointer type
                typedef boost::shared_ptr<EigenSearchBinary> Ptr;
                
                /// Pointer to const type
                typedef boost::shared_ptr<const EigenSearchBinary> ConstPtr;
                
                /**
                 * Constructor: setup index
                 * @param sort set to false to disable sorted results
                 */
                EigenSearchBinary(bool sort = true) : _sort(sort) {}

                /**
                 * Constructor: setup index and index data
                 * @param target binary target feature set
                 * @param sort set to false to disable sorted results
                 */
                EigenSearchBinary(const feature::BinaryMatrixT& target, bool sort = true) : _sort(sort) {
                    setTarget(target);
                }
                
                /// Empty destructor
                virtual ~EigenSearchBinary() {}

                /**
                 * Set the sorting flag
                 * @param sort sorting flag
                 */
                inline void setSort(bool sort) {
                    _sort = sort;
                }

                /**
                 * Set target point/feature set and index the data
                 * @param target binary target feature set
                 */
                inline void setTarget(const feature::BinaryMatrixT& target) {
                    index(target);
                }

                /**
                 * Compute k-NN point/feature matches
                 * @param query query point/feature set
                 * @param k number of matches
                 * @return matches as correspondences
                 */
                core::Correspondence::VecPtr knn(const feature::BinaryMatrixT& query, size_t k) const;

                /**
                 * Compute radius matches
                 * @param query query point/feature set
                 * @param r search radius
                 * @return matches as correspondences
                 */
                core::Correspondence::VecPtr radius(const feature::BinaryMatrixT& query, float r) const;

                /**
                 * Perform a 2-NN search and return correspondences with the distance set to the ratio
                 * of the distance to the first match to the distance to the seconds, all squared for efficiency
                 * @param query query features
                 * @return matches as correspondences
                 */
                inline core::Correspondence::VecPtr ratio(const feature::BinaryMatrixT& query) {
                    core::Correspondence::VecPtr c = this->knn(query, 2);
                    for(size_t i = 0; i < c->size(); ++i) {
                        // Handle invalids
                        float ratio;
                        if((*c)[i].size() == 2)
                            ratio = (*c)[i].distance[0] / (*c)[i].distance[1];
                        else
                            ratio = 1; // Natural upper bound for ratio matching

                        if(!(*c)[i].empty())
                            (*c)[i] = core::Correspondence((*c)[i].query, (*c)[i].match[0], ratio);
                    }

                    return c;
                }

            private:

                /// Search structure type
                typedef flann::LinearIndex<flann::HammingLUT> BinaryIndexT;
//                typedef flann::LshIndex<flann::HammingLUT> BinaryIndexT;
//                typedef flann::LshIndex<flann::Hamming<feature::BinaryMatrixT::Scalar> > BinaryIndexT;
//                typedef flann::LshIndex<flann::HammingPopcnt<feature::BinaryMatrixT::Scalar> > BinaryIndexT;

                /// Search index
                BinaryIndexT _index;

                /// Set to true (default) to return neighbors sorted by distance
                bool _sort;

                /// Resolved point type dimension
                size_t _dim;

                /**
                 * Build search index
                 * @param target data to index
                 */
                void index(const feature::BinaryMatrixT& target);
        };
        
        /**
         * @ingroup detect
         * @brief Perform a single-shot bulk binary k-NN search
         * @note This function returns Hamming distances
         * @param query query points
         * @param target target points to search into
         * @param k number of neighbors to search for
         * @return matches as correspondences
         */
        core::Correspondence::VecPtr knnSearch(const feature::BinaryMatrixT& query,
                                               const feature::BinaryMatrixT& target,
                                               size_t k);

        /**
         * @ingroup detect
         * @brief Perform a single-shot bulk binary k-NN search
         * @note This function returns Hamming distances
         * @param query query points
         * @param target target points to search into
         * @param k number of neighbors to search for
         * @return matches as correspondences
         */
        inline core::Correspondence::VecPtr computeKnnMatches(const feature::BinaryMatrixT& query,
                                                              const feature::BinaryMatrixT& target,
                                                              size_t k) {
            return knnSearch(query, target, k);
        }

        /**
         * @ingroup detect
         * @brief Perform a single-shot bulk binary radius search
         * @note This function returns squared distances
         * @param query query points
         * @param target target points to search into
         * @param r search radius
         * @return matches as correspondences
         */
        core::Correspondence::VecPtr radiusSearch(const feature::BinaryMatrixT& query,
                                                  const feature::BinaryMatrixT& target,
                                                  float r);

        /**
         * @ingroup detect
         * @brief Perform a single-shot bulk binary radius search
         * @note This function returns squared distances
         * @param query query points
         * @param target target points to search into
         * @param r search radius
         * @return matches as correspondences
         */
        inline core::Correspondence::VecPtr computeRadiusMatches(const feature::BinaryMatrixT& query,
                                                                 const feature::BinaryMatrixT& target,
                                                                 float r) {
            return radiusSearch(query, target, r);
        }

        /**
         * @ingroup detect
         * @brief Perform a 2-NN search in binary feature space and return correspondences with the distance set to the
         * ratio of the distance to the first match to the distance to the seconds
         * @param query query features
         * @param target target features to search into
         * @return matches as correspondences
         */
        core::Correspondence::VecPtr computeRatioMatches(const feature::BinaryMatrixT& query,
                                                         const feature::BinaryMatrixT& target);

        /**
         * @ingroup detect
         * @brief Perform ratio matching between two or more sets of binary features and fuse the matches
         * @param query query features
         * @param target target features to search into
         * @return matches as correspondences
         */
        core::Correspondence::VecPtr computeFusionMatches(
                const std::vector<feature::BinaryMatrixT, Eigen::aligned_allocator<feature::BinaryMatrixT> >& query,
                const std::vector<feature::BinaryMatrixT, Eigen::aligned_allocator<feature::BinaryMatrixT> >& target);
    }
}
    
#endif
