// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_DETECT_GRID_SEARCH_IMPL_HPP
#define COVIS_DETECT_GRID_SEARCH_IMPL_HPP

#include "grid_search.h"

// PCL
#include <pcl/common/common.h>

namespace covis {
    namespace detect {
        template<typename PointT>
        void GridSearch<PointT>::index() {
            COVIS_ASSERT_MSG(_dx > 0 && _dy > 0 && _dz > 0, "Resolutions must be set to a positive value before setting target for indexing!");
            pcl::getMinMax3D<PointT>(*_target, _min, _max);
            _nx = (_max.x - _min.x) / _dx + 1;
            _ny = (_max.y - _min.y) / _dy + 1;
            _nz = (_max.z - _min.z) / _dz + 1;

            _index.resize(_nx);
            for(IdxT i = 0; i < _nx; ++i) {
                _index[i].resize(_ny);
                for(IdxT j = 0; j < _ny; ++j)
                    _index[i][j].resize(_nz);
            }

            _reverseIndex.resize(_target->size());
            for(size_t i = 0; i < _target->size(); ++i) {
                const Coordinate c = getIdx(_target->points[i]);
                _index[c.x][c.y][c.z].push_back(i);
                _reverseIndex[i] = c;
            }
        }
        
        template<typename PointT>
        void GridSearch<PointT>::doKnn(const PointT& query, size_t k, core::Correspondence& result) const {
            result.match.clear();
            result.distance.clear();

            const ListT l = getNeighbors(query);
            if(l.empty())
                return;

            QueueT q;
            for(size_t i = 0; i < l.size(); ++i) {
                // Compute distance
                const PointT &t = _target->points[l[i]];
                const float distsq = (t.x - query.x) * (t.x - query.x) +
                                     (t.y - query.y) * (t.y - query.y) +
                                     (t.z - query.z) * (t.z - query.z);

                // Add <index,distance> to queue
                q.push(std::make_pair(l[i], distsq));
            }

            for(size_t i = 0; i < MIN(k, q.size()); ++i) {
                const PairT& p = q.top();
                result.match.push_back(p.first);
                result.distance.push_back(p.second);
                q.pop();
            }
        }
        
        template<typename PointT>
        void GridSearch<PointT>::doRadius(const PointT& query, float r, core::Correspondence& result) const {
            result.match.clear();
            result.distance.clear();

            const ListT l = getNeighbors(query);
            if(l.empty())
                return;

            const float rsq = r * r;
            QueueT q;
            for(size_t i = 0; i < l.size(); ++i) {
                // Compute distance
                const PointT& t = _target->points[l[i]];
                const float distsq = (t.x - query.x) * (t.x - query.x) +
                                     (t.y - query.y) * (t.y - query.y) +
                                     (t.z - query.z) * (t.z - query.z);

                // Add match to result, but only if within radius
                if(distsq <= rsq) {
                    if(_sort) { // Sorting enabled, push to priority queue
                        q.push(std::make_pair(l[i], distsq));
                    } else { // No sorting, directly store result
                        result.match.push_back(l[i]);
                        result.distance.push_back(distsq);
                    }
                }
            }

            // Sort
            if(_sort) {
                while(!q.empty()) {
                    const PairT &p = q.top();
                    result.match.push_back(p.first);
                    result.distance.push_back(p.second);
                    q.pop();
                }
            }
        }

        template<typename PointT>
        const typename GridSearch<PointT>::ListT& GridSearch<PointT>::getVoxel(size_t idx) const {
            COVIS_ASSERT(idx < _reverseIndex.size());
            const Coordinate& c = _reverseIndex[idx];

            return _index[c.x][c.y][c.z];
        }

        template<typename PointT>
        size_t GridSearch<PointT>::count(size_t idx) const {
            return getVoxel(idx).size();
        }

        template<typename PointT>
        size_t GridSearch<PointT>::count(const PointT& query) const {
            return getNeighbors(query).size();
        }
    }
}

#endif
