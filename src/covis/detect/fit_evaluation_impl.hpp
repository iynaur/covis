// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_DETECT_FIT_EVALUATION_IMPL_HPP
#define COVIS_DETECT_FIT_EVALUATION_IMPL_HPP

// Own
#include "fit_evaluation.h"
#include "../core/traits.h"
#include "../core/transform.h"

// STL
#include <cfloat>

namespace covis {
    namespace detect {
        template<typename PointT>
        core::Correspondence::VecPtr FitEvaluation<PointT>::update(typename pcl::PointCloud<PointT>::ConstPtr source,
                const MatrixT& pose) {
            // Sanity checks
            COVIS_ASSERT(source && !source->empty());
            COVIS_ASSERT(!pose.hasNaN()); // This check because NaNs cause horribly convoluted errors in update() below
            
            // Apply transformation
            typename pcl::PointCloud<PointT>::Ptr sourcet(new pcl::PointCloud<PointT>);
            core::transform<PointT>(*source, *sourcet, pose);
            
            return update(sourcet);
        }
        
        template<typename PointT>
        core::Correspondence::VecPtr FitEvaluation<PointT>::update(typename pcl::PointCloud<PointT>::ConstPtr source) {
            // Sanity checks
            COVIS_ASSERT(source && !source->empty());
            if(_occlusionReasoning) {
                COVIS_ASSERT(_search2d && _search2d->getTarget()->size() == _target->size());
            } else {
                COVIS_ASSERT(_search && _search->getTarget() == _target);
            }
            
            // Find point matches
            core::Correspondence::VecPtr correspondences;
            if(_occlusionReasoning) {
                pcl::PointCloud<pcl::PointXY>::Ptr source2d(new pcl::PointCloud<pcl::PointXY>);
                switch(_viewAxis) {
                    case 0: // x
                        source2d->resize(source->size());
                        for(size_t i = 0; i < source->size(); ++i) {
                            source2d->points[i].x = source->points[i].y;
                            source2d->points[i].y = source->points[i].z;
                        }
                        break;
                    case 1: // y
                        source2d->resize(source->size());
                        for(size_t i = 0; i < source->size(); ++i) {
                            source2d->points[i].x = source->points[i].z;
                            source2d->points[i].y = source->points[i].x;
                        }
                        break;
                    default: // z
                        pcl::copyPointCloud(*source, *source2d);
                }
                correspondences = _search2d->knn(source2d, 1);
                for(size_t i = 0; i < correspondences->size(); ++i)
                    (*correspondences)[i].distance[0] = ( source->points[(*correspondences)[i].query].getVector3fMap() -
                            _target->points[(*correspondences)[i].match[0]].getVector3fMap() ).squaredNorm();
            } else {
                // For efficiency, use a radius search here
//                correspondences = _search->knn(source, 1);
                correspondences = _search->radius(source, _inlierThreshold);
            }
            
            update(source, correspondences);
            
            return correspondences;
        }
        
        template<typename PointT>
        void FitEvaluation<PointT>::update(typename pcl::PointCloud<PointT>::ConstPtr source,
                const MatrixT& pose,
                core::Correspondence::VecConstPtr correspondences) {
            // Sanity checks
            COVIS_ASSERT(source && !source->empty());
            
            // Apply transformation to the points indexed in the correspondence set and store point distances
            core::Correspondence::VecPtr corrWithDistances(new core::Correspondence::Vec(correspondences->size()));
            typename pcl::PointCloud<PointT>::Ptr sourcet(new pcl::PointCloud<PointT>(source->size(), 1));
            for(size_t i = 0; i < correspondences->size(); ++i) {
                // Copy
                sourcet->points[(*correspondences)[i].query] = source->points[(*correspondences)[i].query];
                // Transform
                core::transform<PointT>(sourcet->points[(*correspondences)[i].query], pose);
                // Store distance, squared
                (*corrWithDistances)[i] = (*correspondences)[i];
                if((*correspondences)[i].empty())
                    (*corrWithDistances)[i].distance[0] = std::numeric_limits<float>::quiet_NaN();
                else
                    (*corrWithDistances)[i].distance[0] =
                            (sourcet->points[(*correspondences)[i].query].getVector3fMap() -
                             _target->points[(*correspondences)[i].match[0]].getVector3fMap()).squaredNorm();
            }
            
            update(sourcet, corrWithDistances);
        }
            
        template<typename PointT>
        void FitEvaluation<PointT>::update(typename pcl::PointCloud<PointT>::ConstPtr source,
                core::Correspondence::VecConstPtr correspondences) {
            // Sanity checks
            COVIS_ASSERT(source && !source->empty());
            COVIS_ASSERT_MSG(_inlierThresholdSquared > 0.0f, "Inlier threshold not set!");
            
            // Accumulate number of inliers and MSE
            _state.size = correspondences->size();
            _state.inliers.clear();
            _state.inliers.reserve(correspondences->size());
            _state.mse = 0.0;
            _state.occluded.clear();
            _state.occluded.reserve(correspondences->size());
            _state.outliers.clear();
            _state.outliers.reserve(correspondences->size());
            for(size_t i = 0; i < correspondences->size(); ++i) {
                // If point distance is within threshold
                if((*correspondences)[i].size() > 0 && (*correspondences)[i].distance[0] < _inlierThresholdSquared) { // Inlier
                    _state.inliers.push_back((*correspondences)[i]);
                    _state.mse += (*correspondences)[i].distance[0];
                } else { // Non-inlier
                    // Source point index
                    const int isrc = (*correspondences)[i].query;
                    // If occlusion reasoning is on, we can remove occluded points from the list of possible outliers
                    if(_occlusionReasoning && (*correspondences)[i].size() > 0) {
                        // Source/target point
                        const PointT& psrc = source->points[isrc];
                        const int itgt = (*correspondences)[i].match[0];
                        const PointT& ptgt = _target->points[itgt];
                        // Measure distance from sensor
                        if(psrc.getVector3fMap().squaredNorm() > ptgt.getVector3fMap().squaredNorm()) // Occluded
                            _state.occluded.push_back(isrc);
                        else // Occluding
                            _state.outliers.push_back(isrc);
                    } else {
                        // No occlusion reasoning
                        _state.outliers.push_back(isrc);
                    }
                }
            }
            
            // Set to infinite if there were no inliers
            if(_state.inliers.empty())
                _state.mse = DBL_MAX;
            else
                _state.mse /= double(_state.inliers.size());
        }
    }
}
#endif
