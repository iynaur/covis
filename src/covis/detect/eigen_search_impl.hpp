// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_DETECT_EIGEN_SEARCH_IMPL_HPP
#define COVIS_DETECT_EIGEN_SEARCH_IMPL_HPP

namespace covis {
    namespace detect {
        template<typename Derived, typename DistT>
        void EigenSearch<Derived,DistT>::index() {
            const size_t samples = (_target.IsRowMajor ? _target.rows() : _target.cols());
            _dim = (_target.IsRowMajor ? _target.cols() : _target.rows());
            flann::Matrix<float> m(const_cast<float*>(_target.derived().data()), samples, _dim);

            if(_trees == 0) {
                 _linearIndex = LinearIndexT(m);
                 _linearIndex.buildIndex();
            } else if(_dim <= 10) {
                _singleTree = SingleTreeT(m, flann::KDTreeSingleIndexParams(_leaf));
                _singleTree.buildIndex();
            } else {
                _multiTree = MultiTreeT(m, flann::KDTreeIndexParams(_trees));
                _multiTree.buildIndex();
            }
        }
        
        template<typename Derived, typename DistT>
        core::Correspondence::VecPtr EigenSearch<Derived,DistT>::knn(const MatrixT& query, size_t k) const {
            const size_t samples = (query.IsRowMajor ? query.rows() : query.cols());
            const size_t dim (query.IsRowMajor ? query.cols() : query.rows());
            COVIS_ASSERT_MSG(dim == _dim, "Query matrix has inompatible dimensions with indexed matrix!");

            core::Correspondence::VecPtr result(new core::Correspondence::Vec(samples));

            flann::SearchParams param;
            param.sorted = _sort;
            param.checks = (dim <= 10 ? -1 : _checks);

#ifdef _OPENMP
#pragma omp parallel for
#endif
            for(size_t i = 0; i < samples; ++i) {
                (*result)[i].query = i;

                bool isnan;
                if(query.IsRowMajor)
                    isnan = query.row(i).hasNaN();
                else
                    isnan = query.col(i).hasNaN();
                if(isnan) {
#ifdef _OPENMP
#pragma omp critical
#endif
                    COVIS_MSG_WARN("NaN feature given to Eigen k-NN search - returning empty correspondence!");
                    continue;
                }

                flann::Matrix<float> m(const_cast<float*>(query.derived().data() + i * dim), 1, dim);
                (*result)[i].match.resize(k);
                (*result)[i].distance.resize(k);
                flann::Matrix<size_t> mmatch((*result)[i].match.data(), 1, k);
                flann::Matrix<float> mdist((*result)[i].distance.data(), 1, k);

                if(_trees == 0)
                    _linearIndex.knnSearch(m, mmatch, mdist, k, param);
                else if(_dim <= 10)
                    _singleTree.knnSearch(m, mmatch, mdist, k, param);
                else
                    _multiTree.knnSearch(m, mmatch, mdist, k, param);
            }

            return result;
        }
        
        template<typename Derived, typename DistT>
        core::Correspondence::VecPtr EigenSearch<Derived,DistT>::radius(const MatrixT& query, float r) const {
            const size_t samples = (query.IsRowMajor ? query.rows() : query.cols());
            const size_t dim (query.IsRowMajor ? query.cols() : query.rows());
            COVIS_ASSERT_MSG(dim == _dim, "Query matrix has inompatible dimensions with indexed matrix!");

            core::Correspondence::VecPtr result(new core::Correspondence::Vec(samples));

            flann::SearchParams param;
            param.sorted = _sort;
            param.checks = (dim <= 10 ? -1 : _checks);

#ifdef _OPENMP
#pragma omp parallel for
#endif
            for(size_t i = 0; i < samples; ++i) {
                (*result)[i].query = i;

                bool isnan;
                if(query.IsRowMajor)
                    isnan = query.row(i).hasNaN();
                else
                    isnan = query.col(i).hasNaN();
                if(isnan) {
#ifdef _OPENMP
#pragma omp critical
#endif
                    COVIS_MSG_WARN("NaN feature given to Eigen radius search - returning empty correspondence!");
                    continue;
                }

                flann::Matrix<float> m(const_cast<float*>(query.derived().data()+i*dim), 1, dim);

                std::vector<std::vector<size_t> > idxtmp;
                std::vector<std::vector<float> > disttmp;

                if(_trees == 0)
                    _linearIndex.radiusSearch(m, idxtmp, disttmp, r * r, param);
                else if(_dim <= 10)
                    _singleTree.radiusSearch(m, idxtmp, disttmp, r * r, param);
                else
                    _multiTree.radiusSearch(m, idxtmp, disttmp, r * r, param);

                (*result)[i].match = idxtmp[0];
                (*result)[i].distance = disttmp[0];
            }

            return result;
        }
    }
}

#endif
