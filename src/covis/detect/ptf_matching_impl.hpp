// Copyright (c) 2016, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_DETECT_PTF_MATCHING_IMPL_HPP
#define COVIS_DETECT_PTF_MATCHING_IMPL_HPP

#include "ptf_matching.h"

/// @cond
namespace covis {
    namespace detect {


        //********NON MAX SUPPRESSION*********************************************************************************
        template<typename PointT>
        std::vector<bool>
        nonMaxSuppression(typename pcl::PointCloud<PointT>::Ptr modes, std::vector<float> densities, float _diag, bool debug) {

            const float supRad = 0.2 *_diag; //0.2 * _diag; // SUPPRESSION RADIUS: 20 % OF OBJECT BBOX DIAG
            if (debug) COVIS_MSG_INFO("Sup radius for nonMaxSuppression: " << supRad << " _diag: " << _diag);
            core::Correspondence::VecPtr modeRad = detect::radiusSearch<PointT>(modes, modes, supRad);
            // Modes is a point cloud of your ~1300 poses translation components
            std::vector<bool> keep(modes->size(), true);
            for (size_t idx = 0; idx < modes->size(); ++idx) {
                if (!keep[idx]) continue;
                // Loop over all SE(3) neighbors
                for (size_t radidx = 1; radidx < (*modeRad)[idx].size(); ++radidx) {
                    // Neighbor index
                    const size_t nidx = (*modeRad)[idx].match[radidx];
                    // Suppress either neighbor or self
                    if (densities[idx] >= densities[nidx]) keep[nidx] = false;
                    else keep[idx] = false;
                }
            }
            return keep;
        }

        //********CLUSTER POSES*****************************************************************************************
        inline std::vector<Eigen::Matrix4f>
        clusterPoses(std::vector<Eigen::Matrix4f> poses, size_t amount_of_poses_to_look_for, float maxd, bool debug) {

            const float BANDWIDTH_TRANS = 0.01;
            const float BANDWIDTH_ROT = 22.5 * M_PI / 180.0;

            //get translations as pcl::PointXYZ
            pcl::PointCloud<pcl::PointXYZ>::Ptr translations(new pcl::PointCloud<pcl::PointXYZ>());
            translations->width = poses.size();
            translations->height = 1;
            translations->points.resize(poses.size());

            std::vector<Eigen::Matrix3f, Eigen::aligned_allocator<Eigen::Matrix3f> > rotations;
            for (size_t i = 0; i < poses.size(); i++) {
                pcl::PointXYZ tr;
                tr.x = poses[i](0, 3);
                tr.y = poses[i](1, 3);
                tr.z = poses[i](2, 3);
                (*translations)[i] = tr;

                Eigen::Matrix3f rot;
                rot = poses[i].block<3, 3>(0, 0);
                rotations.push_back(rot);
            }

            std::vector<float> densities(translations->size());
            if (debug) COVIS_MSG_INFO("density->size() : " << translations->size());

            covis::detect::SearchBase<pcl::PointXYZ>::Ptr search;
            search.reset(new covis::detect::PointSearch<pcl::PointXYZ>);
            search->setTarget(translations);

            // Find SE(3) neighbors and compute KDEs in the process
            core::ProgressDisplay::Ptr pd;
            if (debug) pd.reset(new core::ProgressDisplay(translations->size()));

#ifdef _OPENMP
#pragma omp parallel for
#endif
            for (size_t idx = 0; idx < translations->size(); ++idx) {
                densities[idx] = 0;
                const core::Correspondence corrRad = search->radius(translations->points[idx], BANDWIDTH_TRANS);
                if (corrRad.size() > 1) {
                    // Loop over Euclidean neighbors, start from index 1 to skip self
                    for (size_t cradidx = 1; cradidx < corrRad.size(); ++cradidx) {
                        // Now find the SO(3) geodesic distance
                        const Eigen::Matrix3f R1 = rotations[idx];
                        const Eigen::Matrix3f R2 = rotations[corrRad.match[cradidx]];
                        const Eigen::Matrix3f RRT = R1.transpose() * R2;
                        const float acosarg = (RRT.trace() - 1) / 2;
                        const float angle = (acosarg <= -1 ? M_PI : (acosarg >= 1 ? 0 : acosf(acosarg)));

                        // Finally, if SO(3) distance is also low, we have an SE(3) neighbor
                        if (angle <= BANDWIDTH_ROT) {
                            const float dist = sqrtf(corrRad.distance[cradidx]); // Non-squared Euclidean
                            densities[idx] +=
                                    (1 - dist / BANDWIDTH_TRANS) + (1 - angle / BANDWIDTH_ROT); // Triangle kernel, sum
                        }
                    }
                }

#pragma omp critical
                if (debug) ++(*pd);
            }

            std::vector<Eigen::Matrix4f> poses_out;
            std::vector<size_t> order;

            if (amount_of_poses_to_look_for > 1) {

                std::vector<bool> keep = nonMaxSuppression<pcl::PointXYZ>(translations, densities, maxd, debug);
                order = core::sort(densities, false);

                size_t count = 0, i = 0;

                while (i < amount_of_poses_to_look_for && count < densities.size()) {

                    if (keep[order[count]]) {
                        poses_out.push_back(poses[order[count]]);
                        i++;

                    }
                   
                    count++;
                }
            } else {
                order = core::sort(densities, false);
                poses_out.push_back(poses[order[0]]);
            }
            return poses_out;
        }

        template<typename T>
        Eigen::Matrix4f computePose(T sp1, T sp2, T sp3, T qp1, T qp2, T qp3) {

            typename pcl::PointCloud<T> point_target, point_source;
            point_source.push_back(qp1);
            point_source.push_back(qp2);
            point_source.push_back(qp3);

            point_target.push_back(sp1);
            point_target.push_back(sp2);
            point_target.push_back(sp3);

            typename pcl::registration::TransformationEstimationSVD<T, T> est;

            Eigen::Matrix4f result;
            est.estimateRigidTransformation(point_source, point_target, result);

            return result;
        }

        //*******COMPUTE POSES*************************************************************************************************
        template<typename T>
        std::vector<Eigen::Matrix4f>
        computeAllPoses(typename pcl::PointCloud<T>::Ptr query, typename pcl::PointCloud<T>::Ptr target,
                        std::vector<std::vector<std::vector<std::pair<covis::core::PTF_Index, covis::core::PTF_Index> > > > voting_array_for_poses,
                        core::Correspondence::Vec best, float maxd) {

            std::vector<Eigen::Matrix4f> poses;
            for (size_t p = 0; p < best.size(); p++) {
                std::vector<std::pair<covis::core::PTF_Index, covis::core::PTF_Index> > max_triplet;
                max_triplet = voting_array_for_poses[best[p].query][best[p].match[0]];

                Eigen::Matrix4f pose;
                std::vector<Eigen::Matrix4f> poses_for_this_point;
                for (size_t t = 0; t < max_triplet.size(); t++) {
                    T sp1 = (*target)[max_triplet[t].first.idx1];
                    T sp2 = (*target)[max_triplet[t].first.idx2];
                    T sp3 = (*target)[max_triplet[t].first.idx3];

                    T qp1 = (*query)[max_triplet[t].second.idx1];
                    T qp2 = (*query)[max_triplet[t].second.idx2];
                    T qp3 = (*query)[max_triplet[t].second.idx3];
                    Eigen::Matrix4f current_pose = computePose<T>(sp1, sp2, sp3, qp1, qp2, qp3);
                    poses_for_this_point.push_back(current_pose);
                }
                if (max_triplet.size() == 0) continue;

                std::vector<Eigen::Matrix4f> pos = clusterPoses(poses_for_this_point, 1, maxd * 2, false);
                poses.push_back(pos[0]);
            }
            return poses;
        }

        //Sets array values to 0
        void setArrayToEmpty(std::vector<std::vector<float> > &voting_array,
                            std::vector<std::vector<std::vector<std::pair<covis::core::PTF_Index, covis::core::PTF_Index> > > > &array_for_backtracking_poses,
                             int target_size, int query_size) {

            for (int p = 0; p < target_size; p++) {
               std::vector<float> curr_pair;
               std::vector<std::vector<std::pair<covis::core::PTF_Index, covis::core::PTF_Index> > > c;
                for (int v = 0; v < query_size; v++) {
                    curr_pair.push_back(0.0f);
                    std::vector<std::pair<covis::core::PTF_Index, covis::core::PTF_Index> > t;
                    c.push_back(t);
                }
                voting_array.push_back(curr_pair);
                array_for_backtracking_poses.push_back(c);
            }
        }

        //***GET VOTING ARRAY and CORRESPONDENCEstd::vector****
        void loopThroughTargetPTFSfindMatchAndVote(pcl::PointCloud<covis::core::PTF> ptfs_target, pcl::PointCloud<covis::core::PTF_Index> ptfs_target_idx,
                                                   detect::FeatureSearch<covis::core::PTF> &fs, pcl::PointCloud<covis::core::PTF_Index> ptfs_query_idx,
                                                   float radius, int target_size, int query_size, bool debug,
                                                   int minVotes, bool ransac, bool pose_clustering,
                                                   std::vector<std::vector<std::vector<std::pair<covis::core::PTF_Index, covis::core::PTF_Index> > > > &array_for_backtracking_poses,
                                                   core::Correspondence::Vec &corrRansac,
                                                   core::Correspondence::Vec &corrPoseClustering) {

            std::vector<std::vector<float> > voting_array;
            setArrayToEmpty(voting_array, array_for_backtracking_poses, target_size, query_size);


            int maximumPossibleVotesArray[target_size]; // array used for vote normalization
            std::fill(maximumPossibleVotesArray, maximumPossibleVotesArray + target_size, 0); // set it to 0


            core::ProgressDisplay::Ptr pd;
            if (debug) pd.reset(new core::ProgressDisplay(ptfs_target.size()));
            for (size_t i = 0; i < ptfs_target.size(); i++) {
                if (debug) ++(*pd);


                covis::core::PTF curr_ptf = (ptfs_target)[i];
                covis::core::PTF_Index curr_ptf_idx = (ptfs_target_idx)[i];

                core::Correspondence foundCorrespondences = fs.radius(curr_ptf, radius);

                if (foundCorrespondences.size() < 1) continue;
                for (size_t j = 0; j < foundCorrespondences.match.size(); j++) {
                    // get QUERY indexes of the found matches
                    int s_id1 = (ptfs_query_idx)[foundCorrespondences.match[j]].idx1;
                    int s_id2 = (ptfs_query_idx)[foundCorrespondences.match[j]].idx2;
                    int s_id3 = (ptfs_query_idx)[foundCorrespondences.match[j]].idx3;

                    const float accu = 1;//(1+sqrtf(foundCorrespondences.distance[j]));
                    voting_array[curr_ptf_idx.idx1][s_id1] = voting_array[curr_ptf_idx.idx1][s_id1] + accu;
                    voting_array[curr_ptf_idx.idx2][s_id2] = voting_array[curr_ptf_idx.idx2][s_id2] + accu;
                    voting_array[curr_ptf_idx.idx3][s_id3] = voting_array[curr_ptf_idx.idx3][s_id3] + accu;

                    // to backtrace triplets
                    std::pair<covis::core::PTF_Index, covis::core::PTF_Index> scene_query_corr;
                    scene_query_corr.first = curr_ptf_idx;
                    scene_query_corr.second = (ptfs_query_idx)[foundCorrespondences.match[j]];

                    array_for_backtracking_poses[curr_ptf_idx.idx1][s_id1].push_back(scene_query_corr);
                    array_for_backtracking_poses[curr_ptf_idx.idx2][s_id2].push_back(scene_query_corr);
                    array_for_backtracking_poses[curr_ptf_idx.idx3][s_id3].push_back(scene_query_corr);

                }

                maximumPossibleVotesArray[curr_ptf_idx.idx1] = maximumPossibleVotesArray[curr_ptf_idx.idx1] + 1;
                maximumPossibleVotesArray[curr_ptf_idx.idx2] = maximumPossibleVotesArray[curr_ptf_idx.idx2] + 1;
                maximumPossibleVotesArray[curr_ptf_idx.idx3] = maximumPossibleVotesArray[curr_ptf_idx.idx3] + 1;
            }


            if (pose_clustering) {
                //make a correspondencestd::vector of the size: TARGET_SIZE*QUERY_SIZE
                for (int i = 0; i < target_size; i++) {
                    core::Correspondence c(1);
                    c.query = i;
                    for (int iq = 0; iq < query_size; iq++) {
                        c.match[0] = iq;
                        c.distance[0] = 1 - voting_array[i][iq] / float(maximumPossibleVotesArray[i]);
                        if (maximumPossibleVotesArray[i] < minVotes || voting_array[i][iq] < minVotes) continue;
                        corrPoseClustering.push_back(c);
                    }
                }
            }
            if (ransac) {
                for (int i = 0; i < target_size; i++) {
                    covis::core::Correspondence c(1);
                    c.query = i;
                    float max = 0;
                    int maxId = 0;
                    for (int iq = 0; iq < query_size; iq++) {
                        if (voting_array[i][iq] > max) {
                            max = voting_array[i][iq];
                            maxId = iq;
                        }
                    }
                    c.match[0] = maxId;
                    c.distance[0] = 1 - max / float(maximumPossibleVotesArray[i]);
                    corrRansac.push_back(c);

                }
            }
        }

        template <typename PointN>
        Eigen::Matrix4f doRansac(typename pcl::PointCloud<PointN>::Ptr obj,
                                 typename pcl::PointCloud<PointN>::Ptr scn,
                                 covis::core::Correspondence::Vec corr, int maxIter, float inlierThr) {

            pcl::CorrespondencesPtr pclc = core::convert(corr);
            pcl::CorrespondencesPtr corrOut(new pcl::Correspondences());
            pcl::registration::CorrespondenceRejectorSampleConsensus<PointN> crsc;
            crsc.setInputTarget(obj);
            crsc.setInputSource(scn);
            crsc.setMaximumIterations(maxIter);
            crsc.setInlierThreshold(inlierThr);
            crsc.setSaveInliers(true);
            crsc.setRefineModel(true);
            crsc.getRemainingCorrespondences(*pclc, *corrOut);
            Eigen::Matrix4f transformation = crsc.getBestTransformation();

            return transformation.inverse();
        }



        //***MATCH VOTING***********************************************************************************************
        template<typename PointInT>
        inline void PTFMatching<PointInT>::match() {

            detect::FeatureSearch<covis::core::PTF> fs(1);

            pcl::PointCloud<covis::core::PTF>::Ptr ptfs_query_ptr(new pcl::PointCloud<covis::core::PTF>());
            pcl::copyPointCloud<covis::core::PTF>(this->_ptfs_query, *ptfs_query_ptr);

            fs.setChecks(this->_checks);
            fs.setTarget(ptfs_query_ptr);

            if (this->_debug) {
                COVIS_MSG_WARN(
                        "Scene size: " << this->_ptfs_target.size() << " Query size: " << this->_ptfs_query.size());
                COVIS_MSG_INFO("Used PTF dimentions for QUERY: " << pcl::getFieldsList(_ptfs_query).c_str());
                COVIS_MSG_INFO("Used PTF dimentions for TARGET: " << pcl::getFieldsList(_ptfs_target).c_str());
            }
           std::vector<std::vector<std::vector<std::pair<covis::core::PTF_Index, covis::core::PTF_Index> > > > array_for_backtracking_poses;

            loopThroughTargetPTFSfindMatchAndVote(this->_ptfs_target, this->_ptfs_idx_target, fs, this->_ptfs_idx_query,
                                                  this->_search_radius, this->_target->size(), this->_query->size(),
                                                  this->_debug,
                                                  2, // at least 2 votes needs to be to accept correspondence
                                                  this->_use_ransac, this->_use_pose_clustering,
                                                  array_for_backtracking_poses, this->_corr_ransac, this->_corr_pose_clustering);


            size_t corr_pose_clustering_size = this->_corr_pose_clustering.size();
            size_t corr_ransac_size = this->_corr_ransac.size();

            if (this->_debug) {

                COVIS_MSG_WARN(
                        "Set to find Ransac " << this->_use_ransac << ", pose clustering: " << this->_use_pose_clustering);
                COVIS_MSG_WARN("Correspondence RANSAC size: " << corr_ransac_size << ", POSE CLUSTERING size: "
                                                              << corr_pose_clustering_size);
                COVIS_MSG_WARN("Set to find " << this->_number_of_poses << " object pose(s)");
            }
            //--PoseClustering pose estimation---
            if (this->_use_pose_clustering) {

                COVIS_ASSERT_MSG(corr_pose_clustering_size >= this->_query->size(),
                                 "corrPoseClustering should at least be " << this->_query->size() << ", but it is: "
                                                                          << corr_pose_clustering_size << "using smaller nu,ber, migh degrate detection precision!");

                core::Correspondence::Vec best_poses;
                best_poses = this->_corr_pose_clustering;
                core::sort(best_poses);
                size_t corr_size = this->_query->size()*this->_number_of_poses;
                if (corr_pose_clustering_size >= corr_size) best_poses.resize(corr_size);
                // use only qyery_size best poses for pose voting and clustering

                if (this->_debug) COVIS_MSG_INFO("Started to compute all poses per k");
                std::vector<Eigen::Matrix4f> poses = computeAllPoses<PointInT>(this->_query, this->_target,
                                                                               array_for_backtracking_poses, best_poses,
                                                                               this->_max_distance);
                if (this->_debug) COVIS_MSG_INFO("Done with computing poses, starting pose clustering");

                std::vector<Eigen::Matrix4f> sorted_poses = clusterPoses(poses, this->_number_of_poses,
                                                                         this->_max_distance * 2, this->_debug);

                this->_poses_from_pose_clustering = sorted_poses;
            }

            //--RANSAC pose estimation---
            if (this->_use_ransac) {
                this->_pose_from_ransac = doRansac<PointInT>(this->_query, this->_target, this->_corr_ransac, 50000, 0.01);
            }

        }
    }
}
/// @endcond

#endif //COVIS_DETECT_PTF_MATCHING_IMPL_HPP