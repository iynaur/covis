// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_DETECT_RECOGNITION_H
#define COVIS_DETECT_RECOGNITION_H

// Own
#include "../feature.h"
#include "../filter/point_cloud_preprocessing.h"
#include "eigen_search.h"
#include "fit_evaluation.h"
#include "pose_voting.h"
#include "point_search.h"
#include "ransac.h"

namespace covis {
    namespace detect {
        /**
         * @ingroup detect
         * @class Recognition
         * @brief 3D detection and 6D pose estimation of one or more objects in point cloud scenes
         *
         * Set a number of point cloud object models (@ref setSources()) and their features (@ref setSourceFeatures())
         * and a target scene (@ref setTarget() and @ref setTargetFeatures()). Optionally set some parameters and then
         * run the recognition using @ref recognize(). Have a look at the example below for a full code example with
         * visualizations.
         * 
         * @tparam PointT point type
         * @author Anders Glent Buch
         * 
         * @example example/recognition/recognition.cpp
         */
        template<typename PointT>
        class Recognition : public DetectBase {
            public:
                /// Underlying detector to use for pose estimation of the individual objects during recognition
                enum DETECTOR {
                    RANSAC,
                    VOTING
                };

                /// Specifies which method to use during verification or removal of detections
                enum VERIFIER {
                    NONE, ///< Accept all hypotheses
                    GREEDY, ///< Greedy verification, just segment out the models one by one
                    CONFLICT, ///< Papazov et al.'s conflict graph based verification @cite papazov2010efficient
                    GLOBAL ///< Aldoma et al.'s global hypothesis verication @cite aldoma2012global
                };

                /**
                 * Constructor: set default parameters
                 */
                Recognition() :
                    _knn(0),
                    _searchTrees(4),
                    _searchChecks(512),
                    _correspondenceFraction(1),
                    _detector(RANSAC),
                    _multiInstance(1),
                    _refinementIterations(50),
                    _multiscaleRefinement(true),
                    _verifier(GREEDY),
                    _ransacIterations(1000),
                    _prereject(false),
                    _prerejectionSimilarity(0.9),
                    _inlierThreshold(0.01),
                    _inlierFraction(0),
                    _fullEvaluation(false),
                    _bwt(0),
                    _bwrot(22.5 * M_PI / 180.0),
                    _tessellation(6 * M_PI / 180.0),
                    _kdeThreshold(0),
                    _verbose(false) {}
                
                /**
                 * Run the recognizer
                 * @return object detections
                 */
                const core::Detection::Vec& recognize();

                /**
                 * Set the source point cloud for the object to recognize
                 * @param source source point cloud
                 */
                inline void setSource(typename pcl::PointCloud<PointT>::Ptr source) {
                    _sources = std::vector<typename pcl::PointCloud<PointT>::Ptr>(1, source);
                }

                /**
                 * Set the source point clouds for the objects to recognize
                 * @param sources source point clouds
                 */
                inline void setSources(std::vector<typename pcl::PointCloud<PointT>::Ptr> sources) {
                    _sources = sources;
                    _sourceResolutions.resize(sources.size());
                    _sources1.resize(sources.size());
                    _sources2.resize(sources.size());
                    _sources4.resize(sources.size());
                    for(size_t i = 0; i < sources.size(); ++i) {
                        _sourceResolutions[i] = 0.01 * computeDiagonal<PointT>(sources[i]);
                        _sources1[i] = filter::downsample<PointT>(_sources[i], _sourceResolutions[i]);
                        _sources2[i] = filter::downsample<PointT>(_sources[i], 2 * _sourceResolutions[i]);
                        _sources4[i] = filter::downsample<PointT>(_sources[i], 4 * _sourceResolutions[i]);
                    }
                    _mapSource.clear();
                }

                /**
                 * Get the source point clouds
                 * @return source point clouds
                 */
                inline const std::vector<typename pcl::PointCloud<PointT>::Ptr>& getSources() const {
                    return _sources;
                }

                /**
                 * Set these if you want to use higher-resolution object models for the final global verification step
                 * @param sourcesVerification source models for hypothesis verification
                 */
                inline void setSourcesVerification(std::vector<typename pcl::PointCloud<PointT>::Ptr> sourcesVerification) {
                    _sourcesVerification = sourcesVerification;
                }

                /**
                 * Set the target point cloud
                 * @note this function also clears any existing feature correspondences
                 * specified by @ref setFeatureCorrespondences()
                 * @param target target point cloud
                 */
                inline void setTarget(typename pcl::PointCloud<PointT>::ConstPtr target) {
                    _target = target;
                    _featureCorrespondences.clear();
                }

                /**
                 * Get the target point cloud
                 * @return target point cloud
                 */
                inline typename pcl::PointCloud<PointT>::ConstPtr getTarget() const {
                    return _target;
                }

                /**
                 * Set this if you want to use a higher-resolution scene model for the final global verification step
                 * @param targetVerification target model for hypothesis verification
                 */
                inline void setTargetVerification(typename pcl::PointCloud<PointT>::ConstPtr targetVerification) {
                    _targetVerification = targetVerification;
                }

                /**
                 * Set the source feature point clouds for the objects to recognize
                 * @note if you use @ref setFeatureCorrespondences, you don't need to use this function
                 * @param sourceFeatures source feature point clouds
                 */
                inline void setSourceFeatures(const feature::MatrixT& sourceFeatures) {
                    _sourceFeatures = sourceFeatures;
                }

                /**
                 * Get the source feature point clouds
                 * @return source feature point clouds
                 */
                inline const feature::MatrixT& getSourceFeatures() const {
                    return _sourceFeatures;
                }

                /**
                 * Set target features
                 * @note if you use @ref setFeatureCorrespondences, you don't need to use this function
                 * @param targetFeatures target features
                 */
                inline void setTargetFeatures(const feature::MatrixT& targetFeatures) {
                    _targetFeatures = targetFeatures;
                }

                /**
                 * Get target features
                 * @return target features
                 */
                inline const feature::MatrixT& getTargetFeatures() const {
                    return _targetFeatures;
                }

                /**
                 * Instead of providing the features for matching (@ref setSourceFeatures, @ref setTargetFeatures), you
                 * can directly provide target --> source correspondences here
                 * @note the correspondences provided here must index into a single big vector of source points, which is
                 * constructed by concatenating all models set in @ref setSource()
                 * @warning you must call this function after @ref setTarget()
                 * @param featureCorrespondences correspondences from target to sources
                 */
                inline void setFeatureCorrespondences(const core::Correspondence::Vec& featureCorrespondences) {
                    COVIS_ASSERT_MSG(_target, "setTarget() not called before calling setFeatureCorrespondences()!");
                    _featureCorrespondences = featureCorrespondences;
                }

                /**
                 * Get feature correspondences
                 * @return feature correspondences
                 */
                inline const core::Correspondence::Vec& getFeatureCorrespondences() const {
                    return _featureCorrespondences;
                }

                /**
                 * Set the names of the objects, otherwise some auto-generated names are used
                 * @param sourceNames object names
                 */
                inline void setSourceNames(const std::vector<std::string>& sourceNames) {
                    _sourceNames = sourceNames;
                }

                /**
                 * Get the names of the objects
                 * @return object names
                 */
                inline const std::vector<std::string>& getSourceNames() const {
                    return _sourceNames;
                }

                /**
                 * Set number of neighbors to find during feature search (set to zero for automatic)
                 * @param knn number of nearest neighbors
                 */
                inline void setKnn(size_t knn) {
                    _knn = knn;
                }

                /**
                 * Set number of randomized k-d trees to use for feature search
                 * @param searchTrees number of trees
                 */
                inline void setSearchTrees(size_t searchTrees) {
                    _searchTrees = searchTrees;
                }

                /**
                 * Set number of iterations to use during feature search
                 * @param searchChecks number of iterations
                 */
                inline void setSearchChecks(size_t searchChecks) {
                    _searchChecks = searchChecks;
                }

                /**
                 * Set the fraction [0,1] of best feature correspondences to pass to RANSAC
                 * @param correspondenceFraction correspondence fraction
                 */
                inline void setCorrespondenceFraction(double correspondenceFraction) {
                    _correspondenceFraction = correspondenceFraction;
                }

                /**
                 * Set the detector
                 * @sa @ref DETECTOR
                 * @param detector detector
                 */
                inline void setDetector(DETECTOR detector) {
                    _detector = detector;
                }

                /**
                 * Get the detector
                 * @return detector
                 */
                inline DETECTOR getDetector() const {
                    return _detector;
                }

                /**
                 * Set number of possible instances of each object to find
                 * @param multiInstance multi instance count
                 */
                inline void setMultiInstance(size_t multiInstance) {
                    _multiInstance = MAX(1, multiInstance);
                }

                /**
                 * Get number of instances to find
                 * @return number of instances to find
                 */
                inline size_t getMultiInstance() const {
                    return _multiInstance;
                }

                /**
                 * Set the number of iterations for pose refinement
                 * @param refinementIterations number of iterations for pose refinement
                 */
                inline void setRefinementIterations(size_t refinementIterations) {
                    _refinementIterations = refinementIterations;
                }

                /**
                 * Get the number of iterations for pose refinement
                 * @return number of iterations for pose refinement
                 */
                inline size_t getRefinementIterations() const {
                    return _refinementIterations;
                }

                /**
                 * Set the multi-scale refinement flag
                 * @param multiscaleRefinement multi-scale refinement flag
                 */
                inline void setMultiscaleRefinement(bool multiscaleRefinement) {
                    _multiscaleRefinement = multiscaleRefinement;
                }

                /**
                 * Get the multi-scale refinement flag
                 * @return multi-scale refinement flag
                 */
                inline bool getMultiscaleRefinement() const {
                    return _multiscaleRefinement;
                }

                /**
                 * Set the verification method
                 * @sa @ref VERIFIER
                 * @param verifier verifier
                 */
                inline void setVerifier(VERIFIER verifier) {
                    _verifier = verifier;
                }

                /**
                 * Number of iterations for the RANSAC detector
                 * @param ransacIterations number of RANSAC iterations
                 */
                inline void setRansacIterations(size_t ransacIterations) {
                    _ransacIterations = ransacIterations;
                }

                /**
                 * Get number of RANSAC iterations
                 * @return number of RANSAC iterations
                 */
                inline size_t getRansacIterations() const {
                    return _ransacIterations;
                }

                /**
                 * Enable/disable prerejection for the RANSAC detector
                 * @param prereject prerejection flag
                 */
                inline void setPrereject(bool prereject) {
                    _prereject = prereject;
                }

                /**
                 * Get prerejection flag
                 * @return prerejection flag
                 */
                inline bool getPrereject() const {
                    return _prereject;
                }

                /**
                 * Get prerejection similarity
                 * @return prerejection similarity
                 */
                inline float getPrerejectionSimilarity() const {
                    return _prerejectionSimilarity;
                }

                /**
                 * Set prerejection similarity for the RANSAC detector
                 * @param prerejectionSimilarity prerejection similarity
                 */
                inline void setPrerejectionSimilarity(float prerejectionSimilarity) {
                    _prerejectionSimilarity = prerejectionSimilarity;
                }
                
                /**
                 * Set Euclidean inlier threshold
                 * @param inlierThreshold Euclidean inlier threshold
                 */
                inline void setInlierThreshold(float inlierThreshold) {
                    _inlierThreshold = inlierThreshold;
                }

                /**
                 * Get inlier threshold
                 * @return inlier threshold
                 */
                inline float getInlierThreshold() const {
                    return _inlierThreshold;
                }
                
                /**
                 * Set required inlier fraction (must be in [0,1])
                 * @param inlierFraction required inlier fraction
                 */
                inline void setInlierFraction(float inlierFraction) {
                    _inlierFraction = inlierFraction;
                }

                /**
                 * Get inlier fraction
                 * @return inlier fraction
                 */
                inline float getInlierFraction() const {
                    return _inlierFraction;
                }

                /**
                 * Set whether to use full surface-based evaluation of hypothesis poses during RANSAC
                 * @param fullEvaluation full evaluation flag
                 */
                inline void setFullEvaluation(bool fullEvaluation) {
                    _fullEvaluation = fullEvaluation;
                }

                /**
                 * Set translation bandwidth for the voting-based detector
                 * @param bwt translation bandwidth
                 */
                inline void setTranslationBandwidth(double bwt) {
                    _bwt = bwt;
                }

                /**
                 * Get translation bandwidth
                 * @return translation bandwidth
                 */
                inline double getTranslationBandwidth() const {
                    return _bwt;
                }

                /**
                 * Set rotation bandwidth for the voting-based detector
                 * @param bwrot rotation bandwidth [rad] between 0 and pi
                 */
                inline void setRotationBandwidth(double bwrot) {
                    _bwrot = bwrot;
                }

                /**
                 * Get rotation bandwidth
                 * @return rotation bandwidth
                 */
                inline double getRotationBandwidth() const {
                    return _bwrot;
                }

                /**
                 * Set tessellation level for the voting-based detector
                 * @param tessellation tessellation level
                 */
                inline void setTessellation(double tessellation) {
                    _tessellation = tessellation;
                }

                /**
                 * Set lower KDE threshold for accepting a voting-based detection
                 * @param kdeThreshold KDE threshold
                 */
                inline void setKdeThreshold(double kdeThreshold) {
                    _kdeThreshold = kdeThreshold;
                }

                /**
                 * Get lower KDE threshold
                 * @return KDE threshold
                 */
                inline double getKdeThreshold() const {
                    return _kdeThreshold;
                }

                /**
                 * Set verbose flag for printing
                 * @param verbose verbose flag
                 */
                inline void setVerbose(bool verbose) {
                    _verbose = verbose;
                }

                /**
                 * Get verbose flag
                 * @return verbose flag
                 */
                inline bool getVerbose() const {
                    return _verbose;
                }
                
            private:
                /// Cloud type
                typedef typename pcl::PointCloud<PointT> CloudT;

                /// Source point clouds to be placed into @ref _target
                std::vector<typename CloudT::Ptr> _sources;

                /// Point spacings on the source models used for multi-scale refinement
                std::vector<float> _sourceResolutions;

                /// Sources at specified resolution
                std::vector<typename CloudT::Ptr> _sources1;

                /// Sources at half resolution
                std::vector<typename CloudT::Ptr> _sources2;

                /// Sources at quarter resolution
                std::vector<typename CloudT::Ptr> _sources4;

                /// Verification models for objects
                std::vector<typename CloudT::Ptr> _sourcesVerification;

                /// Target point cloud
                typename CloudT::ConstPtr _target;

                /// Verification model for scene
                typename CloudT::ConstPtr _targetVerification;

                /// Source features
                feature::MatrixT _sourceFeatures;

                /// Target features
                feature::MatrixT _targetFeatures;

                /// Feature correspondences from target to sources
                core::Correspondence::Vec _featureCorrespondences;

                /// Optional object names
                std::vector<std::string> _sourceNames;

                /// Number of neighbors to find during feature search
                size_t _knn;

                /// Number of randomized trees to use during feature search
                size_t _searchTrees;

                /// Number of iterations to use during feature search
                size_t _searchChecks;

                /// Fraction of feature correspondences to use during detection, in ]0,1]
                double _correspondenceFraction;

                /// Detector type to use
                DETECTOR _detector;

                /// Number of instances of each object to search for
                size_t _multiInstance;

                /// Number of iterations for pose refinement
                size_t _refinementIterations;

                /// Set to true to use multi-scale ICP for refinement
                bool _multiscaleRefinement;

                /// Verification method to use
                VERIFIER _verifier;

                /// Number of iterations for the RANSAC detector
                size_t _ransacIterations;

                /// Prerejection flag for the RANSAC detector
                bool _prereject;

                /// Prerejection similarity for the RANSAC detector
                float _prerejectionSimilarity;
                
                /// Euclidean inlier threshold
                float _inlierThreshold;
                
                /// Visibility or required fraction of inliers of the source points [0,1]
                float _inlierFraction;

                /// Flag determining whether to use full surface-based evaluation of hypothesis poses during RANSAC
                bool _fullEvaluation;

                /// Translation bandwidth for the voting-based detector
                double _bwt;

                /// Rotation bandwidth for the voting-based detector
                double _bwrot;

                /// Tessellation of the unknown final rotation angle around the normal
                double _tessellation;

                /// Lower KDE threshold
                double _kdeThreshold;
                
                /// Verbose flag
                bool _verbose;

                /// All detections found by the last call to @ref recognize()
                core::Detection::Vec _detections;

                /// Index mapping from global source index into seperate source models
                std::vector<std::pair<size_t, size_t> > _mapSource;

                /// Search index for feature matching
                EigenSearch<> _fsearch;
        };
    }
}

#include "recognition_impl.hpp"

#endif
