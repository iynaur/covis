// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_DETECT_FIT_EVALUATION_H
#define COVIS_DETECT_FIT_EVALUATION_H

// Own
#include "detect_base.h"
#include "point_search.h"
#include "../core/correspondence.h"
#include "../core/detection.h"

// Boost
#include <boost/shared_ptr.hpp>

// STL
#include <cmath>

// PCL
#include <pcl/common/io.h>

namespace covis {
    namespace detect {
        /**
         * @ingroup detect
         * @class FitEvaluation
         * @brief Evaluate the relative pose between two point clouds based on different criteria
         * 
         * Use this class to repeatedly evaluate a number of pose hypothesis between one or more object/query models and
         * a fixed scene/target model. The target model is assumed static, and must be input to this class, and then it
         * is a matter of using the @ref update() function with the current pose hypothesis in each iteration of your
         * loop, followed by one or more evaluation functions, e.g. @ref inliers() or @ref penalty().
         * 
         * Do not forget to specify the main parameter, the inlier threshold, using @ref setInlierThreshold().
         * 
         * @tparam PointT point type for source and target
         * @author Anders Glent Buch
         */
        template<typename PointT>
        class FitEvaluation : public DetectBase {
            public:
                /// Enum for the penalty function used by @ref penalty()
                enum PENALTY {
                    INLIERS,             //!< INLIERS 1 - inlier fraction
                    OUTLIERS,            //!< OUTLIERS outlier fraction
                    RMSE,                //!< RMSE RMS error of inliers, normalized by inlier threshold
                    INLIERS_OUTLIERS,    //!< INLIERS_OUTLIERS (1-inlier)+outlier fraction
                    INLIERS_RMSE,        //!< INLIERS_RMSE (1-inlier) fraction + their RMSE
                    OUTLIERS_RMSE,       //!< OUTLIERS_RMSE outlier fraction + normalized RMS error of inliers
                    INLIERS_OUTLIERS_RMSE//!< INLIERS_OUTLIERS_RMSE combination of all three
                };
                
                /// Pointer type
                typedef boost::shared_ptr<FitEvaluation<PointT> > Ptr;
                
                /// Matrix type, just a forward of @ref core::Detection::MatrixT
                typedef core::Detection::MatrixT MatrixT;
                
                /// Empty constructor
                FitEvaluation() :
                    _inlierThreshold(-1.0f),
                    _inlierThresholdSquared(-1.0f),
                    _occlusionReasoning(false),
                    _viewAxis(2),
                    _penalty(INLIERS) {}
                
                /**
                 * Constructor: set input target and create a search index
                 * @sa @ref setTarget()
                 * @param target point cloud
                 */
                FitEvaluation(typename pcl::PointCloud<PointT>::ConstPtr target) :
                    _inlierThreshold(-1.0f),
                    _inlierThresholdSquared(-1.0f),
                    _occlusionReasoning(false),
                    _viewAxis(2),
                    _penalty(INLIERS) {
                    setTarget(target);
                }
                
                /**
                 * Constructor: directly set the search index
                 * @sa @ref setSearch()
                 * @param search search index
                 */
                FitEvaluation(typename PointSearch<PointT>::Ptr search) :
                    _inlierThreshold(-1.0f),
                    _inlierThresholdSquared(-1.0f),
                    _occlusionReasoning(false),
                    _viewAxis(2),
                    _penalty(INLIERS) {
                    setSearch(search);
                }
                
                /// Destructor
                virtual ~FitEvaluation() {}
                
                /**
                 * Update function: apply an input pose hypothesis to a source point cloud and call
                 * @ref update(typename pcl::PointCloud<PointT>::ConstPtr source) "update()"
                 * @param source source point cloud
                 * @param pose hypothesis pose
                 * @return all point correspondences between source and target
                 */
                core::Correspondence::VecPtr update(typename pcl::PointCloud<PointT>::ConstPtr source,
                        const MatrixT& pose);
                
                /**
                 * Update function: assume the source is already aligned to the scene, use the internal search tree to
                 * find point correspondences, and perform the following evaluations:
                 *   - Inlier count: the absolute (@ref inliers()) and relative
                 *    (@ref inlierFraction()) number of point matches up to the inlier threshold
                 *   - (R)MSE: the (root) mean squared error (@ref rmse() and @ref mse())
                 * @param source source point cloud, assumed aligned to the scene according to a pose hypothesis
                 * @return all point correspondences between source and target
                 */
                core::Correspondence::VecPtr update(typename pcl::PointCloud<PointT>::ConstPtr source);
                
                /**
                 * Apply a pose hypothesis to the source and evaluate the fit, in this case using precomputed point
                 * correspondences, e.g. from feature matching. This function applies the pose to the source, and then
                 * calls @ref update(typename pcl::PointCloud<PointT>::ConstPtr, core::Correspondence::VecConstPtr)
                 * @warning Consider disabling occlusion reasoning (@ref setOcclusionReasoning()) when using this function,
                 * since occlusion reasoning works best when the internal 2D search object of this class is employed, as in 
                 * @ref update(typename pcl::PointCloud<PointT>::ConstPtr source, const MatrixT& pose) and
                 * @ref update(typename pcl::PointCloud<PointT>::ConstPtr source)
                 * @param source source point cloud
                 * @param pose hypothesis pose
                 * @param correspondences point correspondences source --> target
                 */
                void update(typename pcl::PointCloud<PointT>::ConstPtr source,
                        const MatrixT& pose,
                        core::Correspondence::VecConstPtr correspondences);
                
                /**
                 * Update function: assume the source is already aligned to the scene, use the input point
                 * correspondences, and perform the following evaluations:
                 *   - Inlier count: the absolute (@ref inliers()) and relative
                 *    (@ref inlierFraction()) number of point matches up to the inlier threshold
                 *   - (R)MSE: the (root) mean squared error (@ref rmse() and @ref mse())
                 * @warning Consider disabling occlusion reasoning (@ref setOcclusionReasoning()) when using this function,
                 * since occlusion reasoning works best when the internal 2D search object of this class is employed, as in 
                 * @ref update(typename pcl::PointCloud<PointT>::ConstPtr source, const MatrixT& pose) and
                 * @ref update(typename pcl::PointCloud<PointT>::ConstPtr source)
                 * @param source source point cloud
                 * @param correspondences point correspondences source --> target
                 */
                void update(typename pcl::PointCloud<PointT>::ConstPtr source,
                        core::Correspondence::VecConstPtr correspondences);
                
                /**
                 * Set the target point cloud and create a search index for it
                 * @param target target point cloud
                 */
                inline void setTarget(typename pcl::PointCloud<PointT>::ConstPtr target) {
                    _target = target;
                    if(!_search)
                        _search.reset(new PointSearch<PointT>);
                    if(_search->getTarget() != _target)
                        _search->setTarget(_target);

                    pcl::PointCloud<pcl::PointXY>::Ptr target2d(new pcl::PointCloud<pcl::PointXY>);
                    switch(_viewAxis) {
                        case 0: // x
                            target2d->resize(_target->size());
                            for(size_t i = 0; i < _target->size(); ++i) {
                                target2d->points[i].x = _target->points[i].y;
                                target2d->points[i].y = _target->points[i].z;
                            }
                            break;
                        case 1: // y
                            target2d->resize(_target->size());
                            for(size_t i = 0; i < _target->size(); ++i) {
                                target2d->points[i].x = _target->points[i].z;
                                target2d->points[i].y = _target->points[i].x;
                            }
                            break;
                        default: // z
                            pcl::copyPointCloud(*_target, *target2d);
                    }
                    _search2d.reset(new PointSearch<pcl::PointXY>);
                    _search2d->setTarget(target2d);
                }
                
                /**
                 * Directly set the search object, the search must be initialized with _target
                 * @param search search index
                 */
                inline void setSearch(typename PointSearch<PointT>::Ptr search) {
                    _search = search;
                    setTarget(search->getTarget());
                }
                
                /**
                 * Get the search object, mutable for outside use
                 * @return search index
                 */
                inline typename PointSearch<PointT>::Ptr getSearch() {
                    return _search;
                }
                
                /**
                 * Set the Euclidean distance determining the inlier threshold
                 * @param inlierThreshold Euclidean (non-squared) inlier threshold
                 */
                inline void setInlierThreshold(float inlierThreshold) {
                    COVIS_ASSERT(inlierThreshold > 0.0f);
                    _inlierThreshold = inlierThreshold;
                    _inlierThresholdSquared = inlierThreshold * inlierThreshold;
                }
                
                /**
                 * @sa @ref PENALTY, @ref penalty()
                 * @param penalty
                 */
                inline void setPenaltyType(PENALTY penalty) {
                    _penalty = penalty;
                }
                
                /**
                 * Enable removal of occluded points - be sure to only use this flag if you have target scenes with a well-defined viewpoint
                 * By default, this class assumes the third (z) axis to be the view axis. If this is not the case for your data,
                 * use @ref setViewAxis() to change this
                 * @param occlusionReasoning occlusion
                 */
                inline void setOcclusionReasoning(bool occlusionReasoning) {
                    _occlusionReasoning = occlusionReasoning;
                }
                
                /**
                 * Specify which of the three sensor axes points in the viewing direction - typically this is the z-axis
                 * @param viewAxis 0 for x, 1 for y, 2 for z
                 */
                inline void setViewAxis(int viewAxis) {
                    COVIS_ASSERT_MSG(viewAxis == 0 || viewAxis == 1 || viewAxis == 2, "View axis must be 0, 1 or 2!");
                    _viewAxis = viewAxis;
                }
                
                /**
                 * Get the inlier point correspondences
                 * @return inliers
                 */
                inline const core::Correspondence::Vec& getInliers() const {
                    return _state.inliers;
                }
                
                /**
                 * Get the inlier count
                 * @return inlier count
                 */
                inline size_t inliers() const {
                    return _state.inliers.size();
                }
                
                /**
                 * Get relative number of inliers to the total number of source points
                 * @return inlier fraction
                 */
                inline float inlierFraction() const {
                    return float(inliers()) / float(_state.size);
                }
                
                /**
                 * Get the squared fitness of the inliers
                 * @return mean squared Euclidean distance
                 */
                inline float mse() const {
                    return _state.mse;
                }
                
                /**
                 * Get the fitness of the inliers
                 * @return root mean squared Euclidean distance
                 */
                float rmse() const {
                    return sqrtf(mse());
                }
                
                /**
                 * Get the indices of occluded object points, only non-empty if occlusion removal is enabled
                 * @return occluded point indices
                 */
                inline const std::vector<size_t>& getOccluded() const {
                    return _state.occluded;
                }
                
                /**
                 * Get the number of occluded points, only non-zero if occlusion removal is enabled
                 * @return number of occluded points
                 */
                size_t occluded() const {
                    return _state.occluded.size();
                }
                
                /**
                 * Get relative number of occluded points to the total number of source points, only non-zero if occlusion removal is enabled
                 * @return occluded fraction
                 */
                inline float occludedFraction() const {
                    return float(occluded()) / float(_state.size);
                }
                
                /**
                 * Get the indices of outlier object points
                 * @return outlier point indices
                 */
                inline const std::vector<size_t>& getOutliers() const {
                    return _state.outliers;
                }
                
                /**
                 * Get the number of outlier points
                 * @return number of outlierpoints
                 */
                size_t outliers() const {
                    return _state.outliers.size();
                }
                
                /**
                 * Get relative number of outliers to the total number of source points
                 * @return outlier fraction
                 */
                inline float outlierFraction() const {
                    return float(outliers()) / float(_state.size);
                }
                
                /**
                 * Get a penalty in [0,1] for the detection, where 0 means the highest quality
                 * This function uses the penalty type set by @ref setPenaltyType() to produce a normalized
                 * penalty measure
                 * @sa @ref PENALTY, @ref setPenaltyType()
                 * @return penalty in [0,1] where lower is better
                 */
                inline float penalty() const {
                    switch(_penalty) {
                        case INLIERS:
                            return 1 - inlierFraction();
                        case OUTLIERS:
                            return outlierFraction();
                        case RMSE:
                            return rmse() / _inlierThreshold;
                        case INLIERS_OUTLIERS:
                            return 0.5 * (1 - inlierFraction() + outlierFraction());
                        case INLIERS_RMSE:
                            return 0.5 * (1 - inlierFraction() + rmse() / _inlierThreshold);
                        case OUTLIERS_RMSE:
                            return 0.5 * (outlierFraction() + rmse() / _inlierThreshold);
                        case INLIERS_OUTLIERS_RMSE:
                            return (1 - inlierFraction() + outlierFraction() + rmse() / _inlierThreshold) / 3;
                        default:
                            COVIS_THROW("Unknown penalty type: " << int(_penalty));
                    }
                }
                
            private:
                /// Target point cloud representing the scene
                typename pcl::PointCloud<PointT>::ConstPtr _target;
                
                /// Search index into @ref _target
                typename PointSearch<PointT>::Ptr _search;
                
                /// 2D search index into @ref _target, used for occlusion reasoning
                typename PointSearch<pcl::PointXY>::Ptr _search2d;
                
                /// Euclidean inlier threshold
                float _inlierThreshold;
                
                /// Euclidean inlier threshold, squared
                float _inlierThresholdSquared;
                
                /// Occlusion removal flag, only used if the input point type contains normal information
                bool _occlusionReasoning;
                
                /// View axis: 0, 1 or 2 meaning x, y or z (defaults to z)
                int _viewAxis;
                
                /// Type of penalty to use for @ref penalty()
                PENALTY _penalty;
                
                struct {
                    size_t size; ///< Number of source points, necessary for computing relative inlier fraction
                    core::Correspondence::Vec inliers; ///< Inlier correspondences
                    double mse; ///< MSE for the inliers
                    std::vector<size_t> occluded; ///< Indices of occluded points, if occlusion removal is enabled
                    std::vector<size_t> outliers; ///< Indices of outlier points, if occlusion removal is enabled
                } _state;
        };
    }
}

#ifndef COVIS_PRECOMPILE
#include "fit_evaluation_impl.hpp"
#endif

#endif
