// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "search_base.h"

covis::core::Correspondence::VecPtr covis::detect::computeFusionMatches(
        const std::vector<core::Correspondence::VecPtr>& corr) {
    // Sanity checks
    COVIS_ASSERT(!corr.empty());
    const size_t size = corr[0]->size();
    for(size_t i = 1; i < size; ++i)
        COVIS_ASSERT(corr[0]->size() == size);

    // Fuse
    covis::core::Correspondence::VecPtr fused(new covis::core::Correspondence::Vec(size));
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for(size_t i = 0; i < size; ++i) { // Loop over number of observations
        for(size_t j = 0; j < corr.size(); ++j) { // Loop over feature types
            if((*corr[j])[i].empty())
                continue;

            if( (*fused)[i].empty() || (*corr[j])[i].distance[0] < (*fused)[i].distance[0] )
                (*fused)[i] = (*corr[j])[i];
        }
    }

    return fused;
}
