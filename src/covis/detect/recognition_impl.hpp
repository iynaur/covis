// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_DETECT_RECOGNITION_IMPL_HPP
#define COVIS_DETECT_RECOGNITION_IMPL_HPP

// Own
#include "point_search.h"
#include "../core/detection.h"
#include "../core/correspondence.h"
#include "../core/io.h"
#include "../core/macros.h"
#include "../core/progress_display.h"

// PCL
#include <pcl/recognition/impl/hv/hv_go.hpp>
#include <pcl/recognition/impl/hv/hv_papazov.hpp>
#include <pcl/registration/icp.h>

namespace covis {
    namespace detect {
        template<typename PointT>
        const core::Detection::Vec& Recognition<PointT>::recognize() {
            // Sanity checks
            COVIS_ASSERT(!_sources.empty() && _target);
            for(size_t i = 0; i < _sources.size(); ++i)
                COVIS_ASSERT(_sources[i]);
            if(_featureCorrespondences.empty()) {
                feature::MatrixT::Index cnt = 0;
                for(size_t i = 0; i < _sources.size(); ++i)
                    cnt += _sources[i]->size();
                COVIS_ASSERT(_sourceFeatures.cols() == cnt && _targetFeatures.size());
            }
            if(_sourceNames.empty()) {
                _sourceNames.resize(_sources.size());
                for(size_t i = 0; i < _sources.size(); ++i)
                    _sourceNames[i] = "Object with index " + core::stringify(i);
            }
            COVIS_ASSERT(_sourceNames.size() == _sources.size());
            COVIS_ASSERT(_correspondenceFraction > 0 && _correspondenceFraction <= 1);
            COVIS_ASSERT(_inlierThreshold > 0);
            COVIS_ASSERT(_inlierFraction >= 0 && _inlierFraction <= 1);

            // Create a map and a library for all object features
            if(_mapSource.empty()) {
                // Create a map from global library to corresponding <object, feature point> index
                size_t sizeAll = 0;
                for (size_t i = 0; i < _sources.size(); ++i)
                    sizeAll += _sources[i]->size();
                _mapSource.resize(sizeAll);
                size_t cnt = 0; // Global counter
                for (size_t i = 0; i < _sources.size(); ++i)
                    for (size_t j = 0; j < _sources[i]->size(); ++j)
                        _mapSource[cnt++] = std::make_pair(i, j);


                if(_featureCorrespondences.empty()) {
                    if (_verbose)
                        COVIS_MSG_INFO("First call to recognize(), initializing search index for object features...");

                    // Create a search index
                    _fsearch.setTrees(_searchTrees);
                    _fsearch.setChecks(_searchChecks);
                    _fsearch.setTarget(_sourceFeatures);
                }
            }

            // Perform feature matching, if necessary
            if(_featureCorrespondences.empty()) {
                // Auto magic for the k parameter for feature matching
                size_t knn = _knn;
                if(knn == 0) { // Automatic, now rely on the multi-instance parameter
                    if(_multiInstance == 0) { // Multi-instance disabled, use 2 for RANSAC and a high number for voting
                        if (_detector == RANSAC)
                            knn = 2;
                        else
                            knn = MIN(10, _sources.size());
                    } else { // Multi-instance enabled, use that number
                        knn = _multiInstance;
                    }
                }

                // Perform feature matching
                if(knn <= 2 && _correspondenceFraction < 1) { // If 1- or 2-NNs and ranking necessary, use ratio search
                    if (_verbose)
                        COVIS_MSG_INFO("Performing ratio matching...");
                    _featureCorrespondences = *_fsearch.ratio(_targetFeatures);
                } else { // Otherwise find all
                    if (_verbose)
                        COVIS_MSG_INFO("Performing " << knn << "-NN matching...");
                    _featureCorrespondences = *_fsearch.knn(_targetFeatures, knn);
                    _featureCorrespondences = *core::flatten(_featureCorrespondences);
                }
            } else {
                if(_verbose)
                    COVIS_MSG_INFO("Feature correspondences already provided, skipping feature matching!");
            }

            // Reverse all correspondences (object --> scene), and spread them out on all object models
            _featureCorrespondences = *core::flatten(_featureCorrespondences);
            std::vector<core::Correspondence::VecPtr> corrQuerytarget(_sources.size());
            for(size_t i = 0; i < _sources.size(); ++i)
                corrQuerytarget[i].reset(new core::Correspondence::Vec);

            for (size_t i = 0; i < _featureCorrespondences.size(); ++i) {
                if(_featureCorrespondences[i].empty())
                    continue;
                const size_t idxMatch = _featureCorrespondences[i].match[0]; // Index into object library
                const std::pair<size_t, size_t>& idx = _mapSource[idxMatch]; // Index for <object, feature>
                corrQuerytarget[idx.first]->push_back(
                        core::Correspondence(idx.second, _featureCorrespondences[i].query, _featureCorrespondences[i].distance[0])
                );
            }

            // Take a subset of best correspondences for each object
            if(_correspondenceFraction < 1) {
                for (size_t i = 0; i < _sources.size(); ++i) {
                    core::sort(*corrQuerytarget[i]);
                    corrQuerytarget[i]->resize(_correspondenceFraction * corrQuerytarget[i]->size());
                }
            }

            // Get the number of correspondences for each object
            std::vector<size_t> sizes(_sources.size());
            for (size_t i = 0; i < _sources.size(); ++i)
                sizes[i] = corrQuerytarget[i]->size();

            // Sort the object list by descending number of correspondences
            const std::vector<size_t> order = core::sort<size_t>(sizes, false);

            // For evaluating poses
            typename detect::FitEvaluation<PointT>::Ptr eval(new detect::FitEvaluation<PointT>(_target));
            eval->setInlierThreshold(_inlierThreshold);
            if (_fullEvaluation) {
                eval->setOcclusionReasoning(true);
                eval->setPenaltyType(detect::FitEvaluation<PointT>::INLIERS_OUTLIERS_RMSE);
            } else {
                eval->setOcclusionReasoning(false);
                eval->setPenaltyType(detect::FitEvaluation<PointT>::INLIERS);
            }

            // For doing segmentation
            detect::PointSearch<PointT> targetSearch;
            targetSearch.setTarget(_target);

            // Scene detections (all and accepted by inlier/voting thresholds)
            _detections.clear();
            std::vector<bool> targetMask(_target->size(), true); // Will be filled with false for each segmented point

            // Loop over all objects and run estimation in the above order
            for (size_t idx = 0; idx < _sources.size(); ++idx) {
                // Current object index
                const size_t kidx = order[idx];

                // For refinement
                typename CloudT::ConstPtr target2 = filter::downsample<PointT>(_target, 2 * _sourceResolutions[kidx]);
                typename CloudT::ConstPtr target4 = filter::downsample<PointT>(_target, 4 * _sourceResolutions[kidx]);

                // Take correspondences only for remaining target feature points
                core::Correspondence::VecPtr corrRemain(new core::Correspondence::Vec);
                for (size_t j = 0; j < corrQuerytarget[kidx]->size(); ++j)
                    if (targetMask[ (*corrQuerytarget[kidx])[j].match[0] ])
                        corrRemain->push_back( (*corrQuerytarget[kidx])[j] );

                // If this object has no correspondences in remaining scene data, continue to next
                if (corrRemain->size() < 3) {
                    if (_verbose)
                        COVIS_MSG_INFO("No correspondences for " << _sourceNames[kidx] << " - not found!");
                    continue;
                }

                // Run detector to get a pose
                core::Detection::Vec d;
                if(_detector == RANSAC) {
                    if (_verbose)
                        COVIS_MSG_INFO("Running RANSAC detector with " << corrRemain->size() <<
                                       " correspondences for " << _sourceNames[kidx] << "...");
                    // Initialize RANSAC
                    detect::Ransac<PointT> ransac;
                    ransac.setSource(_sources[kidx]);
                    ransac.setTarget(_target);
                    ransac.setCorrespondences(corrRemain);
                    ransac.setFitEvaluation(eval);

                    ransac.setIterations(_ransacIterations);
                    ransac.setWeightedSampling(false); // Sample correspondences with a probability proportional to their quality
                    ransac.setInlierThreshold(_inlierThreshold); // Inlier threshold
                    ransac.setInlierFraction(_inlierFraction); // Inlier fraction
                    ransac.setFullEvaluation(_fullEvaluation); // Evaluation using only feature point matches if false
                    ransac.setPrerejection(_prereject);
                    ransac.setPrerejectionSimilarity(_prerejectionSimilarity);

                    ransac.setVerbose(_verbose);

                    // Perform pose estimation
                    if(_multiInstance == 1) {
                        d.push_back(ransac.estimate());
                    } else {
                        ransac.estimate();
                        d = ransac.getAllDetections();
                        core::sort(d);
                        if(d.size() > _multiInstance)
                            d.resize(_multiInstance);
                    }
                } else if(_detector == VOTING) {
                    if (_verbose)
                        COVIS_MSG_INFO("Running pose voting detector with " << corrRemain->size() <<
                                       " correspondences for " << _sourceNames[kidx] << "...");
                    detect::PoseVoting<PointT> nv;
                    nv.setSource(_sources[kidx]);
                    nv.setTarget(_target);
                    nv.setCorrespondences(corrRemain);
                    nv.setTranslationBandwidth(_bwt);
                    nv.setRotationBandwidth(_bwrot);
                    nv.setTessellation(_tessellation);
                    nv.setVerbose(_verbose);

                    if(_multiInstance == 1) {
                        d.push_back(nv.estimate());
                    } else {
                        nv.estimate();
                        d = nv.getAllDetections();
                        core::sort(d, "kde", true);
                        if(d.size() > _multiInstance)
                            d.resize(_multiInstance);
                    }
                    
                    for(size_t i = 0; i < d.size(); ++i) {
                        if (_fullEvaluation)
                            eval->update(_sources[kidx], d[i].pose);
                        else
                            eval->update(_sources[kidx], d[i].pose, corrRemain);
                        d[i].rmse = eval->rmse();
                        d[i].inlierfrac = eval->inlierFraction();
                        d[i].outlierfrac = eval->outlierFraction();
                        d[i].penalty = eval->penalty();
                    }
                } else {
                    COVIS_THROW("Unknown detector: " << int(_detector));
                }

#ifdef _OPENMP
#pragma omp parallel for
#endif
                for(size_t i = 0; i < d.size(); ++i) {
                    if (d[i]) {
                        // Refine
                        if(_refinementIterations > 0) {

                            pcl::IterativeClosestPoint<PointT, PointT> icp;
                            icp.setMaximumIterations(_refinementIterations);
                            CloudT tmp;

                            if(!_multiscaleRefinement) {
                                icp.setInputSource(_sources[kidx]);
                                icp.setInputTarget(_target);
                                icp.setMaxCorrespondenceDistance(_inlierThreshold); // Set inlier threshold to lowest res
                                icp.align(tmp, d[i].pose);
                            } else {
                                const float inlierThresIcp = 2 * _sourceResolutions[kidx];
                                icp.setInputSource(_sources4[kidx]);
                                icp.setInputTarget(target4);
                                icp.setMaxCorrespondenceDistance(4 * inlierThresIcp);
                                icp.align(tmp, d[i].pose);
                                if(icp.hasConverged()) {
                                    icp.setInputSource(_sources2[kidx]);
                                    icp.setInputTarget(target2);
                                    icp.setMaxCorrespondenceDistance(2 * inlierThresIcp);
                                    icp.align(tmp, icp.getFinalTransformation());
                                    if(icp.hasConverged()) {
                                        icp.setInputSource(_sources1[kidx]);
                                        icp.setInputTarget(_target);
                                        icp.setMaxCorrespondenceDistance(inlierThresIcp);
                                        icp.align(tmp, icp.getFinalTransformation());
                                    }
                                }
                            }
                            
                            if(icp.hasConverged())
                                d[i].pose = icp.getFinalTransformation();
                            else
                                d[i].clear();
                        }
                    }
                }

                // For all detections (maybe refined), do postprocessing
                for(size_t i = 0; i < d.size(); ++i) {
                    // Postprocessing
                    if (d[i]) {
                        if (_fullEvaluation)
                            eval->update(_sources[kidx], d[i].pose);
                        else
                            eval->update(_sources[kidx], d[i].pose, corrRemain);
                        d[i].rmse = eval->rmse();
                        d[i].inlierfrac = eval->inlierFraction();
                        d[i].outlierfrac = eval->outlierFraction();
                        d[i].penalty = eval->penalty();

                        // Accept or reject final detection
                        bool accepted = true;
                        if (_detector == VOTING && _kdeThreshold > 0)
                            accepted &= (d[i].params["kde"] >= _kdeThreshold);
                        else if (_detector == RANSAC)
                            accepted &= (d[i].inlierfrac >= _inlierFraction);

                        // Correct the object index and set label before storing
                        if (accepted) {
                            d[i].idx = kidx;
                            d[i].label = _sourceNames[kidx];
                            _detections.push_back(d[i]);

                            if (_verifier == GREEDY) {
                                // Mask out the segment in the scene
                                if (_verbose)
                                    COVIS_MSG("Segmenting " << _sourceNames[kidx] << " from scene...");
                                CloudT queryT;
                                pcl::transformPointCloud<PointT>(*_sources[kidx], queryT, d[i].pose);
                                core::Correspondence::VecPtr corrQueryTTarget =
                                        targetSearch.radius(queryT, 0.5 * _inlierThreshold); // TODO: Radius
                                for (size_t j = 0; j < corrQueryTTarget->size(); ++j)
                                    for (size_t k = 0; k < (*corrQueryTTarget)[j].size(); ++k)
                                        if (targetMask[(*corrQueryTTarget)[j].match[k]])
                                            targetMask[(*corrQueryTTarget)[j].match[k]] = false;
                            }
                        }
                    }
                }

                // If detector produced result(s), print status
                if(_verbose) {
                    const size_t dsize = _detections.size();
                    if(dsize == 0)
                        COVIS_MSG_WARN(_sourceNames[kidx] << " not found!");
                    else
                        COVIS_MSG_INFO(_sourceNames[kidx] << " detected" <<
                                       (dsize == 1 ? "!" : (" with " + core::stringify(dsize) + " instances!")));
                }
            }

            // Global verification
            if(!_detections.empty() && (_verifier == GLOBAL || _verifier == CONFLICT)) {
                // Take a copy of the scene
                typename CloudT::Ptr tcopy;
                if(_targetVerification)
                    tcopy.reset(new CloudT(*_targetVerification));
                else
                    tcopy.reset(new CloudT(*_target));

                // Place the models objects in the scene
                std::vector<typename CloudT::ConstPtr> alignedModels;
                for (size_t i = 0; i < _detections.size(); ++i) {
                    typename CloudT::Ptr aligned(new CloudT);
                    if(_sourcesVerification.size() == _sources.size())
                        core::transform(*_sourcesVerification[_detections[i].idx], *aligned, _detections[i].pose);
                    else
                        core::transform(*_sources[_detections[i].idx], *aligned, _detections[i].pose);
                    alignedModels.push_back(aligned);
                }

                // Run verification
                std::vector<bool> mask;
                if(_verifier == CONFLICT) {
                    pcl::PapazovHV<PointT,PointT> phv;
                    phv.setSceneCloud(tcopy);
                    phv.addModels(alignedModels, true);
                    phv.setInlierThreshold(0.01);
                    phv.verify();
                    phv.getMask(mask);
                } else if(_verifier == GLOBAL) {
                    // http://pointclouds.org/documentation/tutorials/global_hypothesis_verification.php
                    pcl::GlobalHypothesesVerification<PointT, PointT> ghv;
                    ghv.setSceneCloud(tcopy);
                    ghv.addModels(alignedModels, true);
                    ghv.setInlierThreshold(_inlierThreshold);
                    ghv.setOcclusionThreshold(0.01);
                    ghv.setRegularizer(3);
                    ghv.setRadiusClutter(0.03);
                    ghv.setClutterRegularizer(5);
                    ghv.setDetectClutter(true);
                    ghv.setRadiusNormals(0.05);
                    ghv.verify();
                    ghv.getMask(mask);
                }

                // Remove rejected hypotheses
                _detections = core::mask(_detections, mask);
            }

            return _detections;
        }
    }
}
#endif
