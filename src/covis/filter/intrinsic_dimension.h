// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FILTER_INTRINSIC_DIMENSION_H
#define COVIS_FILTER_INTRINSIC_DIMENSION_H

// Own
#include "image_filter_base.h"

namespace covis {
    namespace filter {
        /**
         * @class IntrinsicDimension
         * @ingroup filter
         * @brief Intrinsic dimensionality computation
         *
         * The intrinsic dimension (iD) is a low-level image representation, which builds on top of local structural
         * image representations consisting of magnitude and orientation (see e.g. @ref MonogenicSignal).
         * The iD representation gives three values per pixel (i0D, i1D and i2D), which characterizes the pixel
         * in terms of homogeneous (0-dimensional), line (1-dimensional) and junction/texture (>= 2-dimensional)
         * responses. The iD signal can thus be used for determining whether local image regions contain either of these
         * structures.
         *
         * For more information, see @cite felsberg2009continuous.
         *
         * @sa @ref MonogenicSignal
         *
         * @author Anders Glent Buch
         * @author Sinan Kalkan
         *
         * @example example/intrinsic_dimension/intrinsic_dimension.cpp
         */
        class IntrinsicDimension : public ImageFilterBase {
            public:
                /// Initialize the ID computation with default parameters
                IntrinsicDimension() : _sigma(3), _normalizeLinear(false) {}

                /// Destructor
                virtual ~IntrinsicDimension() {}

                /**
                 * Calculate the intrinsic dimension using a BGR or grayscale image
                 *
                 * @note This function automatically calculates the local magnitude and orientation using
                 * @ref IntensityGradient, and then
                 * @ref filter(const cv::Mat_<double>&,const cv::Mat_<double>&,double,bool) "filter()" below is called.
                 * If you want control over the granularity of the output, use e.g. @ref IntensityGradient or
                 * @ref MonogenicSignal to compute the local magnitude and orientation. The output can then be passed to
                 * @ref filter(const cv::Mat_<double>&,const cv::Mat_<double>&,double,bool) "filter()" below.
                 *
                 * @param image input BGR or grayscale image, can have either integral or floating point elements
                 * @return a 3-channel double precision image representing intrinsic dimensions (i0D, i1D and i2D),
                 * all in range @f$[0,1]@f$
                 */
                cv::Mat filter(const cv::Mat& image);

                /**
                 * Calculate the intrinsic dimension from a magnitude and orientation image pair
                 *
                 * @param m magnitude image
                 * @param o orientation image
                 * @return a 3-channel double precision image representing intrinsic dimensions (i0D, i1D and i2D),
                 * all in range @f$[0,1]@f$
                 */
                cv::Mat filter(const cv::Mat_<double>& m, const cv::Mat_<double>& o);

                /**
                 * Set standard deviation of smoothing filter for complex images
                 * @param sigma standard deviation of smoothing filter
                 */
                inline void setSigma(double sigma) {
                    _sigma = sigma;
                }

                /**
                 * Get standard deviation of smoothing filter
                 * @return standard deviation of smoothing filter
                 */
                inline double getSigma() const {
                    return _sigma;
                }

                /**
                 * Set linear normalization flag
                 * @param normalizeLinear linear normalization flag
                 */
                inline void setNormalizeLinear(bool normalizeLinear) {
                    _normalizeLinear = normalizeLinear;
                }

                /**
                 * Get linear normalization flag
                 * @return linear normalization flag
                 */
                inline bool getNormalizeLinear() const {
                    return _normalizeLinear;
                }

            private:
                /// Standard deviation of smoothing filter for complex images
                double _sigma;

                /// Linear normalization flag for magnitude image
                bool _normalizeLinear;
        };
        
        /**
         * @ingroup filter
         * 
         * Compute the intrinsic dimension of an image using @ref IntrinsicDimension
         * 
         * @param image input BGR or grayscale image, can have either integral or floating point elements
         * @param sigma standard deviation of smoothing filter
         * @param normalizeLinear linear normalization flag for magnitude image
         * @return a 3-channel double precision image representing intrinsic dimensions (i0D, i1D and i2D),
         * all in range @f$[0,1]@f$
         */
        inline cv::Mat computeIntrinsicDimension(const cv::Mat& image, double sigma = 3, bool normalizeLinear = false) {
            IntrinsicDimension id;
            id.setSigma(sigma);
            id.setNormalizeLinear(normalizeLinear);
            return id.filter(image);
        }
    }
}

#endif
