// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FILTER_POINT_CLOUD_PREPROCESSING_HPP
#define COVIS_FILTER_POINT_CLOUD_PREPROCESSING_HPP

// Own
#include "point_cloud_preprocessing.h"
#include "../feature/normal_correction_manifold.h"

// PCL
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/vtk_lib_io.h>

// VTK
#include <vtkLinearSubdivisionFilter.h>
#include <vtkPolyData.h>
#include <vtkPolyDataPointSampler.h>
#include <vtkQuadricDecimation.h>
#include <vtkSmartPointer.h>

namespace {
template<typename PointT>
struct Splitter {
    /**
     * Subdivide a triangle into four new triangles
     * @param tri triangle point indices
     * @param cloud points indexed by tri
     * @param tol lower tolerance on edge lengths for subdividing
     * @param offset offset for indices of new vertices
     * @param trinew new triangle indices
     * @param pnew new vertices
     * @return
     */
    bool operator()(const std::vector<uint32_t>& tri,
                    pcl::PointCloud<PointT>& cloud,
                    float tol,
                    size_t offset,
                    std::vector<pcl::Vertices>& trinew,
                    pcl::PointCloud<PointT>& pnew) {

        if(tri.size() == 3) {
            // Points on original triangle
            const PointT p0 = cloud[tri[0]];
            const PointT p1 = cloud[tri[1]];
            const PointT p2 = cloud[tri[2]];
            const Eigen::Vector3f p0v = p0.getVector3fMap();
            const Eigen::Vector3f p1v = p1.getVector3fMap();
            const Eigen::Vector3f p2v = p2.getVector3fMap();

            // If any edge is larger than tolerance, subdivide
            if((p1v - p0v).norm() > tol || (p2v - p0v).norm() > tol || (p2v - p1v).norm() > tol) {
                pnew.clear();
                trinew.clear();
                trinew.resize(4);

                // Midpoints
                PointT p01 = p0;
                PointT p02 = p2;
                PointT p12 = p1;
                // TODO: Interpolate all fields here
                p01.getVector3fMap() = (p0v + p1v) / 2.0;
                p02.getVector3fMap() = (p0v + p2v) / 2.0;
                p12.getVector3fMap() = (p1v + p2v) / 2.0;
                pnew.push_back(p01);
                pnew.push_back(p02);
                pnew.push_back(p12);

                // New middle point indices
                const size_t idx01 = offset;
                const size_t idx02 = offset + 1;
                const size_t idx12 = offset + 2;

                // Four new triangles

                // Triangle p0 p01 p02
                trinew[0].vertices.push_back(tri[0]);
                trinew[0].vertices.push_back(idx01);
                trinew[0].vertices.push_back(idx02);

                // Triangle p01 p12 p02
                trinew[1].vertices.push_back(idx01);
                trinew[1].vertices.push_back(idx12);
                trinew[1].vertices.push_back(idx02);

                // Triangle p01 p1 p12
                trinew[2].vertices.push_back(idx01);
                trinew[2].vertices.push_back(tri[1]);
                trinew[2].vertices.push_back(idx12);

                // Triangle p02 p12 p2
                trinew[3].vertices.push_back(idx02);
                trinew[3].vertices.push_back(idx12);
                trinew[3].vertices.push_back(tri[2]);

                return true;
            }
        }

        return false;
    }
};
}

namespace covis {
    namespace filter {
        template<typename PointT>
        template<typename PointT2>
        struct PointCloudPreprocessing<PointT>::Downsampler<PointT2, false> {
            static typename pcl::PointCloud<PointT2>::Ptr run(typename pcl::PointCloud<PointT2>::ConstPtr cloud,
                                                              float resolution) {
                typename pcl::PointCloud<PointT2>::Ptr result(new pcl::PointCloud<PointT2>);
                pcl::VoxelGrid<PointT2> vg;
                vg.setDownsampleAllData(true);
                vg.setLeafSize(resolution, resolution, resolution);
                vg.setInputCloud(cloud);
                vg.filter(*result);

                return result;
            }
        };

        template<typename PointT>
        template<typename PointT2>
        struct PointCloudPreprocessing<PointT>::Downsampler<PointT2, true> {
            static typename pcl::PointCloud<PointT2>::Ptr run(typename pcl::PointCloud<PointT2>::ConstPtr cloud,
                                                              float resolution) {
                typename pcl::PointCloud<PointT2>::Ptr result = Downsampler<PointT2, false>::run(cloud, resolution);

                // Also copy the normals
                detect::PointSearch<PointT2> ps(cloud);
#ifdef _OPENMP
#pragma omp parallel for
#endif
                for(size_t i = 0; i < result->size(); ++i) {
                    const core::Correspondence corr = ps.knn(result->points[i], 1);
                    if(corr)
                        result->points[i].getNormalVector3fMap() = cloud->points[corr.match[0]].getNormalVector3fMap();
                }

                return result;
            }
        };

        template<typename PointT>
        typename pcl::PointCloud<PointT>::Ptr PointCloudPreprocessing<PointT>::filter(
                typename pcl::PointCloud<PointT>::ConstPtr cloud) {
            // Sanity checks
            COVIS_ASSERT(cloud);

            typename pcl::PointCloud<PointT>::Ptr result(new pcl::PointCloud<PointT>(*cloud));

            // Scale
            if(_scale != 1) {
                if(_verbose)
                    COVIS_MSG_INFO("Scaling coordinates by " << _scale << "...");
                result = scale<PointT>(result, _scale);
            }

            // Remove NaNs
            if(_removeNan) {
                const size_t sz = result->size();
                std::vector<int> dummy;
                pcl::removeNaNFromPointCloud<PointT>(*result, *result, dummy);

                if(_verbose && !dummy.empty())
                    COVIS_MSG_INFO("Removed " << sz - dummy.size() << " points with NaN coordinates!");
            }

            // Remove pure zeros
            pcl::PointCloud<PointT> tmp;
            for(size_t i = 0; i < result->size(); ++i) {
                if(result->points[i].x == 0 && result->points[i].y == 0 && result->points[i].z == 0)
                    continue;
                tmp.push_back(result->points[i]);
            }

            if(_verbose)
                COVIS_MSG_INFO("Removed " << result->size() - tmp.size() << "/" << result->size() << " points with zero coordinates!");

            *result = tmp;

            if(_far > 0) {
                if(_verbose)
                    COVIS_MSG_INFO("Removing points with a depth larger than " << _far << "...");
                result = removeFarPoints<PointT>(result, _far);
            }

            if(_resolution > 0) {
                if(_verbose)
                    COVIS_MSG_INFO("Downsampling surface to a resolution of " << _resolution << "...");
                result = Downsampler<PointT>::run(result, _resolution);
            }

            if(_normalRadius > 0) {
                if(core::HasNormal<PointT>::value) {
                    computeNormals(result);

                    if(_orientNormals) {
                        if(_verbose)
                            COVIS_MSG_INFO("Applying consistent normal orientation algorithm...");
                        feature::computeCorrectedNormals(*result, !_concave);
                    }
                } else {
                    COVIS_MSG_WARN("Non-zero normal radius set, but cannot estimate normals for input point type!");
                }

                if(_removeNan) {
                    std::vector<int> index;
                    const size_t sz = result->size();
                    pcl::removeNaNNormalsFromPointCloud(*result, *result, index);
                    if(_verbose && !index.empty())
                        COVIS_MSG_INFO("Removed " << sz - index.size() << "/" << sz << " points with NaN normals!");
                }
            }

            if(_removeTable) {
                if(_verbose)
                    COVIS_MSG_INFO("Removing dominant plane...");
                result = removeDominantPlane<PointT>(result, _tableRemovalThreshold);
            }

            return result;
        }

        template<typename PointT>
        typename pcl::PointCloud<PointT>::Ptr PointCloudPreprocessing<PointT>::filter(
                pcl::PolygonMesh::ConstPtr meshin) {
            // Sanity checks
            COVIS_ASSERT(meshin);

            pcl::PolygonMesh::Ptr mesh = boost::make_shared<pcl::PolygonMesh>(*meshin);

            const bool haspoly = (mesh->polygons.size() > 0);

            // Scale
            if(_scale != 1) {
                if(_verbose)
                    COVIS_MSG_INFO("Scaling coordinates by " << _scale << "...");
                mesh = scale(mesh, _scale);
            }

            // Compute mesh normals in any case if we have faces
            if(haspoly) {
                // Convert from PCL mesh representation to the VTK representation
                vtkSmartPointer<vtkPolyData> poly;
                pcl::io::mesh2vtk(*mesh, poly);

                vtkSmartPointer<vtkPolyDataNormals> pdn = vtkPolyDataNormals::New();
                pdn->ComputePointNormalsOn();
                pdn->SplittingOff();
#if VTK_MAJOR_VERSION <= 5
                pdn->SetInput(poly);
#else
                pdn->SetInputData(poly);
#endif
                pdn->Update();
                poly = pdn->GetOutput();

                pcl::io::vtk2mesh(poly, *mesh);
                pdn->Delete();
            }

            // Below we do downsampling - but this is problematic for already low-res models, so here we can upsample
            if(_resolution > 0 && haspoly) {
                float res = detect::computeResolution(mesh, false, 5);
                if(res > _resolution / 2.0) { // Handle varying mesh resolutions by using a factor here
                    if(_verbose)
                        COVIS_MSG_INFO("Mesh has a low resolution of " << res << " - upsampling before processing...");

                    Splitter<PointT> split;

                    pcl::PointCloud<PointT> cloud;
                    pcl::fromPCLPointCloud2(mesh->cloud, cloud);
                    std::vector<pcl::Vertices> polygons = mesh->polygons;
                    std::vector<bool> visit(polygons.size(), true);

                    size_t splits;
                    do {
                        splits = 0;

                        std::vector<pcl::Vertices> polytmp;
                        pcl::PointCloud<PointT> cloudtmp = cloud;
                        std::vector<bool> visitTmp;
                        for(size_t i = 0; i < polygons.size(); ++i) {
                            pcl::PointCloud<PointT> pnew;
                            std::vector<pcl::Vertices> trinew;
                            if(visit[i] &&
                               split(polygons[i].vertices, cloud, _resolution, cloudtmp.size(), trinew, pnew)) {
                                ++splits;
                                cloudtmp += pnew;
                                polytmp.insert(polytmp.end(), trinew.begin(), trinew.end());
                                visitTmp.insert(visitTmp.end(), trinew.size(), true);
                            } else {
                                polytmp.push_back(polygons[i]);
                                visitTmp.push_back(false);
                            }
                        }

                        polygons = polytmp;
                        cloud = cloudtmp;
                        visit = visitTmp;
                    } while(splits > 0);

                    pcl::PolygonMesh::Ptr meshup(new pcl::PolygonMesh);
                    meshup->polygons = polygons;
                    pcl::toPCLPointCloud2(cloud, meshup->cloud);

                    vtkSmartPointer<vtkPolyData> poly;
                    pcl::io::mesh2vtk(*meshup, poly);

                    if(_verbose)
                        COVIS_MSG("\tRecomputing orientation for upsampled mesh...");

                    // Recompute normals for upsampled mesh
                    vtkSmartPointer<vtkPolyDataNormals> pdn = vtkPolyDataNormals::New();
                    pdn->ComputePointNormalsOn();
                    pdn->SplittingOff();
#if VTK_MAJOR_VERSION <= 5
                    pdn->SetInput(poly);
#else
                    pdn->SetInputData(poly);
#endif
                    pdn->Update();

                    poly = pdn->GetOutput();
                    pcl::io::vtk2mesh(poly, *meshup);
                    pdn->Delete();

                    // Finally write to mesh
                    *mesh = *meshup;
                }
            }

            // Now go the PCL representation
            typename pcl::PointCloud<PointT>::Ptr result(new pcl::PointCloud<PointT>);
            pcl::fromPCLPointCloud2<PointT>(mesh->cloud, *result);

            // Remove NaNs
            if(_removeNan) {
                pcl::IndicesPtr index(new std::vector<int>);

                size_t sz = result->size();
                pcl::removeNaNFromPointCloud<PointT>(*result, *result, *index);
                if(_verbose && !index->empty())
                    COVIS_MSG_INFO("Removed " << sz - index->size() << "/" << sz << " points with NaN coordinates!");
            }

            // Remove pure zeros
            pcl::PointCloud<PointT> tmp;
            for(size_t i = 0; i < result->size(); ++i) {
                if(result->points[i].x == 0 && result->points[i].y == 0 && result->points[i].z == 0)
                    continue;
                tmp.push_back(result->points[i]);
            }

            if(_verbose)
                COVIS_MSG_INFO("Removed " << result->size() - tmp.size() << "/" << result->size() << " points with zero coordinates!");

            *result = tmp;

            if(_far > 0) {
                if(_verbose)
                    COVIS_MSG_INFO("Removing points with a depth larger than " << _far << "...");
                result = removeFarPoints<PointT>(result, _far);
            }

            if(_resolution > 0) {
                if(_verbose)
                    COVIS_MSG_INFO("Downsampling surface to a resolution of " << _resolution << "...");

                result = Downsampler<PointT>::run(result, _resolution);
            }

            if(_normalRadius > 0) {
                computeNormals(result);
                if(haspoly) {
                    if(_verbose)
                        COVIS_MSG_INFO("Using the input mesh for consistent normal orientation...");

                    typename pcl::PointCloud<PointT>::Ptr tmp(new pcl::PointCloud<PointT>);
                    pcl::fromPCLPointCloud2<PointT>(mesh->cloud, *tmp);
                    detect::PointSearch<PointT> search(tmp);
#ifdef _OPENMP
#pragma omp parallel for
#endif
                    for(size_t i = 0; i < result->size(); ++i) {
                        const core::Correspondence c = search.knn(result->points[i], 1);
                        const Eigen::Vector3f n1 = result->points[i].getNormalVector3fMap();
                        const Eigen::Vector3f n2 = tmp->points[c.match[0]].getNormalVector3fMap();
                        if(n1.dot(n2) < 0) {
                            result->points[i].normal_x = -result->points[i].normal_x;
                            result->points[i].normal_y = -result->points[i].normal_y;
                            result->points[i].normal_z = -result->points[i].normal_z;
                        }
                    }
                } else {
                    if(_orientNormals) {
                        if(_verbose)
                            COVIS_MSG_INFO("Applying consistent normal orientation algorithm...");
                        feature::computeCorrectedNormals(*result, !_concave);
                    }
                }

                if(_removeNan) {
                    std::vector<int> index;
                    const size_t sz = result->size();
                    pcl::removeNaNNormalsFromPointCloud(*result, *result, index);
                    if(_verbose && !index.empty())
                        COVIS_MSG_INFO("Removed " << sz - index.size() << "/" << sz << " points with NaN normals!");
                }
            }

            if(_removeTable) {
                if(_verbose)
                    COVIS_MSG_INFO("Removing dominant plane...");
                result = removeDominantPlane<PointT>(result, _tableRemovalThreshold);
            }

            return result;
        }
    }
}

#endif
