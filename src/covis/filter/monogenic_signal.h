// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FILTER_MONOGENIC_SIGNAL_H
#define COVIS_FILTER_MONOGENIC_SIGNAL_H

// Own
#include "image_filter_base.h"

namespace covis {
    namespace filter {
        /**
         * @class MonogenicSignal
         * @ingroup filter
         * @brief Monogenic signal
         *
         * The monogenic signal is an image filter which produces a tuple of
         * local magnitude, orientation and phase for each pixel. The three
         * output images thus have the same size as the input, and provide local structural
         * information about the input.
         *
         * This implementation comes with three predefined filtering kernels,
         * giving outputs at different levels of granularity.
         *
         * For more information, see @cite felsberg2001monogenic.
         *
         * @author Anders Glent Buch
         * @author Dirk Kraft
         *
         * @example example/monogenic_signal/monogenic_signal.cpp
         */
        class MonogenicSignal : public ImageFilterBase {
            public:
                /// Monogenic filter type
                enum FILTER_TYPE {
                    MONOGENIC_FREQ0110, /**< Highest frequency */
                    MONOGENIC_FREQ0055, /**< Middle frequency */
                    MONOGENIC_FREQ0028 /**< Lowest frequency */
                };

                /**
                 * Initialize monogenic filters to a given frequency
                 * @param ft filter type
                 * @param useComplex use complex filter
                 */
                MonogenicSignal(FILTER_TYPE ft = MONOGENIC_FREQ0110, bool useComplex = true);

                /// Destructor
                virtual ~MonogenicSignal();

                /**
                 * Apply the monogenic filter to BGR or grayscale image
                 *
                 * @note if @b image has 3 channels, it is automatically converted to grayscale
                 *
                 * @param image input RGB or grayscale image, can have either integral or floating point elements
                 * @return a 3-channel double precision image representing magnitude, orientation and phase:
                 *  - Magnitude range: @f$[0,\infty]@f$
                 *  - Orientation range: @f$[0,\pi]@f$
                 *  - Phase range: @f$[-\pi,\pi]@f$
                 */
                cv::Mat filter(const cv::Mat& image);

                /**
                 * Set filter type
                 * @param ft filter type
                 */
                inline void setFilterType(FILTER_TYPE ft) {
                    setupFilters(ft);
                }

                /**
                 * Set complex filter flag
                 * @param useComplex complex filter flag
                 */
                inline void setUseComplex(bool useComplex) {
                    _useComplex = useComplex;
                }

            private:
                /// Filter kernel data, highest frequency
                static const double Kernel0110[3][11][11];

                /// Filter kernel data, middle frequency
                static const double Kernel0055[3][23][23];

                /// Filter kernel data, lowest frequency
                static const double Kernel0028[3][33][33];

                /// Filter kernel, first component
                cv::Mat_<double> _firstFilter;

                /// Filter kernel, second component
                cv::Mat_<double> _secondFilter;

                /// Filter kernel, third component
                cv::Mat_<double> _thirdFilter;

                /// Set to true to use complex filters
                bool _useComplex;

                /**
                 * Load \ref _firstFilter, \ref _secondFilter and \ref _thirdFilter
                 * based on the filter type in the input
                 * @param ft filter type
                 */
                void setupFilters(FILTER_TYPE ft);

                /**
                 * Compute fast Fourier transform
                 * @param dftRows image rows
                 * @param dftCols image columns
                 * @param dftA first image
                 * @param dftB
                 * @param dftT
                 * @param filter1
                 * @param filter2
                 * @param incHeight
                 * @param grayImg
                 * @param imageRes
                 * @param imageRes2
                 */
                void fft(int dftRows,
                         int dftCols,
                         cv::Mat& dftA,
                         cv::Mat& dftB,
                         cv::Mat& dftT,
                         const cv::Mat_<double>& filter1,
                         const cv::Mat_<double>& filter2,
                         int incHeight,
                         const cv::Mat_<double>& grayImg,
                         cv::Mat_<double> &imageRes,
                         cv::Mat_<double> &imageRes2);

                /**
                 * Compute magnitude
                 * @param p real component
                 * @param ip first complex component
                 * @param jp second complex component
                 * @return magnitude image
                 */
                cv::Mat_<double> magnitude(const cv::Mat_<double>& p,
                                           const cv::Mat_<double>& ip,
                                           const cv::Mat_<double>& jp);

                /**
                 * Compute orientation
                 * @param m magnitude
                 * @param ip first complex component
                 * @param jp second complex component
                 * @return orientation image
                 */
                cv::Mat_<double> orientation(const cv::Mat_<double>& m,
                                             const cv::Mat_<double>& ip,
                                             const cv::Mat_<double>& jp);

                /**
                 * Compute phase
                 * @param o orientation
                 * @param p real component
                 * @param ip first complex component
                 * @param jp second complex component
                 * @return phase image
                 */
                cv::Mat_<double> phase(const cv::Mat_<double>& o,
                                       const cv::Mat_<double>& p,
                                       const cv::Mat_<double>& ip,
                                       const cv::Mat_<double>& jp);
        };
        
        /**
         * @ingroup filter
         * 
         * Compute the monogenic signal of an image using @ref MonogenicSignal
         * 
         * @param image input BGR or grayscale image, can have either integral or floating point elements
         * @param ft filter type
         * @param useComplex use complex filter flag
         * @return a 3-channel double precision image representing magnitude, orientation and phase:
         *  - Magnitude range: @f$[0,\infty]@f$
         *  - Orientation range: @f$[0,\pi]@f$
         *  - Phase range: @f$[-\pi,\pi]@f$
         */
        inline cv::Mat computeMonogenicSignal(const cv::Mat& image,
                MonogenicSignal::FILTER_TYPE ft = MonogenicSignal::MONOGENIC_FREQ0110,
                bool useComplex = true) {
            MonogenicSignal ms(ft, useComplex);
            return ms.filter(image);
        }
    }
}

#endif