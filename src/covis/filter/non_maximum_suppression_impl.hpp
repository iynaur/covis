// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FILTER_NON_MAXIMUM_SUPPRESSION_IMPL_HPP
#define COVIS_FILTER_NON_MAXIMUM_SUPPRESSION_IMPL_HPP

// Own
#include "non_maximum_suppression.h"
#include "../core/range.h"
#include "../detect/point_search.h"

namespace covis {
    namespace filter {
        template<typename PointT>
        typename pcl::PointCloud<PointT>::Ptr NonMaximumSuppression<PointT>::filter(
                typename pcl::PointCloud<PointT>::ConstPtr cloud) {
            // Sanity checks
            COVIS_ASSERT(cloud);
            COVIS_ASSERT_MSG(cloud->size() == _scores.size(), "Empty or incompatible scores for NMS!");
            COVIS_ASSERT_MSG(_suppressionRadius > 0, "No suppression radius set for NMS!");
            COVIS_ASSERT_MSG(_suppressionFactor >= 0, "Negative suppression factor set for NMS!");

            // Setup search
            detect::PointSearch<PointT> ps(false);
            ps.setTarget(cloud);

            // Perform NMS
            const float maxScore = *std::max_element(_scores.begin(), _scores.end());
            const float minScore = _suppressionFactor * maxScore;
            _mask.assign(cloud->size(), true);
            for(size_t i = 0; i < cloud->size(); ++i) {
                if(_mask[i]) {
                    if(_scores[i] >= minScore) { // Score high enough
                        // Loop over all neighbor particles
                        const core::Correspondence cloudRad = ps.radius(cloud->points[i], _suppressionRadius);
                        for(size_t j = 0; j < cloudRad.size(); ++j) {
                            // Neighbor index
                            const size_t ni = cloudRad.match[j];
                            // Avoid self
                            if(ni == i)
                                continue;
                            // Suppress either neighbor or self
                            if(_scores[i] >= _scores[ni])
                                _mask[ni] = false;
                            else
                                _mask[i] = false;
                        }
                    } else { // Score too low, suppress
                        _mask[i] = false;
                    }
                }
            }

            typename pcl::PointCloud<PointT>::Ptr result(new pcl::PointCloud<PointT>(core::mask(*cloud, _mask)));

            return result;
        }
    }
}

#endif