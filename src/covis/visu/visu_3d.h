// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_VISU_VISU_3D_H
#define COVIS_VISU_VISU_3D_H

// Own
#include "visu_base.h"
#include "../core/io.h"
#include "../core/macros.h"
#include "../core/traits.h"
#include "../detect/point_search.h"

// PCL
#include <pcl/point_cloud.h>
#include <pcl/visualization/pcl_visualizer.h>

// VTK
#include <vtkOpenGLRenderWindow.h>
#include <vtkRenderWindow.h>

namespace covis {
    namespace visu {
        /**
         * @class Visu3D
         * @ingroup visu
         * @brief Visualization class for point clouds
         *
         * Main class for all types of point cloud visualization classes. This class implements the interface function
         * @ref VisuBase::show() for starting the visualizer, but also a limited number of point cloud-related
         * functions, e.g. @ref addPointCloud(). So when subclassing, just overload @ref show()
         * again, add data to the internal visualizer using @ref addPointCloud() and end by calling
         * @ref VisuBase::show().
         * 
         * @todo Implement default color handlers for normal point clouds
         *
         * @author Anders Glent Buch
         */
        class Visu3D : public VisuBase {
            using VisuBase::_title;
            
            public:
                /// Visualizer type
                typedef pcl::visualization::PCLVisualizer VisuT;
                
                /// Internal visualizer
                VisuT visu;
                
                /// @copydoc VisuBase::VisuBase(const std::string&)
                Visu3D(const std::string& title = "Point cloud visualization") : VisuBase(title), visu("", false) {
                    _bg.r = 64;
                    _bg.g = 64;
                    _bg.b = 64;
                    _bgGradient = true;
                    _showOrigo = false;
                    _setup = false;
                    _textOffset = 0;
                }

                /// Destructor
                virtual ~Visu3D() {}
                
                /**
                 * Add a point cloud
                 * 
                 * This function automagically detects the point type of the input cloud, and makes sure that RGB
                 * information is shown on screen if it is available. For all types of point clouds, a depth (z)
                 * rendering and a random color handler are also added, allowing the user to choose between these
                 * with the keyboard numbers.
                 * 
                 * This function additionally handles the case where a point cloud with the given label already exists,
                 * in which case the point cloud is simply updated.
                 * 
                 * @param cloud point cloud
                 * @param label label
                 */
                template<typename PointT>
                void addPointCloud(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                                   const std::string& label = "cloud");

                /**
                 * @copydoc addPointCloud()
                 */
                void addPointCloud(pcl::PCLPointCloud2::ConstPtr cloud,
                                   const std::string& label = "cloud");
                
                /**
                 * Add a polygon mesh and set the interpolation method during rendering to Phong
                 * 
                 * @param mesh polygon mesh
                 * @param label label
                 */
                void addMesh(pcl::PolygonMesh::ConstPtr mesh, const std::string& label = "mesh");
                
                /**
                 * Get a smart pointer to the LOD actor used for rendering a mesh - returns a NULL pointer if label is not found
                 * This function is useful for modifying the mesh actor, e.g. by setting various properties:
                 * @code
                 * getMeshActor()->GetProperty()->SetRepresentationToWireframe();
                 * @endcode
                 * @param label mesh label
                 * @return LOD actor
                 */
                inline vtkSmartPointer<vtkLODActor> getMeshActor(const std::string& label = "mesh") {
                    auto it = visu.getCloudActorMap()->find(label);
                    if(it == visu.getCloudActorMap()->end())
                        return nullptr;
                    else
                        return it->second.actor;
                }
                
                /**
                 * Add point cloud normals
                 * @param cloud point cloud
                 * @param level show every max(1,level)'th normal
                 * @param scale scale normals by this value
                 * @param label label
                 * @tparam PointNT point type, must contain XYZ and normal data
                 */
                template<typename PointNT>
                void addNormals(typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                        size_t level = 2,
                        float scale = 0.02f,
                        const std::string& label = "cloud");

                /**
                 * Add a point cloud showing the coordinates of the normal vectors on the unit sphere
                 * @param normals normal vectors
                 * @param color color to provide for the points in the Gauss map
                 * @param label label
                 * @param pointSize point size, values > 1 are recommended for sparse data
                 * @tparam PointNT point type, must contain normal data
                 */
                template<typename PointNT>
                void addGaussMap(typename pcl::PointCloud<PointNT>::ConstPtr normals,
                        const pcl::RGB& color,
                        size_t pointSize = 5,
                        const std::string& label = "gauss_map");

                /**
                 * Add a point cloud showing the coordinates of the normal vectors on the unit sphere
                 * @param normals normal vectors
                 * @param pointSize point size, values > 1 are recommended for sparse data
                 * @param label label
                 * @tparam PointNT point type, must contain normal data
                 */
                template<typename PointNT>
                void addGaussMap(typename pcl::PointCloud<PointNT>::ConstPtr normals,
                        size_t pointSize = 5,
                        const std::string& label = "gauss_map");
                
                /**
                 * Add a point cloud with a custom constant color
                 * 
                 * @param cloud point cloud
                 * @param r red component
                 * @param g green component
                 * @param b blue component
                 * @param label label
                 * @tparam PointT point type, must contain XYZ data
                 */
                template<typename PointT>
                void addColor(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                        uchar r, uchar g, uchar b,
                        const std::string& label = "cloud");
                
                /**
                 * Render a point cloud as a scalar field, scalars taken from another cloud of same size
                 * @param cloud point cloud
                 * @param field scalar values to render at each point
                 * @param fname field name
                 * @param label label
                 * @tparam PointXYZT point with XYZ coordinates
                 * @tparam PointFieldT point with scalar to render at each coordinate
                 * @exception an exception is thrown if size(cloud) != size(field)
                 */
                template<typename PointXYZT, typename PointFieldT>
                void addScalarField(typename pcl::PointCloud<PointXYZT>::ConstPtr cloud,
                        typename pcl::PointCloud<PointFieldT>::ConstPtr field,
                        const std::string& fname,
                        const std::string& label = "cloud");
                
                /**
                 * Render a point cloud as a scalar field
                 * @param cloud point cloud
                 * @param fname field name
                 * @param label label
                 * @tparam PointT point type, must contain a field with the given name
                 */
                template<typename PointT>
                void addScalarField(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                        const std::string& fname,
                        const std::string& label = "cloud");
                
                /**
                 * Render a point cloud as a scalar field
                 * @param cloud point cloud
                 * @param field scalar values to render at each point
                 * @param label label
                 * @tparam PointT point type, must contain XYZ data
                 * @tparam T scalar type
                 * @exception an exception is thrown if size(cloud) != size(field)
                 */
                template<typename PointT, typename T>
                void addScalarField(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                        const typename std::vector<T>& field,
                        const std::string& label = "cloud");
                
                /**
                 * Show a pose as a reference frame
                 * @param T transformation matrix
                 * @param length length of the axes
                 * @param label base name of the labels to use for the three lines
                 */
                void addPose(const Eigen::Matrix4f& T, float length = 0.025, const std::string& label = "axis");
                
                /**
                 * Show a pose as a reference frame
                 * @param R rotation matrix
                 * @param t translation vector
                 * @param length length of the axes
                 * @param label base name of the labels to use for the three lines
                 */
                inline void addPose(const Eigen::Matrix3f& R,
                        const Eigen::Vector3f& t,
                        float length = 0.025,
                        const std::string& label = "axis") {
                    Eigen::Matrix4f T;
                    T.block<3,3>(0,0) = R;
                    T.block<3,1>(0,3) = t;
                    T.row(3) << 0, 0, 0, 1;
                    addPose(T, length, label);
                }
                
                /**
                 * Add a vector of poses to a view
                 * @param T transformation matrices
                 * @param length length of the axes
                 */
                inline void addPoses(const std::vector<Eigen::Matrix4f>& T, float length = 0.025) {
                    for(size_t i = 0; i < T.size(); ++i)
                        addPose(T[i], length, "axis" + core::stringify<size_t>(i));
                }

                /**
                 * Add a text label to the bottom left corner of the screen
                 * @note When setting offset=-1 (the default), successive calls to this function incrementally stacks
                 * new text labels above the existing labels
                 * @param text text label
                 * @param r red
                 * @param g green
                 * @param b blue
                 * @param offset height offset in pixels, set to < 0 for auto
                 * @param fontSize font size
                 * @param id label
                 */
                void addText(const std::string& text,
                             unsigned char r = 255,
                             unsigned char g = 255,
                             unsigned char b = 255,
                             int offset = -1,
                             int fontSize = 15,
                             const std::string& id = "");
                
                /**
                 * Return true if the internal visualizer already has an actor with the given label
                 * @param label label
                 * @return true if an actor with that label already exists
                 */
                inline bool hasCloud(const std::string& label) {
                    return visu.getCloudActorMap()->find(label) != visu.getCloudActorMap()->end();
                }
                
                /**
                 * Set a constant background color
                 * @param bg background color
                 */
                inline void setBackgroundColor(const pcl::RGB& bg) {
                    _bg = bg;
                    setBackgroundGradient(false);
                }
                
                /**
                 * Set background color
                 * @param r red
                 * @param g green
                 * @param b blue
                 */
                inline void setBackgroundColor(uchar r, uchar g, uchar b) {
                    _bg.r = r;
                    _bg.g = g;
                    _bg.b = b;
                }

                /**
                 * Set whether to use background gradient going from the specified background color
                 * (@ref setBackgroundColor()) to black
                 * @param bgGradient background color
                 */
                inline void setBackgroundGradient(bool bgGradient) {
                    _bgGradient = bgGradient;
                }
                
                /**
                 * Get an RGB tuple for the jet color map
                 * [blue @f$\rightarrow@f$ cyan @f$\rightarrow@f$ green @f$\rightarrow@f$ yellow @f$\rightarrow@f$ red]
                 * 
                 * See http://paulbourke.net/texture_colour/colourspace, section on 
                 * "Colour Ramping for Data Visualisation"
                 * @param v offset in [0,1] into the jet, where 0 means blue and 1 means red
                 * @param r output red component in [0,1]
                 * @param g output green component in [0,1]
                 * @param b output blue component in [0,1]
                 */
                void jet(double v, double& r, double& g, double& b);

                /**
                 * Get an RGB tuple for a red to green colormap
                 * @param v offset in [0,1] into the color map, where 0 means read and 1 means green
                 * @param r output red component in [0,1]
                 * @param g output green component in [0,1]
                 * @param b output blue component in [0,1]
                 */
                void redgreen(double v, double& r, double& g, double& b);
                
                /**
                 * Set origo flag
                 * @param showOrigo origo flag
                 */
                inline void setShowOrigo(bool showOrigo) {
                    _showOrigo = showOrigo;
                }
                
                /**
                 * Start the visualization window
                 * @note This function is blocking
                 */
                inline virtual void show() {
                    setup();
                    visu.spin();
                }

            protected:
                /// Background color
                pcl::RGB _bg;

            private:
                /// Background gradient
                bool _bgGradient;
                
                /// Set to true to show origo
                bool _showOrigo;
                
                /// Setup flag: if the window has never been shown, it is false
                bool _setup;
                
                /// Used to keep track of the current text offset when using @ref addText()
                size_t _textOffset;

                /**
                 * Prepare the visualizer first time
                 */
                inline void setup() {
                    if(!_setup) {
                        _setup = true;
                        visu.createInteractor();
                        visu.setShowFPS(false);
                        visu.setWindowName(_title);
                        visu.getRendererCollection()->InitTraversal();
                        vtkRenderer* renderer = visu.getRendererCollection()->GetNextItem();
                        while(renderer != NULL) {
                            renderer->SetGradientBackground(_bgGradient);
                            if(_bgGradient) {
                                renderer->SetBackground2(double(_bg.r) / 255.0,
                                                         double(_bg.g) / 255.0,
                                                         double(_bg.b) / 255.0); // Top color
                                renderer->SetBackground(0, 0, 0); // Bottom color
                            } else {
                                renderer->SetBackground(double(_bg.r) / 255.0,
                                                        double(_bg.g) / 255.0,
                                                        double(_bg.b) / 255.0);
                            }
                            renderer = visu.getRendererCollection()->GetNextItem();
                        }
                        if(!visu.cameraFileLoaded())
                            visu.setCameraPosition(0, 0, -1, 0, 0, 1, 0, -1, 0); // 1 m behind origo, focusing at (0,0,1)
                        if(_showOrigo)
#if PCL_VERSION_COMPARE(>=,1,7,2)
                            visu.addCoordinateSystem(0.25, "reference", 0);
#else
                            visu.addCoordinateSystem(0.25);
#endif
                        visu.setSize(1280, 720);
                    }
                }

                /**
                 * Add a point cloud to a visualizer, automatically use intensity and RGB information if available
                 * @param visu visualizer
                 * @param cloud point cloud
                 * @param label label of the point cloud
                 */
                template<typename PointT>
                void add(VisuT& visu, typename pcl::PointCloud<PointT>::ConstPtr cloud, const std::string& label);
        };
        
        /**
         * @ingroup visu
         * @brief Show a point cloud
         * @param cloud point cloud
         * @param title window title
         */
        template<typename PointT>
        inline void showPointCloud(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                const std::string& title = "Point cloud visualization") {
            Visu3D vb(title);
            vb.addPointCloud<PointT>(cloud);
            vb.show();
        }
        
        /**
         * @ingroup visu
         * @brief Show a point cloud from XYZ data in an n-by-3 Eigen matrix
         * @param xyz point cloud XYZ data, stored an in n-by-3 Eigen matrix
         * @param title window title
         */
        void showPointCloud(const Eigen::MatrixXf& xyz, const std::string& title = "Point cloud visualization");

        /**
         * @ingroup visu
         * @brief Show a point cloud from a binary blob
         * @param cloud point cloud
         * @param title window title
         */
        void showPointCloud(const pcl::PCLPointCloud2::Ptr& cloud, const std::string& title = "Point cloud visualization");
        
        /**
         * @ingroup visu
         * @brief Show a mesh
         * @param mesh mesh
         * @param title window title
         */
        void showMesh(pcl::PolygonMesh::ConstPtr mesh, const std::string& title = "Mesh visualization");

        /**
         * @ingroup visu 
         * @brief Show a mesh with vertex dots and normals
         * @note The point and normal coordinates for the vertices are taken from the mesh
         * @param mesh mesh
         * @param level show every level'th normal
         * @param scale scale normals by this value - if set to 0, use 10 x the resolution of the point cloud
         * @param title window title
         */
        void showMeshNormals(pcl::PolygonMesh::ConstPtr mesh,
                size_t level = 1,
                float scale = 0,
                const std::string& title = "Mesh and normal visualization");

        /**
         * @ingroup visu 
         * @brief Show a mesh with keypoints
         * @param mesh mesh
         * @param keypoints keypoints, shown in red
         * @param title window title
         */
        template<typename PointT>
        inline void showMeshKeypoints(pcl::PolygonMesh::ConstPtr mesh,
                typename pcl::PointCloud<PointT>::ConstPtr keypoints,
                const std::string& title = "Mesh and keypoint visualization") {
            Visu3D vb(title);
            vb.addMesh(mesh);
            vb.addColor<PointT>(keypoints, 255, 0, 0, "keypoints");
            vb.visu.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "keypoints");
            vb.show();
        }
        
        /**
         * @ingroup visu
         * @brief Show a normal point cloud
         * @param cloud point cloud with normals
         * @param level show every level'th normal
         * @param scale scale normals by this value - if set to 0, use 10 x the resolution of the point cloud
         * @param title window title
         */
        template<typename PointNT>
        inline void showNormals(typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                size_t level = 1,
                float scale = 0,
                const std::string& title = "Normal visualization") {
            Visu3D vb(title);
            if(scale > 0)
                vb.addNormals<PointNT>(cloud, level, scale);
            else
                vb.addNormals<PointNT>(cloud, level, 10 * detect::computeResolution<PointNT>(cloud));
            vb.show();
        }

        /**
         * @ingroup visu
         * @brief Show a normal point cloud
         * @param cloud point cloud with normals
         * @param level show every level'th normal
         * @param scale scale normals by this value - if set to 0, use 10 x the resolution of the point cloud
         * @param title window title
         */
        void showNormals(const pcl::PCLPointCloud2::Ptr& cloud,
                size_t level = 1,
                float scale = 0,
                const std::string& title = "Normal visualization");
        
        /**
         * @ingroup visu
         * @brief Visualize point cloud normals by the coordinates of the normal vectors on the unit sphere
         * @param normals point cloud with normals
         * @param title window title
         */
        template<typename PointNT>
        inline void showGaussMap(typename pcl::PointCloud<PointNT>::ConstPtr normals,
                const std::string& title = "Gauss map visualization") {
            Visu3D vb(title);
            vb.addGaussMap<PointNT>(normals);
            vb.show();
        }
        
        /**
         * @ingroup visu
         * @brief Show a scalar field
         * @param cloud point cloud
         * @param field values to show for each point
         * @param fname field name
         * @param title window title
         */
        template<typename PointXYZT, typename PointFieldT>
        inline void showScalarField(typename pcl::PointCloud<PointXYZT>::ConstPtr cloud,
                typename pcl::PointCloud<PointFieldT>::ConstPtr field,
                const std::string& fname,
                const std::string& title = "Point cloud visualization") {
            Visu3D vb(title);
            vb.addScalarField<PointXYZT, PointFieldT>(cloud, field, fname);
            vb.show();
        }
        
        /**
         * @ingroup visu
         * @brief Show a scalar field
         * @param cloud point cloud
         * @param fname field name
         * @param title window title
         */
        template<typename PointT>
        inline void showScalarField(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                const std::string& fname,
                const std::string& title = "Point cloud visualization") {
            showScalarField<PointT, PointT>(cloud, cloud, fname, title);
        }
        
        /**
         * @ingroup visu
         * @brief Show a scalar field
         * @param cloud point cloud
         * @param field values to show for each point
         * @param title window title
         * @exception if size(field) != size(cloud)
         */
        template<typename PointT, class ScalarT>
        inline void showScalarField(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                const std::vector<ScalarT>& field,
                const std::string& title = "Point cloud visualization") {
            Visu3D vb(title);
            vb.addScalarField<PointT,ScalarT>(cloud, field);
            vb.show();
        }
        
        /**
         * @ingroup visu
         * @brief Show a vector of poses
         * @param T transformation matrices
         * @param length length of the axes
         * @param title window title
         */
        void showPoses(const std::vector<Eigen::Matrix4f>& T,
                       float length = 0.025,
                       const std::string& title = "Pose visualization");
    }
}

#include "visu_3d_impl.hpp"

#endif
