// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_VISU_DETECTION_VISU_H
#define COVIS_VISU_DETECTION_VISU_H

// Own
#include "visu_3d.h"
#include "../core/detection.h"
#include "../core/macros.h"

// PCL
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

// Eigen
#include<Eigen/StdVector>

namespace covis {
    namespace visu {
        /**
         * @class DetectionVisu
         * @ingroup visu
         * @brief Detection visualizer
         * 
         * Use this class to show the result of one or more object detections in a scene. Set the input object(s) using
         * @ref setQuery()/@ref setQueries(), and the scene data by @ref setTarget(). If you have performed pose
         * estimation, you can use @ref setPose()/@ref setPoses() to set the alignment pose(s) for the object(s). The
         * pose and object vectors must have the same size in this case. It is also possible to show detections by
         * instead using @ref setDetection()/@ref setDetections(). In this case, the detections will index the detected
         * objects using the input vector of objects.
         * 
         * Mesh-based overloads for @ref setQuery()/@ref setQueries() and @ref setTarget() also exist, in which case any
         * point cloud data will be ignored.
         * 
         * @tparam PointT point type for both query and target
         * @sa @ref core::Detection
         * @author Anders Glent Buch
         */
        template<typename PointT = pcl::PointXYZ>
        class DetectionVisu : public Visu3D {
            public:
                /// Matrix type used by this object, just a forward of @ref core::Detection::MatrixT
                typedef core::Detection::MatrixT MatrixT;

                /// Matrix vector type used by this object, just a forward of @ref core::Detection::MatrixVecT
                typedef core::Detection::MatrixVecT MatrixVecT;
                
                /// Empty constructor
                DetectionVisu() : Visu3D("Detections"), _opacity(0.75) {
                    Visu3D::setBackgroundGradient(false);
                    Visu3D::setBackgroundColor(255, 255, 255);
                }

                /// Destructor
                virtual ~DetectionVisu() {}
                
                /// Add everything to the view without showing it - you can use this function to spin the view only once
                void render();
                
                /// @copydoc VisuBase::show()
                inline void show() {
                    render();
                    Visu3D::show();
                }
                
                /**
                 * Set query/object point cloud
                 * @param query query point cloud
                 */
                inline void setQuery(typename pcl::PointCloud<PointT>::ConstPtr query) {
                    _queries.assign(1, query);
                }
                
                /**
                 * Set multiple query/object point clouds
                 * @param queries query point clouds
                 */
                inline void setQueries(const std::vector<typename pcl::PointCloud<PointT>::ConstPtr>& queries) {
                    _queries = queries;
                }
                
                /**
                 * Set multiple query/object point clouds
                 * @param queries query point clouds
                 */
                inline void setQueries(const std::vector<typename pcl::PointCloud<PointT>::Ptr>& queries) {
                    _queries.resize(queries.size());
                    for(size_t i = 0; i < queries.size(); ++i)
                        _queries[i] = queries[i];
                }
                
                /**
                 * Set query/object mesh and do not use the point cloud
                 * @param query query mesh
                 */
                inline void setQuery(pcl::PolygonMesh::ConstPtr query) {
                    _queryMeshes.assign(1, query);
                }
                
                /**
                 * Set multiple query/object meshes and do not use the point clouds
                 * @param queries query meshes
                 */
                inline void setQueries(const std::vector<pcl::PolygonMesh::ConstPtr>& queries) {
                    _queryMeshes.resize(queries.size());
                    for(size_t i = 0; i < queries.size(); ++i)
                        _queryMeshes[i] = queries[i];
                }
                
                /**
                 * Set multiple query/object meshes and do not use the point clouds
                 * @param queries query meshes
                 */
                inline void setQueries(const std::vector<pcl::PolygonMesh::Ptr>& queries) {
                    _queryMeshes.resize(queries.size());
                    for(size_t i = 0; i < queries.size(); ++i)
                        _queryMeshes[i] = queries[i];
                }
                
                /**
                 * Set target/scene point cloud
                 * @param target target point cloud
                 */
                inline void setTarget(typename pcl::PointCloud<PointT>::ConstPtr target) {
                    _target = target;
                }
                
                /**
                 * Set target/scene mesh and do not use the point cloud
                 * @param target target mesh
                 */
                inline void setTarget(pcl::PolygonMesh::ConstPtr target) {
                    _targetMesh = target;
                }
                
                /**
                 * Set object detections
                 * @param detections object detections
                 */
                inline void setDetections(const core::Detection::Vec& detections) {
                    _detections = detections;
                }
                
                /**
                 * Set object detection
                 * @param detection object detection
                 */
                inline void setDetection(const core::Detection& detection) {
                    setDetections(core::Detection::Vec(1, detection));
                }
                
                /**
                 * Set object alignment poses
                 * @warning This function assumes that an equal-sized vector of object models has been input using
                 * @ref setQueries()
                 * @param poses object alignment poses
                 */
                inline void setPoses(const MatrixVecT& poses) {
                    _detections.resize(poses.size());
                    for(size_t i = 0; i < poses.size(); ++i) {
                        _detections[i].idx = i;
                        _detections[i].pose = poses[i];
                    }
                }
                
                /**
                 * Set object alignment pose
                 * @param pose object alignment pose
                 */
                inline void setPose(const MatrixT& pose) {
                    setPoses(MatrixVecT(1,pose));
                }

                /**
                 * Set opacity of the overlaid, aligned models
                 * @param opacity
                 */
                inline void setOpacity(double opacity) {
                    _opacity = opacity;
                }

            private:
                /// Query/object point clouds
                std::vector<typename pcl::PointCloud<PointT>::ConstPtr> _queries;
                
                /// Target/scene point cloud
                typename pcl::PointCloud<PointT>::ConstPtr _target;
                
                /// Query/object meshes
                std::vector<pcl::PolygonMesh::ConstPtr> _queryMeshes;
                
                /// Target/scene mesh
                pcl::PolygonMeshConstPtr _targetMesh;
                
                /// Object detections
                core::Detection::Vec _detections;

                /// Opacity from 0 to 1 of the overlaid, aligned models
                double _opacity;
        };
        
        /**
         * @ingroup visu
         * @brief Show an alignment of two point clouds
         * @param query query/object point cloud
         * @param target target/scene point cloud
         * @param pose alignment pose of query/object
         * @param title window title
         */
        template<typename PointT>
        inline void showDetection(typename pcl::PointCloud<PointT>::ConstPtr query,
                typename pcl::PointCloud<PointT>::ConstPtr target,
                const core::Detection::MatrixT& pose = core::Detection::MatrixT::Identity(),
                const std::string& title = "Detection") {
            DetectionVisu<PointT> dv;
            dv.Visu3D::setTitle(title);
            dv.setQuery(query);
            dv.setTarget(target);
            dv.setPose(pose);
            dv.show();
        }

        /**
         * @ingroup visu
         * @brief Show alignments of several object point clouds (queries) in a scene (target)
         * @param queries query/object point clouds
         * @param target target/scene point cloud
         * @param detections object detections with poses
         * @param title window title
         */
        template<typename PointT>
        inline void showDetections(const std::vector<typename pcl::PointCloud<PointT>::Ptr>&  queries,
                typename pcl::PointCloud<PointT>::ConstPtr target,
                const core::Detection::Vec& detections,
                const std::string& title = "Detections") {
            DetectionVisu<PointT> dv;
            dv.Visu3D::setTitle(title);
            dv.setQueries(queries);
            dv.setTarget(target);
            dv.setDetections(detections);
            dv.show();
        }

        /**
         * @ingroup visu
         * @brief Show alignments of several object meshes (queries) in a point cloud scene (target)
         * @param queries query/object meshes
         * @param target target/scene point cloud
         * @param detections object detections with poses
         * @param title window title
         */
        template<typename PointT>
        inline void showDetections(const std::vector<pcl::PolygonMeshPtr>&  queries,
                                   typename pcl::PointCloud<PointT>::ConstPtr target,
                                   const core::Detection::Vec& detections,
                                   const std::string& title = "Detections") {
            DetectionVisu<PointT> dv;
            dv.Visu3D::setTitle(title);
            dv.setQueries(queries);
            dv.setTarget(target);
            dv.setDetections(detections);
            dv.show();
        }
        
        /**
         * @ingroup visu
         * @brief Show an alignment of two meshes
         * @param query query/object mesh
         * @param target target/scene mesh
         * @param pose alignment pose of query/object
         * @param title window title
         */
        inline void showDetection(pcl::PolygonMesh::ConstPtr query,
                pcl::PolygonMesh::ConstPtr target,
                const core::Detection::MatrixT& pose = core::Detection::MatrixT::Identity(),
                const std::string& title = "Detection") {
            DetectionVisu<pcl::PointXYZ> dv;
            dv.Visu3D::setTitle(title);
            dv.setQuery(query);
            dv.setTarget(target);
            dv.setPose(pose);
            dv.show();
        }

        /**
         * @ingroup visu
         * @brief Show alignments of several object meshes (queries) in a scene (target)
         * @param queries query/object meshes
         * @param target target/scene mesh
         * @param detections object detections with poses
         * @param title window title
         */
        inline void showDetections(const std::vector<pcl::PolygonMeshPtr>&  queries,
                                   pcl::PolygonMesh::ConstPtr target,
                                   const core::Detection::Vec& detections,
                                   const std::string& title = "Detections") {
            DetectionVisu<pcl::PointXYZ> dv;
            dv.Visu3D::setTitle(title);
            dv.setQueries(queries);
            dv.setTarget(target);
            dv.setDetections(detections);
            dv.show();
        }
    }
}

#ifndef COVIS_PRECOMPILE
#include "detection_visu_impl.hpp"
#endif

#endif
