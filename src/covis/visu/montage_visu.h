// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_VISU_MONTAGE_VISU_H
#define COVIS_VISU_MONTAGE_VISU_H

// Own
#include "visu_2d_base.h"

// OpenCV
#include <opencv2/core/core.hpp>

namespace covis {
    namespace visu {
        /**
         * @class MontageVisu
         * @ingroup visu
         * @brief Tile an array of images and show them in a montage
         * 
         * This class can handle images of different sizes, depths and number of channels by converting all
         * images to the same type (CV_32F/CV_32FC3 with normalization to [0,1])
         * 
         * @author Anders Glent Buch
         */
        class MontageVisu : public Visu2DBase {
            using VisuBase::_title;
            
            public:
                /// Default constructor: setup default parameters
                MontageVisu() : Visu2DBase("Montage"), _images(NULL), _n(0), _rows(0), _spacing(10),
                        _bg(cv::Scalar::all(0.75)), _scale(0.5), _centering(true) {}
            
                /// Destructor
                virtual ~MontageVisu() {}
                
                /// @copydoc VisuBase::show()
                void show();
                
                /**
                 * Perform the tiling of the input images without actually showing them
                 * @return tiled image, always type CV_32F/CV_32FC3, depending on whether the input has any RGB images
                 */
                cv::Mat tile();
                
                /**
                 * Set image data
                 * @param images pointer to image array
                 * @param n number of images in array
                 */
                inline void setImages(const cv::Mat* images, size_t n) {
                    _images = images;
                    _n = n;
                }
                
                /**
                 * Set image data
                 * @param images vector of images
                 */
                inline void setImages(const std::vector<cv::Mat>& images) {
                    _images = images.data();
                    _n = images.size();
                }
                
                /**
                 * Set of image rows to show in the montage (0 for automatic)
                 * @param rows of image rows to show in the montage (0 for automatic)
                 */
                inline void setRows(size_t rows ) {
                    _rows = rows;
                }
                
                /**
                 * Set space between images in pixels
                 * @param spacing space between images in pixels
                 */
                inline void setSpacing(size_t spacing) {
                    _spacing = spacing;
                }
                
                /**
                 * Set background color
                 * @param bg background color
                 */
                inline void setBackgroundColor(const cv::Scalar& bg) {
                    _bg = bg;
                }
                
                /**
                 * Set scale
                 * @param scale scale
                 */
                inline void setScale(double scale) {
                    _scale = scale;
                }
                
                /**
                 * Set centering flag
                 * @param centering centering flag
                 */
                inline void setCentering(bool centering) {
                    _centering = centering;
                }
                
            private:
                /// Pointer to image array
                const cv::Mat* _images;
                
                /// Number of images in array
                size_t _n;
                
                /// Number of image rows to show in the montage (0 for automatic)
                size_t _rows;
                
                /// Space between images in pixels
                size_t _spacing;
                
                /// Background color
                cv::Scalar _bg;
                
                /// Scaling to apply to input images before inserting into montage
                double _scale;
                
                /** 
                 * If the input images have unequal sizes, each image is placed in a subwindow with size equal to
                 * the largest image size. All smaller images are then padded with the background color before insertion
                 * into the montage. This flag controls whether the image should be placed in the center of its
                 * subwindow (true) or in the top left corner (false)
                 */
                bool _centering;
        };
        
        /**
         * @ingroup visu
         * 
         * Tile a list of images in a montage
         * 
         * This function can handle images of different sizes, depths and number of channels by converting all
         * images to the same type (CV_32F/CV_32FC3 with normalization to [0,1])
         * 
         * @param images image array pointer, varying numbers of channels (1, 3 or 4) and depths
         * (32/64 bit float and 8 bit unsigned) are allowed
         * @param n number of images in array
         * @param rows number of image rows to use in the montage (0 for automatic)
         * @param spacing number of background pixels to put between each image 
         * @param bg background color, all elements should be in normalized range [0,1]
         * @param scale scaling to apply to each image before it is inserted into the montage
         * @param center if the input images have unequal sizes, each image is placed in a subwindow with size equal to
         * the largest image size. All smaller images are then padded with the background color before insertion into
         * the montage. This flag controls whether the image should be placed in the center of its subwindow (true) or
         * in the top left corner (false)
         * @return tiled image, always of type CV_32F/CV_32FC3, depending on whether the input contains any RGB images
         */
        inline cv::Mat tile(const cv::Mat* images, size_t n, size_t rows = 0, size_t spacing = 10,
                const cv::Scalar& bg = cv::Scalar::all(0.75), double scale = 0.5, bool center = true) {
            MontageVisu mv;
            mv.setImages(images, n);
            mv.setRows(rows);
            mv.setSpacing(spacing);
            mv.setBackgroundColor(bg);
            mv.setScale(scale);
            mv.setCentering(center);
            
            return mv.tile();
        }
        
        /**
         * @ingroup visu
         * 
         * Tile a list of images in a montage
         * 
         * This function can handle images of different sizes, depths and number of channels by converting all
         * images to the same type (CV_32F/CV_32FC3 with normalization to [0,1])
         * 
         * @param images image vector, varying numbers of channels (1, 3 or 4) and depths
         * (32/64 bit float and 8 bit unsigned) are allowed
         * @param rows number of image rows to use in the montage (0 for automatic)
         * @param spacing number of background pixels to put between each image 
         * @param bg background color, all elements should be in normalized range [0,1]
         * @param scale scaling to apply to each image before it is inserted into the montage
         * @param center if the input images have unequal sizes, each image is placed in a subwindow with size equal to
         * the largest image size. All smaller images are then padded with the background color before insertion into
         * the montage. This flag controls whether the image should be placed in the center of its subwindow (true) or
         * in the top left corner (false)
         * @return tiled image, always of type CV_32F/CV_32FC3, depending on whether the input contains any RGB images
         */
        inline cv::Mat tile(const std::vector<cv::Mat>& images, size_t rows = 0, size_t spacing = 10,
                const cv::Scalar& bg = cv::Scalar::all(0.75), double scale = 0.5, bool center = true) {
            return tile(images.data(), images.size(), rows, spacing, bg, scale, center);
        }

        
        /**
         * @ingroup visu
         * 
         * Show a list of images in a montage
         * 
         * This function can handle images of different sizes, depths and number of channels by converting all
         * images to the same type (CV_32F/CV_32FC3 with normalization to [0,1])
         * 
         * @param title title of the window
         * @param images image array pointer, varying numbers of channels (1, 3 or 4) and depths
         * (32/64 bit float and 8 bit unsigned) are allowed
         * @param n number of images in array
         * @param rows number of image rows to use in the montage (0 for automatic)
         * @param spacing number of background pixels to put between each image 
         * @param bg background color, all elements should be in normalized range [0,1]
         * @param scale scaling to apply to each image before it is inserted into the montage
         * @param center if the input images have unequal sizes, each image is placed in a subwindow with size equal to
         * the largest image size. All smaller images are then padded with the background color before insertion into
         * the montage. This flag controls whether the image should be placed in the center of its subwindow (true) or
         * in the top left corner (false)
         * @return tiled image, always of type CV_32F/CV_32FC3, depending on whether the input contains any RGB images
         */
        inline void showMontage(const std::string& title,
                const cv::Mat* images, size_t n, size_t rows = 0, size_t spacing = 10,
                const cv::Scalar& bg = cv::Scalar::all(0.75), double scale = 0.5, bool center = true) {
            MontageVisu mv;
            mv.setTitle(title);
            mv.setImages(images, n);
            mv.setRows(rows);
            mv.setSpacing(spacing);
            mv.setBackgroundColor(bg);
            mv.setScale(scale);
            mv.setCentering(center);
            
            mv.show();
        }
        
        /**
         * @ingroup visu
         * 
         * Show a list of images in a montage
         * 
         * This function can handle images of different sizes, depths and number of channels by converting all
         * images to the same type (CV_32F/CV_32FC3 with normalization to [0,1])
         * 
         * @param title title of the window
         * @param images image vector, varying numbers of channels (1, 3 or 4) and depths
         * (32/64 bit float and 8 bit unsigned) are allowed
         * @param rows number of image rows to use in the montage (0 for automatic)
         * @param spacing number of background pixels to put between each image 
         * @param bg background color, all elements should be in normalized range [0,1]
         * @param scale scaling to apply to each image before it is inserted into the montage
         * @param center if the input images have unequal sizes, each image is placed in a subwindow with size equal to
         * the largest image size. All smaller images are then padded with the background color before insertion into
         * the montage. This flag controls whether the image should be placed in the center of its subwindow (true) or
         * in the top left corner (false)
         * @return tiled image, always of type CV_32F/CV_32FC3, depending on whether the input contains any RGB images
         */
        inline void showMontage(const std::string& title,
                const std::vector<cv::Mat>& images, size_t rows = 0, size_t spacing = 10,
                const cv::Scalar& bg = cv::Scalar::all(0.75), double scale = 0.5, bool center = true) {
            showMontage(title, images.data(), images.size(), rows, spacing, bg, scale, center);
        }
    }
}

#endif