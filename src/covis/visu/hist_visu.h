// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_VISU_HIST_VISU_H
#define COVIS_VISU_HIST_VISU_H

// Own
#include "visu_2d_base.h"
#include "../core/macros.h"

namespace covis {
    namespace visu {
        /**
         * @class HistVisu
         * @ingroup visu
         * @brief Histogram visualizer
         * 
         * Use this class by first setting the histogram data by @ref covis::visu::HistVisu::setHist() "setHist()",
         * followed by a call to @ref covis::visu::VisuBase::show() "show()".
         * 
         * If you need a function for computing a histogram from a data sample, have a look at
         * @ref covis::core::hist() "core::hist()".
         *
         * @tparam N number of histogram bins
         * @tparam T element type for the histogram data
         * @author Anders Glent Buch
         * @example example/histogram_visualization/histogram_visualization.cpp
         */
        template<typename T = float>
        class HistVisu : public Visu2DBase {
            using VisuBase::_title;
            
            public:
                /**
                 * Constructor: initialize dimension
                 * @param dim dimension (defaults to 0)
                 */
                HistVisu(size_t dim = 0) : Visu2DBase("Histogram"), _dim(dim), _data(NULL), _size(640, 240) {}

                /// Destructor
                virtual ~HistVisu() {}
                
                /// @copydoc VisuBase::show()
                void show();

                /**
                 * Set histogram dimension
                 * @param dim dimension
                 */
                inline void setDim(size_t dim) {
                    _dim = dim;
                }
                
                /**
                 * Set pointer to histogram data
                 * @param data pointer to histogram data, must contain N valid entries
                 */
                inline void setHist(const T* data) {
                    _data = data;
                }
                
                /**
                 * Set window size
                 * @param size window size
                 */
                inline void setSize(const cv::Size& size) {
                    _size = size;
                }
                
            private:
                /// Histogram dimension
                size_t _dim;

                /// Pointer to histogram data
                const T* _data;
                
                /// Window size
                cv::Size _size;
        };

        /**
         * @ingroup visu
         * Show a histogram
         * @param data pointer to histogram data, must contain N valid entries
         * @param size window size
         * @tparam N number of histogram bins
         * @tparam T element type for the histogram data
         */
        template<size_t N, typename T>
        inline void showHistogram(const T* data, const cv::Size& size = cv::Size(640, 240)) {
            HistVisu<T> hv(N);
            hv.setHist(data);
            hv.setSize(size);
            hv.show();
        }
    }
}

#include "hist_visu_impl.hpp"

#endif
