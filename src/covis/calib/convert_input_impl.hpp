
#ifndef COVIS_CALIB_CONVERT_INPUT_IMPL_HPP
#define COVIS_CALIB_CONVERT_INPUT_IMPL_HPP

#include "convert_input.h"

#include <pcl/io/pcd_io.h>

using namespace covis::calib;

namespace covis {
    namespace calib {
        template<typename PointRGB>
        cv::Mat ConvertInput<PointRGB>::getRgbImageFromPointCloud(
                const typename pcl::PointCloud<PointRGB>::Ptr &cloud){

            cv::Mat_<cv::Vec3b> result;

            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr rgbPointCloud (new pcl::PointCloud<pcl::PointXYZRGBA>);
            pcl::copyPointCloud(*cloud, *rgbPointCloud);

            if (rgbPointCloud->isOrganized()) {
                result = cv::Mat_<cv::Vec3b>(cloud->height, cloud->width);

                for (int h = 0; h< result.rows; h++) {
                    for (int w = 0; w< result.cols; w++) {
                        pcl::PointXYZRGBA point = rgbPointCloud->at(w, h);
                        Eigen::Vector3i rgb = point.getRGBVector3i();
                        result.at<cv::Vec3b>(h,w)[0] = rgb[2];
                        result.at<cv::Vec3b>(h,w)[1] = rgb[1];
                        result.at<cv::Vec3b>(h,w)[2] = rgb[0];
                    }
                }
            }
            return result;
        }
    }
}

#endif /* COVIS_CONVERT_INPUT_IMPL_HPP */
