#include "progress_display.h"

using namespace covis::core;

ProgressDisplay& ProgressDisplay::operator+=(size_t inc) {
    if (!_done) {
        // Increment
        _i += inc;

        if (_abs) {
            // Delete current line
            for (int i = 0; i < _cnt; ++i)
                printf("\b");

            // Check if we are done
            if (_i >= _iter) {
                printf("> %lu/%lu\n", _iter, _iter);
                _done = true;
            } else {
                // Write progress
                _cnt = printf("> %lu/%lu", _i, _iter);
            }
        } else {
            // Get progress in %
            const int pct( 100.0 * double(_i) / double(_iter - 1) + 0.5 );

            // Check if we are done
            if (pct >= 100) {
                // Delete current line
                for (int i = 0; i < _cnt; ++i)
                    printf("\b");
                printf("> 100 %%\n");
                _done = true;
            } else if(pct != _pct) { // Only write percentage if it changed yet
                // Delete current line
                for (int i = 0; i < _cnt; ++i)
                    printf("\b");
                // Write progress
                _cnt = printf("> %i %%", pct);
            }

            // Update percentage
            _pct = pct;
        }

        // Flush
        fflush(stdout);
    }

    return *this;
}

ProgressDisplay& ProgressDisplay::operator++() {
    return operator+=(1);
}

void ProgressDisplay::operator++(int) {
    // Increment
    operator+=(1);
}
