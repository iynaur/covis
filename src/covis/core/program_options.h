// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.


#ifndef COVIS_CORE_PROGRAM_OPTIONS_H
#define COVIS_CORE_PROGRAM_OPTIONS_H

// Own
#include "macros.h"

// Boost
#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
namespace po = boost::program_options;

// C
#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>

namespace covis {
    namespace core {
        /**
         * @class ProgramOptions
         * @ingroup core
         * @brief Simple wrapper class for creating program options
         *
         * Use this class to create program options for your applications. Options come in three types:
         *  - Long options prefixed by '\-\-'
         *  - Flags prefixed by either '-' or '\-\-'
         *  - Positional options (no prefix)
         *
         * Create an instance of this class, and add the options you wish to present to the user of your program.
         * Flags are assumed a value of false if not given by the user, and true if set in the command-line.
         * Long options must be provided by the user, if no default value is given by you. Upon execution,
         * a warning message will be presented if this happens.
         * Positional options, if specified, are also required.
         *
         * Example
         * @code
         * int main(int argc, char** argv) {
         *     ProgramOptions po;
         *     po.addFlag('v', "verbose", "enable printing");
         *     po.addOption<int>("iter", 'i', 10, "number of iterations"); // Default value of 10
         *     po.addPositional("infiles", "two input files", 2);
         *
         *     if(!po.parse(argc, argv))
         *         return 1;
         *
         *     const bool verbose = po.getFlag("verbose");
         *     const int iter = po.getValue<int>("iter");
         *     const std::vector<std::string> infile = po.getVector("infiles");
         *
         *     return 0;
         * }
         * @endcode
         *
         * @author Anders Glent Buch
         */
        class ProgramOptions {
            public:
                /**
                 * Empty constructor
                 */
                ProgramOptions() : _maxlen(0) {}

                /**
                 * Empty destructor
                 */
                virtual ~ProgramOptions() {}

                /**
                 * Add a positional option to the option list, with the possibility that multiple entries can be given
                 * @param name long name
                 * @param desc description
                 * @param maxnum maximal number of positionals (-1 for inifinte)
                 */
                void addPositional(const std::string& name, const std::string& desc, int maxnum = 1);

                /**
                 * Add a flag to the option list
                 * @param f flag name, prefixed by '-'
                 * @param name long name, prefixed by '\-\-'
                 * @param desc description
                 */
                void addFlag(char f, const std::string& name, const std::string& desc);

                /**
                 * Add a flag to the option list
                 * @param f flag name, prefixed by either '-' or '\-\-'
                 * @param desc description
                 */
                void addFlag(char f, const std::string& desc);

                /**
                 * Add a flag to the option list
                 * @param name long name, prefixed by '\-\-'
                 * @param desc description
                 */
                void addFlag(const std::string& name, const std::string& desc);

                /**
                 * Add a value of any type to the option list
                 * @param name long name, prefixed by '\-\-'
                 * @param desc description
                 */
                void addOption(const std::string& name, const std::string& desc);

                /**
                 * Add a value of any type to the option list
                 * @param name long name, prefixed by '\-\-'
                 * @param shortName short name, prefixed by '-', just like flags
                 * @param desc description
                 */
                inline void addOption(const std::string& name, char shortName, const std::string& desc) {
                    addOption(name + "," + std::string(1, shortName), desc);
                }

                /**
                 * Add a value of any type to the option list, with default value
                 * @param name long name, prefixed by '\-\-'
                 * @param defaultValue default value
                 * @param desc description
                 */
                template<typename T>
                inline void addOption(const std::string& name, const T& defaultValue, const std::string& desc) {
                    COVIS_ASSERT_MSG(_vm.empty(), "Options already parsed, new options cannot be added!");
                    COVIS_ASSERT(!name.empty());

                    _options.add_options()(name.c_str(),
                                           po::value<std::string>()->default_value(boost::lexical_cast<std::string>(defaultValue)),
                                           desc.c_str());

                    _maxlen = std::max<size_t>(_maxlen, name.size());
                }

                /**
                 * Add a value of any type to the option list, with default value
                 * @param name long name, prefixed by '\-\-'
                 * @param shortName short name, prefixed by '-', just like flags
                 * @param defaultValue default value
                 * @param desc description
                 */
                template<typename T>
                inline void addOption(const std::string& name, char shortName,
                        const T& defaultValue, const std::string& desc) {
                    addOption<T>(name + "," + std::string(1, shortName), defaultValue, desc);
                }

                /**
                 * Add a string value to the option list, with default value
                 * @param name long name, prefixed by '\-\-'
                 * @param defaultValue default value
                 * @param desc description
                 */
                void addOption(const std::string& name, const std::string& defaultValue, const std::string& desc);

                /**
                 * Add a string value to the option list, with default value
                 * @param name long name, prefixed by '\-\-'
                 * @param shortName short name, prefixed by '-', just like flags
                 * @param defaultValue default value
                 * @param desc description
                 */
                inline void addOption(const std::string& name, char shortName,
                        const std::string& defaultValue, const std::string& desc) {
                    addOption(name + "," + std::string(1, shortName), defaultValue, desc);
                }

                /**
                 * Get the value of a flag
                 * @param name long name
                 * @return flag value
                 */
                inline bool getFlag(const std::string& name) {
                    COVIS_ASSERT_MSG(!_vm.empty(), "Options not parsed, use parse()!");

                    return _vm.count(name);
                }

                /**
                 * Get the value of a flag
                 * @param f flag name
                 * @return flag value
                 */
                inline bool getFlag(char f) {
                    return getFlag(std::string(&f, 1));
                }

                /**
                 * Get the value of an option
                 * @param name long name
                 * @return option value
                 * @exception if the option does not exist, an exception is thrown
                 */
                template<typename T>
                inline T getValue(const std::string& name) {
                    COVIS_ASSERT_MSG(!_vm.empty(), "Options not parsed, use parse()!");
                    COVIS_ASSERT_MSG(_vm.count(name), "Option \"" << name << "\" does not exist!");

                    T result;

                    try {
                        result = boost::lexical_cast<T>(_vm.at(name).as<std::string>());
//                        result = _vm.at(name).as<T>(); // This will not work since all values are stored as strings
                    } catch (const std::exception& e) {
                        COVIS_THROW(e.what());
                    }

                    return result;
                }

                /**
                 * Get the value of an option as a string
                 * @param name long name
                 * @return option value string
                 * @exception if the option does not exist, an exception is thrown
                 */
                inline std::string getValue(const std::string& name) {
                    return getValue<std::string>(name);
                }

                /**
                 * Get the value of an option, vector version
                 * @param name long name
                 * @param delim delimiters to use
                 * @return option values
                 */
                template<typename T>
                inline std::vector<T> getVector(const std::string& name, const std::string& delim = " \t\n,;") {
                    COVIS_ASSERT_MSG(!_vm.empty(), "Options not parsed, use parse()!");
                    COVIS_ASSERT_MSG(_vm.count(name), "Option \"" << name << "\" does not exist!");

//                    // Find out whether the name is a positional or an option
//                    bool ispos = false;
//                    for(std::vector<std::pair<std::string, std::string> >::const_iterator it = _posvec.begin(); it != _posvec.end(); ++it) {
//                        if(it->first == name) {
//                            ispos = true;
//                            break;
//                        }
//                    }

                    std::vector<T> result;
                    
                    // First get as a string vector
                    try {
                        std::vector<std::string> tmp;
                        boost::split(tmp, _vm.at(name).as<std::string>(), boost::is_any_of(delim), boost::token_compress_on);
                        
                        // Then convert to output type
                        result.reserve(tmp.size());
                        for(size_t i = 0; i < tmp.size(); ++i)
                            if(!tmp[i].empty()) // I would have thought that boost::split() should avoid this from ever happening!
                                result.push_back(boost::lexical_cast<T>(tmp[i]));
                    } catch (const std::exception& e) {
                        COVIS_THROW(e.what());
                    }
                    
                    return result;
                    
//                    if(ispos) { // Positional
//                        // First get as a string vector
//                        try {
//                            boost::split(tmp, _vm.at(name).as<std::string>(), boost::is_any_of(delim), boost::token_compress_on);
////                            tmp = _vm.at(name).as<std::vector<std::string> >();
//                            
//                            // Then convert to output type
//                            std::vector<T> result;
//                            result.reserve(tmp.size());
//                            for(size_t i = 0; i < tmp.size(); ++i)
//                                if(!tmp[i].empty()) // I would have thought that boost::split() should avoid this from ever happening!
//                                    result.push_back(boost::lexical_cast<T>(tmp[i]));
//                            
//                            return result;
//                        } catch (const std::exception& e) {
//                            COVIS_THROW(e.what());
//                        }
//                    } else { // Option
//                        try {
//                            boost::split(tmp, _vm.at(name).as<std::string>(), boost::is_any_of(delim), boost::token_compress_on);
//
//                            // Then convert to output type
//                            std::vector<T> result;
//                            result.reserve(tmp.size());
//                            for(size_t i = 0; i < tmp.size(); ++i)
//                                if(!tmp[i].empty()) // I would have thought that boost::split() should avoid this from ever happening!
//                                    result.push_back(boost::lexical_cast<T>(tmp[i]));
//                            
//                            return result;
//                        } catch (const std::exception& e) {
//                            COVIS_THROW(e.what());
//                        }
//                    }
                }


                /**
                 * Get the value of an option as a string, vector version
                 * @param name long name
                 * @param delim delimiters to use
                 * @return option string values
                 */
                inline std::vector<std::string> getVector(const std::string& name, const std::string& delim = " \t\n,;") {
                    return getVector<std::string>(name, delim);
                }

                /**
                 * Process command line and print help if needed
                 * @param argc argument count
                 * @param argv argument vector
                 * @return true if success
                 */
                bool parse(int argc, const char** argv);

                /**
                 * Print the value of all options
                 */
                void print();

            private:
                /// Binary options, only used for printing
                po::options_description _flags;

                /// Real-valued options, only used for printing
                po::options_description _options;

                /// Hidden options, used for naming the positionals
                po::options_description _hidden;

                /// Positionals
                po::positional_options_description _positionals;

                /// Vector of <name,description> of the positionals, used for printing help
                std::vector<std::pair<std::string, std::string> > _posvec;

                /// All options
                po::options_description _all;

                /// Output list of option values
                po::variables_map _vm;

                /// Max string length of all options, used for printing
                size_t _maxlen;
        };
    }
}
#endif
