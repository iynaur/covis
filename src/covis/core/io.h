// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_CORE_IO_H
#define COVIS_CORE_IO_H

// Eigen
#include <eigen3/Eigen/Core>

namespace covis {
    namespace core {
        /**
         * @ingroup core
         * @brief Stringify a value
         * @param value input value
         * @return string representation of value
         */
        template<typename T>
        std::string stringify(const T& value);
        
        /**
         * @ingroup core
         * @brief Stringify a floating point value with specified precision
         * @param value input value
         * @param precision number of digits
         * @return string representation of value
         */
        template<typename RealT>
        std::string stringify(const RealT& value, size_t precision);

        /**
         * Return true if a file exists, false if not
         * @param filepath input file path
         * @return exist status
         */
        bool exist(const std::string& filepath);

        /**
         * Return the name of a file (the part of the path after the directory)
         * @param filepath input file path
         * @return file name
         */
        std::string filename(const std::string& filepath);

        /**
         * Return the extension of a file (without the dot) in lower-case
         * @param filepath input file path
         * @return extension
         */
        std::string extension(const std::string& filepath);

        /**
         * Return the stem of a file (i.e. the part before the extension)
         * @param filepath input file path
         * @return stem
         */
        std::string stem(const std::string& filepath);

        /**
         * Return the number of lines in an ASCII file
         * @param filepath input file path
         * @return number of lines
         * @sa @ref rowscols()
         */
        size_t lines(const std::string& filepath);

        /**
         * Return the number of rows (lines) and columns in an ASCII file
         * @param filepath input file path
         * @return number of rows and columns as a pair
         * @sa @ref lines()
         */
        std::pair<size_t,size_t> rowscols(const std::string& filepath);
    }
}

#include "io_impl.hpp"

#endif
