set(SOURCES
    correspondence.cpp
    detection.cpp
    io.cpp
    macros.cpp
    program_options.cpp
    progress_display.cpp
    random.cpp
    range.cpp
    scoped_timer.cpp
    stat.cpp
    transform.cpp
    PARENT_SCOPE
   )

