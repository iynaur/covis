// Copyright (c) 2016, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_CORE_FEEDFORWARD_NET_H
#define COVIS_CORE_FEEDFORWARD_NET_H

//Eigen
#include <Eigen/src/Core/Matrix.h>

//Own
#include "covis/core/ptf.h"

namespace covis {
    namespace core {
        /**
        * @ingroup core
        * @struct FeedForward Network
        * @brief Implementation of 3 hidden layer neural network with TanSig
        *
        * Use the following matlab code to generate the use xml output from matlab training
        * @code
        * fileID = fopen('{name}.xml','w');
        * fprintf(fileID,'%6s\n', '<?xml version="1.0"?>');
        * fprintf(fileID,'%6s\n', '<opencv_storage>');
        * a = size(x1_step1.xoffset);
        * wfile(fileID, 'x1_xoffset', a(1), a(2), x1_step1.xoffset, 'f');
        * a = size(x1_step1.gain);
        * wfile(fileID, 'x1_gain', a(1), a(2), x1_step1.gain, 'f');
        * a = size(x1_step1.ymin);
        * wfile(fileID, 'x1_ymin', a(1), a(2), x1_step1.ymin, 'i');
        * a = size(b1);
        * wfile(fileID, 'b1', a(1), a(2), b1, 'f');
        * a = size(IW1_1);
        * wfile(fileID, 'iw1', a(1), a(2), transpose(IW1_1), 'f');
        * a = size(b2);
        * wfile(fileID, 'b2', a(1), a(2), b2, 'f');
        * a = size(LW2_1);
        * wfile(fileID, 'iw2', a(1), a(2), transpose(LW2_1), 'f');
        * a = size(b3);
        * wfile(fileID, 'b3', a(1), a(2), b3, 'f');
        * a = size(LW3_2);
        * wfile(fileID, 'iw3', a(1), a(2), transpose(LW3_2), 'f');
        * a = size(b4);
        * wfile(fileID, 'b4', a(1), a(2), b4, 'f');
        * a = size(LW4_3);
        * wfile(fileID, 'iw4', a(1), a(2), transpose(LW4_3), 'f');
        * fprintf(fileID,'%6s\n', '</opencv_storage>');
        * fclose(fileID);
        * -------------------------------------------------------------
        * function [w] = wfile(fileID, name, rows, cols, data, type)
        *  fprintf(fileID,'<%s type_id="opencv-matrix">\n', name);
        *  fprintf(fileID,'\t%6s %d %6s\n', '<rows>', rows,  '</rows>');
        *  fprintf(fileID,'\t%6s %d %6s\n', '<cols>', cols, '</cols>');
        *  fprintf(fileID,'\t%6s %s %6s\n', '<dt>', type, '</dt>');
        *  fprintf(fileID,'\t%s ', '<data>');
        *  if type == 'i'
        *      fprintf(fileID,'%d ', data);
        *  else
        *      fprintf(fileID,'%.12f ', data);
        *  end
        *  fprintf(fileID,'%s\n', '</data>');
        *  fprintf(fileID,'</%s>\n', name);
        * end
        * @endcode
        *
        * @author Lilita Kiforenko
        *
        */
        class FeedForwardNet {
        public:

            FeedForwardNet() {};

            ~FeedForwardNet() {};


            inline void setX1Offset(Eigen::MatrixXf val) {
                this->_x1_offset = val;
            }

            inline void setX1Gain(Eigen::MatrixXf val) {
                this->_x1_gain = val;
            }

            inline void setX1Ymin(int val) {
                this->_x1_ymin = val;
            }

            inline void setB1(Eigen::MatrixXf val) {
                this->_b1 = val;
            }

            inline void setB2(Eigen::MatrixXf val) {
                this->_b2 = val;
            }

            inline void setB3(Eigen::MatrixXf val) {
                this->_b3 = val;
            }

            inline void setB4(float val) {
                this->_b4 = val;
            }

            inline void setIW1(Eigen::MatrixXf val) {
                this->_iw1 = val;
            }

            inline void setIW2(Eigen::MatrixXf val) {
                this->_iw2 = val;
            }

            inline void setIW3(Eigen::MatrixXf val) {
                this->_iw3 = val;
            }

            inline void setIW4(Eigen::MatrixXf val) {
                this->_iw4 = val;
            }

            inline void setY1Ymin(int val) {
                this->_y1_ymin = val;
            }

            inline void setY1Gain(float val) {
                this->_y1_gain = val;
            }

            inline void setY1Offset(int val) {
                this->_y1_offset = val;
            }

            inline Eigen::MatrixXf mapminmax_apply(Eigen::MatrixXf t, Eigen::MatrixXf _x1_offset, Eigen::MatrixXf _x1_gain, int _x1_ymin) {
                Eigen::MatrixXf out;

                out = t - _x1_offset;
                out = out.array() * _x1_gain.array();
                out = out.array() + _x1_ymin;

                return out;
            }

            inline Eigen::MatrixXf tansig_apply(Eigen::MatrixXf t) {

                Eigen::MatrixXf denom = 1 + (-2 * t).array().exp();
                Eigen::MatrixXf hidden = (2 * denom.cwiseInverse()).array() - 1.0;
                return hidden;
            }


            /**
            * @ingroup core
            * @brief 3 hidden layers feedforward neural network implementation
            * @param Eigen::MatrixXf X dimensional input vector
            * @return Eigen::MatrixXf prediction values
            */
            inline Eigen::MatrixXf computeThreeNeuronsTanSig(Eigen::MatrixXf t){
                Eigen::MatrixXf xp1 = mapminmax_apply(t, this->_x1_offset, this->_x1_gain, this->_x1_ymin);

                Eigen::MatrixXf iw1_xp1 = this->_iw1 * xp1;

                Eigen::MatrixXf b_iw1_xp1 = this->_b1 + iw1_xp1;
                Eigen::MatrixXf hidden1 = tansig_apply(b_iw1_xp1); // Layer 1

                Eigen::MatrixXf b2_iw2_a1 = this->_b2 + (this->_iw2 * hidden1);
                Eigen::MatrixXf hidden2 = tansig_apply(b2_iw2_a1); //Layer 2

                Eigen::MatrixXf b3_iw3_a2 = this->_b3 + (this->_iw3 * hidden2);
                Eigen::MatrixXf hidden3 = tansig_apply(b3_iw3_a2); //Layer 3

                Eigen::MatrixXf out = this->_b4 + (this->_iw4 * hidden3).array(); //Layer 4

                return out;
            }

            /**
            * @ingroup core
            * @brief 3 hidden layers feedforward neural network implementation
            * @param covis::core::PTF 15-dimensional input vector
            * @return float prediction value
            */
            inline float getPrediction(covis::core::PTF ptf){
                Eigen::MatrixXf t(ptf.descriptorSize(), 1);
                t << ptf.d12, ptf.d23, ptf.d13, ptf.a1, ptf.a2, ptf.a3, ptf.n1np, ptf.n2np, ptf.n3np, ptf.n1d12, ptf.n2d23, ptf.n3d13, ptf.n1n2, ptf.n1n3, ptf.n2n3;

                float out = computeThreeNeuronsTanSig(t)(0);

                return out;
            }

            /**
            * @ingroup core
            * @brief 3 hidden layers feedforward neural network implementation
            * @param pcl::PointCloud<covis::core::PTF> poit cloud of PTFs
            * @return Eigen::MatrixXf with all predictions
            */
            inline Eigen::MatrixXf getPredictions(pcl::PointCloud<covis::core::PTF> ptf) {
                Eigen::MatrixXf tmp(ptf[0].descriptorSize(), ptf.size());

                for (size_t i = 0; i < ptf.size(); i++) {
                    tmp.col(i) << ptf[i].d12, ptf[i].d23, ptf[i].d13, ptf[i].a1, ptf[i].a2, ptf[i].a3, ptf[i].n1np, ptf[i].n2np, ptf[i].n3np, ptf[i].n1d12, ptf[i].n2d23, ptf[i].n3d13, ptf[i].n1n2, ptf[i].n1n3, ptf[i].n2n3;
                }
                Eigen::MatrixXf out = computeThreeNeuronsTanSig(tmp);
                std::cout << "out: " << out.cols() << " - " << out.rows() << std::endl;
                std::cout << "tmp: " << tmp.cols() << " - " << tmp.rows() << std::endl;
                return out;
            }

        private:
            Eigen::MatrixXf _x1_offset;
            Eigen::MatrixXf _x1_gain;
            int _x1_ymin;
            Eigen::MatrixXf _b1, _iw1, _iw2, _iw3, _b2, _b3, _iw4;
            float _b4;
            int _y1_ymin;
            float _y1_gain;
            int _y1_offset;

        };
    }
}

#endif