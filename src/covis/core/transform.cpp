#include "transform.h"

#include <pcl/common/io.h>

void covis::core::transform(pcl::PCLPointCloud2& input, const Eigen::Matrix4f& T) {
    pcl::PCLPointCloud2 output;
    if(pcl::getFieldIndex(input, "normal_x") >= 0) {
        pcl::PointCloud<pcl::PointNormal> tmp;
        pcl::fromPCLPointCloud2(input, tmp);
        transform(tmp, T);
        pcl::toPCLPointCloud2(tmp, output);
    } else {
        pcl::PointCloud<pcl::PointXYZ> tmp;
        pcl::fromPCLPointCloud2(input, tmp);
        transform(tmp, T);
        pcl::toPCLPointCloud2(tmp, output);
    }

    pcl::concatenateFields(input, output, input);
}

void covis::core::transform(const pcl::PCLPointCloud2& input,  pcl::PCLPointCloud2& output, const Eigen::Matrix4f& T) {
    if(&output != &input) // Copy
        output = input;
    transform(output, T);
}
