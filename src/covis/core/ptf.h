// Copyright (c) 2016, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_CORE_PTF_H
#define COVIS_CORE_PTF_H

//PCL
#include <pcl/point_types.h>
#include <pcl/register_point_struct.h>

namespace covis {
    namespace core {
        /**
        * @ingroup core
        * @struct PTF
        * @brief Structure for representing a PTF type
        *
        * PTF consist of 15 relations between three surface points:
        * d12, d23, d13 - Eucledian distance between two points
        * a1, a2, a3 - angles inside spanned triangle by three points
        * n1np, n2np, n3np - angle between point normal and triangle plane normal
        * n1d12, n2d23, n3d13 - angle between point normal and the distance vector
        * n1n2, n1n3, n2n3 - angle between two point normals
        *
        * @author Lilita Kiforenko
        *
        */
        struct PTF {
            float d12;
            float d23;
            float d13;
            float a1;
            float a2;
            float a3;
            float n1np;
            float n2np;
            float n3np;
            float n1d12;
            float n2d23;
            float n3d13;
            float n1n2;
            float n2n3;
            float n1n3;
            static int descriptorSize() { return sizeof(PTF)/sizeof(float) - 1; }
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        } EIGEN_ALIGN16;

        /**
        * @ingroup core
        * @struct PTF_Index
        * @brief Structure for representing a PTF_Index type
        *
        * PTF_Index contains three point's indexes
        *
        */

        struct PTF_Index {
            int idx1;
            int idx2;
            int idx3;

            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        } EIGEN_ALIGN16;
    }
}

POINT_CLOUD_REGISTER_POINT_STRUCT (covis::core::PTF,
                                   (float, d12, d12)
                                           (float, d23, d23)
                                           (float, d13, d13)
                                           (float, a1, a1)
                                           (float, a2, a2)
                                           (float, a3, a3)
                                           (float, n1np, n1np)
                                           (float, n2np, n2np)
                                           (float, n3np, n3np)
                                           (float, n1d12, n1d12)
                                           (float, n2d23, n2d23)
                                           (float, n3d13, n3d13)
                                           (float, n1n2, n1n2)
                                           (float, n2n3, n2n3)
                                           (float, n1n3, n1n3)
)
POINT_CLOUD_REGISTER_POINT_STRUCT (covis::core::PTF_Index, (int, idx1, idx1)(int, idx2, idx2)(int, idx3, idx3))

#endif /* COVIS_CORE_PTF_H */
