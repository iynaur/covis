// Copyright (c) 2016, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_PTF_EXTRACTION_IMPL_HPP
#define COVIS_FEATURE_PTF_EXTRACTION_IMPL_HPP

// Own
#include "ptf_extraction.h"
#include "../core/macros.h"

namespace covis {
    namespace feature {

        //*** Helper functions ****************************************************************************************
        template<typename T>
        bool is_infinite(const T &value) {
            T max_value = std::numeric_limits<T>::max();
            T min_value = -max_value;

            return !(min_value <= value && value <= max_value);
        }

        template<typename T>
        bool is_nan(const T &value) {
            // true if nan
            return value != value;
        }

        template<typename T>
        bool is_zero(const T &value) {
            // true if nan
            return value == 0;
        }

        template<typename T>
        bool is_valid(const T &value) {
            return !is_infinite(value) && !is_nan(value) && !is_zero(value);
        }


        template<typename T>
        float computeDistance(const T p1, const T p2) {
            return sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y) + (p1.z - p2.z) * (p1.z - p2.z));
        }

        template<typename T>
        inline bool checkIfValueIsWithinLimits(const T p1, const T p2, float mind, float maxd, float& d) {

            d = computeDistance<T>(p1, p2);
            if (d > maxd || d < mind) return false;
            return true;
        }

        template<typename T>
        inline bool checkIfValueIsWithinLimits(const T p1, const T p2, float mind, float maxd) {

            float d;
            return (checkIfValueIsWithinLimits(p1, p2, mind, maxd, d));
        }


        template<typename T>
        void computeNDts(T p1, T p2, T p3, Eigen::Vector3f _p1, Eigen::Vector3f _p2, Eigen::Vector3f _p3,
                         float &n1d12, float &n2d23, float &n3d13 ) {

            Eigen::Vector3f for_n1d = ((_p2 - _p1) + (_p3 - _p1)); for_n1d.normalize();
            Eigen::Vector3f for_n2d = ((_p1 - _p2) + (_p3 - _p2)); for_n2d.normalize();
            Eigen::Vector3f for_n3d = ((_p1 - _p3) + (_p2 - _p3)); for_n3d.normalize();

            n1d12 = (p1.getNormalVector3fMap().dot(for_n1d) + 1) / 2;
            n2d23 = (p2.getNormalVector3fMap().dot(for_n2d) + 1) / 2;
            n3d13 = (p3.getNormalVector3fMap().dot(for_n3d) + 1) / 2;

        }


        void computeAnglesInsideTriangle(Eigen::Vector3f _p1, Eigen::Vector3f _p2, Eigen::Vector3f _p3,
                                         float &a1, float &a2, float &a3) {
            Eigen::Vector3f _d21, _d31, _d32, _d23, _d13, _d12;

            _d21 = _p2 - _p1; _d21.normalize();
            _d32 = _p3 - _p2; _d32.normalize();
            _d31 = _p3 - _p1; _d31.normalize();
            _d23 = _p2 - _p3; _d23.normalize();
            _d13 = _p1 - _p3; _d13.normalize();
            _d12 = _p1 - _p2; _d12.normalize();

           a1 = (_d31.dot(_d21) + 1) / 2;
           a2 = (_d32.dot(_d12) + 1) / 2;
           a3 = (_d13.dot(_d23) + 1) / 2;

        }

        Eigen::Vector3f computePlaneNormal(Eigen::Vector3f _p1, Eigen::Vector3f _p2, Eigen::Vector3f _p3) {

            Eigen::Vector3f n1 = (_p2 - _p1);
            Eigen::Vector3f n2 = (_p3 - _p1);

            Eigen::Vector3f n = n1.cross(n2);
            n.normalize();
            return n;
        }

        template<typename T>
        bool computeAngleNormalAndPlaneNormal(T p1, T p2, T p3, Eigen::Vector3f _p1, Eigen::Vector3f _p2,
                                              Eigen::Vector3f _p3, float &n1np, float &n2np, float &n3np) {

            Eigen::Vector3f plane_normal = computePlaneNormal(_p1, _p2, _p3);
            if (!is_valid(plane_normal(0)) ) return false;
            n1np = (p1.getNormalVector3fMap().dot(plane_normal) + 1) / 2;
            n2np = (p2.getNormalVector3fMap().dot(plane_normal) + 1) / 2;
            n3np = (p3.getNormalVector3fMap().dot(plane_normal) + 1) / 2;
            return true;

        }

        void processValue(float &val, std::string name, float tol){
            COVIS_ASSERT_MSG(val > -tol && val< 1+tol, name << val << ", should be [0, 1]");
            if (val < 0) val = 0.0;
            else if (val > 1) val = 1.0;
        }

        void checkPTF(covis::core::PTF ptf){

            const float tol = 1e-5;
            processValue(ptf.d12, "ptf.d12: ", tol);
            processValue(ptf.d23, "ptf.d23: ", tol);
            processValue(ptf.d13, "ptf.d13: ", tol);
            processValue(ptf.a1, "ptf.a1: ", tol);
            processValue(ptf.a2, "ptf.a2: ", tol);
            processValue(ptf.a3, "ptf.a3: ", tol);
            processValue(ptf.n1d12, "ptf.n1d12: ", tol);
            processValue(ptf.n2d23, "ptf.n2d23: ", tol);
            processValue(ptf.n3d13, "ptf.n3d13: ", tol);
            processValue(ptf.n1n2, "ptf.n1n2: ", tol);
            processValue(ptf.n2n3, "ptf.n2n3: ", tol);
            processValue(ptf.n1n3, "ptf.n1n3: ", tol);
            processValue(ptf.n1np, "ptf.n1np: ", tol);
            processValue(ptf.n2np, "ptf.n2np: ", tol);
            processValue(ptf.n3np, "ptf.n3np: ", tol);

        }

        //*** COMPUTE PTF ***************************************************************
        template<typename T>
        inline bool computePTF(const T p1, const T p2, const T p3, float mind, float maxd, covis::core::PTF &ptf) {

            float s12, s13, s23;
            if (!checkIfValueIsWithinLimits<T>(p1, p2, mind, maxd, s12))
                return false;
            if (!checkIfValueIsWithinLimits<T>(p1, p3, mind, maxd, s13))
                return false;
            if (!checkIfValueIsWithinLimits<T>(p2, p3, mind, maxd, s23))
                return false;

            float multipl = 1.0 / ( maxd - mind);

            ptf.d12 = (s12 - mind) * multipl;
            ptf.d13 = (s13 - mind) * multipl;
            ptf.d23 = (s23 - mind) * multipl;

            Eigen::Vector3f _p1, _p2, _p3;

            _p1 << p1.x, p1.y, p1.z;
            _p2 << p2.x, p2.y, p2.z;
            _p3 << p3.x, p3.y, p3.z;

            computeAnglesInsideTriangle(_p1, _p2, _p3, ptf.a1, ptf.a2, ptf.a3);
            bool np = computeAngleNormalAndPlaneNormal(p1, p2, p3, _p1, _p2, _p3, ptf.n1np, ptf.n2np, ptf.n3np);
            if (!np) return false;
            computeNDts(p1, p2, p3, _p1, _p2, _p3, ptf.n1d12, ptf.n2d23, ptf.n3d13);

            ptf.n1n2 = (p1.getNormalVector3fMap().dot(p2.getNormalVector3fMap()) + 1) / 2;
            ptf.n2n3 = (p2.getNormalVector3fMap().dot(p3.getNormalVector3fMap()) + 1) / 2;
            ptf.n1n3 = (p1.getNormalVector3fMap().dot(p3.getNormalVector3fMap()) + 1) / 2;

            checkPTF(ptf);
            return true;
        }

        // Create all possible triplets withing min max and compute PTFs for them
        template<typename T>
        void computePTFs(typename pcl::PointCloud<T>::ConstPtr query, std::vector<std::vector<int> > ring_points,
                         float mind, float maxd,
                         pcl::PointCloud<covis::core::PTF> &ptf_out,
                         pcl::PointCloud<covis::core::PTF_Index> &ptf_out_indexes ) {

            // *** First loop ***
            for (size_t i = 0; i < ring_points.size(); i++) {
                T p1 = (*query)[i];

                if (!is_valid<float>(p1.x) || !is_valid<float>(p1.y) || !is_valid<float>(p1.z)) continue;
                if (!is_valid<float>(p1.normal_x) || !is_valid<float>(p1.normal_y) || !is_valid<float>(p1.normal_z)) continue;

                std::vector<int> all_neigh = ring_points[i];
                // *** Second loop ***
                for (size_t ii = 0; ii < all_neigh.size(); ii++) {
                    T p2 = (*query)[all_neigh[ii]];

                    if (!is_valid<float>(p2.x) || !is_valid<float>(p2.y) || !is_valid<float>(p2.z)) continue;
                    if (!is_valid<float>(p2.normal_x) || !is_valid<float>(p2.normal_y) || !is_valid<float>(p2.normal_z)) continue;

                    // *** Third loop ****
                    for (size_t iii = 0; iii < all_neigh.size(); iii++) {
                        if (ii == iii) continue;
                        T p3 = (*query)[all_neigh[iii]];

                        if (!is_valid<float>(p3.x) || !is_valid<float>(p3.y) || !is_valid<float>(p3.z)) continue;
                        if (!is_valid<float>(p3.normal_x) || !is_valid<float>(p3.normal_y) || !is_valid<float>(p3.normal_z)) continue;

                        covis::core::PTF ptf;
                        covis::core::PTF_Index ptf_idx;
                        ptf_idx.idx1 = i;
                        ptf_idx.idx2 = all_neigh[ii];
                        ptf_idx.idx3 = all_neigh[iii];

                        bool accept = computePTF(p1, p2, p3, mind, maxd, ptf);

                        if (accept) {
                            ptf_out.push_back(ptf);
                            ptf_out_indexes.push_back(ptf_idx);
                        }
                    } // end of third loop
                } // end of second loop
            } // end of first loop
        }
        //*************************************************************************************************************

        template<typename T>
        void findPointsOnRings(typename pcl::PointCloud<T>::ConstPtr cloud,
                               std::vector<std::vector<int> > &ring_points_out,
                               float max_distance, int number_of_rings) {


            float threshold = 0.001; // 1mm

            float border = max_distance / number_of_rings;

            size_t cloud_size = cloud->size();
            for (size_t i = 0; i < cloud_size; i += 1) {
                T p1 = (*cloud)[i];
                std::vector<int> neighbors;
                for (size_t ii = 0; ii < cloud_size - 1; ii += 1) {
                    if (i == ii) continue;
                    T p2 = (*cloud)[ii];

                    for (int r = 0; r < number_of_rings; r++) {
                        float value = border * (r + 1);
                        if (checkIfValueIsWithinLimits<T>(p1, p2, value - threshold, value + threshold)) {
                            neighbors.push_back(ii);
                            break;
                        }
                    } // end of second order loop
                } //end of point loop
                ring_points_out.push_back(neighbors);
            }
        }
        //**** COMPUTE ************************************************************************************************
        template<typename PointInT>
        inline void PTFExtraction<PointInT>::compute(typename pcl::PointCloud<PointInT>::ConstPtr cloud,
                               pcl::PointCloud<covis::core::PTF> &ptf_out,
                               pcl::PointCloud<covis::core::PTF_Index> &ptf_out_indexes, int number_of_rings) {

            // Check for valid inputs
            COVIS_ASSERT(cloud && !cloud->empty());

            std::vector<std::vector<int> > ring_points;
            findPointsOnRings<PointInT>(cloud, ring_points, this->_max_distance, number_of_rings);

            if (this->_debug) {
                COVIS_MSG_INFO ("Input cloud size: " << cloud->size() );
                COVIS_MSG_INFO ( "Min distance: " << this->_min_distance << ", max distance: " << this->_max_distance);
            }

            computePTFs<PointInT>(cloud, ring_points, this->_min_distance, this->_max_distance,
                                  ptf_out, ptf_out_indexes);

        } // end of compute
    }
}


#endif /* COVIS_FEATURE_PTF_EXTRACTION_IMPL_HPP */