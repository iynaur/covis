// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_PPF_EXTRACTION_H
#define COVIS_FEATURE_PPF_EXTRACTION_H

// Own
#include "feature_3d.h"
#include "eigen_feature.h"
#include "../core/macros.h"

// PCL
#include <pcl/point_types.h>

namespace covis {
    namespace feature {
        /**
         * @class PPFExtraction
         * @ingroup feature
         * @brief A semi-global set of PPF features
         *
         * @sa @ref PPFHistogram
         *
         * @tparam PointNT input point type, must contain XYZ+normal data
         * @author Anders Glent Buch
         */
        template<typename PointNT>
        class PPFExtraction : public Feature3D<PointNT, pcl::PPFSignature> {
            using Feature3D<PointNT,pcl::PPFSignature>::_surface;

            public:
                /// Empty constructor
                PPFExtraction() : _maxDistance(0), _complete(true) {}

                /**
                 * Constructor: set parameters
                 * @param maxDistance maximal sampling distance
                 * @param complete completeness flag, which specifies whether to compute all possible PPFs
                 */
                PPFExtraction(float maxDistance, bool complete) : _maxDistance(maxDistance), _complete(complete) {}

                /// Empty destructor
                virtual ~PPFExtraction() {}

                /**
                 * Compute PPFs
                 * @param cloud input point cloud, must contain XYZ and normal data
                 * @return PPFs
                 */
                pcl::PointCloud<pcl::PPFSignature>::Ptr compute(typename pcl::PointCloud<PointNT>::ConstPtr cloud);

                /**
                 * Get index mapping from PPFs --> point pairs
                 * @note This function only produces a valid result after a call to @ref compute()
                 * @return index
                 */
                inline const std::vector<std::pair<int,int> >& getIndex() const {
                    return _index;
                }

            private:
                /// Maximal sampling distance
                float _maxDistance;

                /// When set to true (which is the default), all possible PPFs are computed, otherwise only half
                bool _complete;

                /// Index mapping from each PPF to the point pairs in the input point cloud
                std::vector<std::pair<int,int> > _index;
        };

        /**
         * @ingroup feature
         * @brief Compute semi-global point pair features using @ref PPFExtraction
         * @param cloud input point cloud, must contain XYZ and normal data
         * @param maxDistance maximal sampling distance, set to zero to compute all possible PPFs
         * @param complete completeness flag, which specifies whether to compute all possible PPFs
         * @param index output index mapping from each PPF to point pair indices in the input cloud
         * @return PPFs
         */
        template<typename PointNT>
        inline pcl::PointCloud<pcl::PPFSignature>::Ptr
        computePPF(typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                   float maxDistance,
                   bool complete,
                   std::vector<std::pair<int,int> >& index) {
            PPFExtraction<PointNT> ppf(maxDistance, complete);
            pcl::PointCloud<pcl::PPFSignature>::Ptr result = ppf.compute(cloud);
            index = ppf.getIndex();

            return result;
        }

        /**
         * @ingroup feature
         * @brief Compute semi-global point pair features using @ref PPFExtraction for multiple point clouds
         * @param clouds input point clouds, must contain XYZ and normal data
         * @param maxDistance maximal sampling distance, set to zero to compute all possible PPFs
         * @param complete completeness flag, which specifies whether to compute all possible PPFs
         * @param index output index mapping from each PPF to GLOBAL point pair indices in the input clouds
         * (i.e. meaning to linear indices in one long list of points created by concatenating all clouds together)
         * @return PPFs
         */
        template<typename PointNT>
        inline pcl::PointCloud<pcl::PPFSignature>::Ptr
        computePPF(const std::vector<typename pcl::PointCloud<PointNT>::Ptr>& clouds,
                   float maxDistance,
                   bool complete,
                   std::vector<std::pair<int,int> >& index) {
            pcl::PointCloud<pcl::PPFSignature>::Ptr result(new pcl::PointCloud<pcl::PPFSignature>);
            index.clear();
            size_t offset = 0;
            for(size_t i = 0; i < clouds.size(); ++i) {
                std::vector<std::pair<int,int> > indexi;
                pcl::PointCloud<pcl::PPFSignature>::Ptr ppfi =
                        computePPF<PointNT>(clouds[i], maxDistance, complete, indexi);
                if(offset > 0) {
                    for(size_t j = 0; j < indexi.size(); ++j) {
                        indexi[j].first += offset;
                        indexi[j].second += offset;
                    }
                }

                *result += *ppfi;
                index.insert(index.end(), indexi.begin(), indexi.end());

                offset += clouds[i]->size();
            }

            return result;
        }

        /**
         * @ingroup feature
         * @brief Compute semi-global point pair features using @ref PPFExtraction
         *
         * @sa @ref detect::computeKnnMatchesPPF() for how to match these features properly
         *
         * @param cloud input point cloud, must contain XYZ and normal data
         * @param maxDistance maximal sampling distance, set to zero to compute all possible PPFs
         * @param complete completeness flag, which specifies whether to compute all possible PPFs
         * @param index output index mapping from each PPF to point pair indices in the input cloud
         * @return PPFs as columns in an Eigen matrix
         */
        template<typename PointNT>
        inline feature::MatrixT
        computePPFEigen(typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                        float maxDistance,
                        bool complete,
                        std::vector<std::pair<int,int> >& index) {
            pcl::PointCloud<pcl::PPFSignature>::Ptr ppf =
                    computePPF<PointNT>(cloud, maxDistance, complete, index);
            feature::MatrixT result =
                    Eigen::Map<feature::MatrixT, 0, Eigen::OuterStride<5> >(&ppf->points[0].f1, 4, ppf->size());

            return result;
        }

        /**
         * @ingroup feature
         * @brief Compute semi-global point pair features using @ref PPFExtraction for multiple point clouds
         *
         * @sa @ref detect::computeKnnMatchesPPF() for how to match these features properly
         *
         * @param clouds input point clouds, must contain XYZ and normal data
         * @param maxDistance maximal sampling distance, set to zero to compute all possible PPFs
         * @param complete completeness flag, which specifies whether to compute all possible PPFs
         * @param index output index mapping from each PPF to GLOBAL point pair indices in the input clouds
         * (i.e. meaning to linear indices in one long list of points created by concatenating all clouds together)
         * @return PPFs as columns in an Eigen matrix
         */
        template<typename PointNT>
        inline feature::MatrixT
        computePPFEigen(const std::vector<typename pcl::PointCloud<PointNT>::Ptr>& clouds,
                        float maxDistance,
                        bool complete,
                        std::vector<std::pair<int,int> >& index) {
            feature::MatrixT result;
            index.clear();
            size_t offset = 0;
            for(size_t i = 0; i < clouds.size(); ++i) {
                std::vector<std::pair<int,int> > indexi;
                feature::MatrixT ppfi = computePPFEigen<PointNT>(clouds[i], maxDistance, complete, indexi);
                if(offset > 0) {
                    for(size_t j = 0; j < indexi.size(); ++j) {
                        indexi[j].first += offset;
                        indexi[j].second += offset;
                    }
                }

                feature::MatrixT tmp(4, result.cols() + ppfi.cols());
                tmp << result, ppfi;
                result = tmp;
                index.insert(index.end(), indexi.begin(), indexi.end());

                offset += clouds[i]->size();
            }

            return result;
        }
    }
}

#ifndef COVIS_PRECOMPILE
#include "ppf_extraction_impl.hpp"
#endif

#endif
