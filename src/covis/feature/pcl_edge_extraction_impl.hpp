// Copyright (c) 2014, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_PCL_EDGE_EXTRACTION_IMPL_HPP
#define COVIS_FEATURE_PCL_EDGE_EXTRACTION_IMPL_HPP

// Own
#include "pcl_edge_extraction.h"
#include "covis/feature/edge_gradient.h"
#include "../core/scoped_timer.h"

// PCL
#include <pcl/kdtree/impl/kdtree_flann.hpp>
#include <pcl/search/impl/search.hpp>
#include <pcl/search/impl/kdtree.hpp>
#include <pcl/search/impl/organized.hpp>
#include <pcl/features/normal_3d_omp.h>

#include <pcl/io/pcd_io.h>

namespace covis {
    namespace feature {

        template<typename PointT>
        int PCLEdgeExtraction<PointT>::extractPCLEdges(const typename pcl::PointCloud<PointT>::Ptr &cloud,
                std::vector<pcl::PointIndices> & label_indices, pcl::PointCloud<core::CategorisedEdge>::Ptr &out,
                pcl::PointCloud<core::CategorisedEdge>::Ptr &only_edge,  pcl::PointIndices::Ptr &edge_indices) {
            pcl::PointCloud<pcl::Normal>::Ptr normal (new pcl::PointCloud<pcl::Normal>);
            float radiusNormals = 0.01;
            {

                covis::core::ScopedTimer t("Organized PointCloud Normal Estimation");
                pcl::NormalEstimationOMP<PointT, pcl::Normal> ne;
                ne.setInputCloud(cloud);
                typename pcl::search::OrganizedNeighbor<PointT>::Ptr tree(new pcl::search::OrganizedNeighbor<PointT>());
                tree->setInputCloud(cloud);
                ne.setSearchMethod (tree);
                ne.setRadiusSearch (radiusNormals);
                ne.compute (*normal);
            }

            //-------------------------------------------------------------------------------
            out.reset(new pcl::PointCloud<core::CategorisedEdge>());
            out->height = cloud->height;
            out->width = cloud->width;
            out->points.resize(out->height * out->width);

            for (size_t r = 0; r < out->height; r++)
                for (size_t c = 0; c < out->width; c++){
                    (*out)(c,r).x = (*cloud)(c,r).x;
                    (*out)(c,r).y = (*cloud)(c,r).y;
                    (*out)(c,r).z = (*cloud)(c,r).z;

                    (*out)(c,r).r = (*cloud)(c,r).r;
                    (*out)(c,r).g = (*cloud)(c,r).g;
                    (*out)(c,r).b = (*cloud)(c,r).b;

                    (*out)(c,r).normal_x = (*normal)(c,r).normal_x;
                    (*out)(c,r).normal_y = (*normal)(c,r).normal_y;
                    (*out)(c,r).normal_z = (*normal)(c,r).normal_z;
                }
            {
                covis::core::ScopedTimer t("Edge gradient");

                //extract pcl edges
                covis::feature::EdgeGradient<PointT, pcl::Normal, pcl::Label> oed;
                oed.setInputNormals (normal);
                oed.setInputCloud (cloud);
                oed.setDepthDisconThreshold (this->_th_dd);
                oed.setMaxSearchNeighbors (_max_search);
                oed.setRGBCannyLowThreshold(_cannyLow);
                oed.setRGBCannyHighThreshold(_cannyHigh);
                oed.setEdgeType (_edgeType);
                oed.setItIsTemplate(this->_it_is_template);
                pcl::PointCloud<pcl::Label> labels;

                oed.myCompute(labels, label_indices, out);
            }
            //-----------------------------------------------------------------------------------------------
            {
                covis::core::ScopedTimer t("loop index labels");
                int canny = 0, occluding = 0, occluded = 0, highcurvature = 0;
                int index_in_edge = 0;
                //set everything to [-1, -1, -1, -1] and idx in original point cloud
                for (size_t i = 0; i < out->size(); i++) {
                    (*out)[i].idx_edge_types[0] =
                            (*out)[i].idx_edge_types[1] =
                                    (*out)[i].idx_edge_types[2] =
                                            (*out)[i].idx_edge_types[3] = -1;
                    (*out)[i].idx_original = i;
                }
                for (size_t e1 = 0; e1 < label_indices[1].indices.size(); e1++) { //occluding
                    core::CategorisedEdge& point = (*out)[label_indices[1].indices[e1]];
                    if (pcl_isfinite(point.x) && pcl_isfinite(point.y) && pcl_isfinite(point.z)){
                        point.idx_edge_types[0] = e1;
                        occluding++;
                    }
                }
                pcl::console::print_error("Occluding size: %d\n", occluding);
                for (size_t e1 = 0; e1 < label_indices[2].indices.size(); e1++) {  //occluded
                    core::CategorisedEdge& point = (*out)[label_indices[2].indices[e1]];
                    if (pcl_isfinite(point.x) && pcl_isfinite(point.y) && pcl_isfinite(point.z)){
                        point.idx_edge_types[1] = e1;
                        occluded++;
                    }
                }
                pcl::console::print_error("occluded size: %d\n", label_indices[2].indices.size());

                for (size_t e1 = 0; e1 < label_indices[3].indices.size(); e1++) {  //curvature
                    core::CategorisedEdge& point = (*out)[label_indices[3].indices[e1]];
                    if (pcl_isfinite(point.x) && pcl_isfinite(point.y) && pcl_isfinite(point.z)){
                        point.idx_edge_types[2] = e1;
                        highcurvature++;
                    }
                }

                pcl::console::print_error("curvature size: %d\n", label_indices[3].indices.size());

                for (size_t e1 = 0; e1 < label_indices[4].indices.size(); e1++) { //canny
                    core::CategorisedEdge& point = (*out)[label_indices[4].indices[e1]];
                    if (pcl_isfinite(point.x) && pcl_isfinite(point.y) && pcl_isfinite(point.z)){
                        point.idx_edge_types[3] = e1;
                        canny++;
                    }
                }
                //*****************************************************************************
                //remove background canny edges
                typename pcl::search::Search<covis::core::CategorisedEdge>::Ptr s;
                s.reset(new pcl::search::KdTree<covis::core::CategorisedEdge>);
                s->setInputCloud(out);

                std::vector<int> removed_idx;
                for (size_t p = 0; p < out->size(); p++){
                    covis::core::CategorisedEdge& point = (*out)[p];
                    if (point.idx_edge_types[3] == -1) continue;
                    std::vector<int> idx;
                    std::vector<float> distsq;
                    s->radiusSearch(point, 0.005, idx, distsq);

                    for (size_t i = 0; i < idx.size(); i++) {
                        covis::core::CategorisedEdge neigh = (*out)[idx[i]];
                        if (neigh.idx_edge_types[1] != -1) {
                            point.idx_edge_types[3] = -1;
                            removed_idx.push_back(p);
                            break;
                        }
                    }
                }
                pcl::console::print_error("canny original size: %d, after thresholding: %d\n", canny, canny - removed_idx.size());
                //compute size of edge points and keep them in separate point cloud
                for (size_t i = 0; i < out->size(); i++) {
                    if ((*out)[i].idx_edge_types[0] != -1 || (*out)[i].idx_edge_types[1] != -1 ||
                            (*out)[i].idx_edge_types[2] != -1 || (*out)[i].idx_edge_types[3] != -1 )
                    {
                        edge_indices->indices.push_back(i);
                        (*out)[i].idx_edge = index_in_edge;
                        index_in_edge++;
                    }
                }

            }

            only_edge.reset((new pcl::PointCloud<covis::core::CategorisedEdge>));
            pcl::copyPointCloud (*out, edge_indices->indices, *only_edge);

            return only_edge->size();
        }

    }
}

#endif
