// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_PPF_EXTRACTION_IMPL_HPP
#define COVIS_FEATURE_PPF_EXTRACTION_IMPL_HPP

#include "ppf_extraction.h"

namespace covis {
    namespace feature {
        template<typename PointNT>
        pcl::PointCloud<pcl::PPFSignature>::Ptr
        PPFExtraction<PointNT>::compute(typename pcl::PointCloud<PointNT>::ConstPtr cloud) {
            // Check for valid inputs
            COVIS_ASSERT(cloud && !cloud->empty());

            if(_surface && _surface != cloud && !_surface->empty())
                COVIS_MSG_WARN("Surface is specified, but PPF computation will ignore it!");

            float maxDistanceMeasured = 0;

            pcl::PointCloud<pcl::PPFSignature>::Ptr result(new pcl::PointCloud <pcl::PPFSignature>);
            _index.clear();
            for(int i = 0; i < int(cloud->size() - 1); ++i) {
                const Eigen::Vector3f &p1 = cloud->points[i].getVector3fMap();
                const Eigen::Vector3f &n1 = cloud->points[i].getNormalVector3fMap();
                for(int j = i + 1; j < int(cloud->size()); ++j) {
                    const Eigen::Vector3f &p2 = cloud->points[j].getVector3fMap();
                    const Eigen::Vector3f &n2 = cloud->points[j].getNormalVector3fMap();

                    const Eigen::Vector3f r = (p2 - p1);
                    const float dsq = r.squaredNorm();

                    if(dsq < 1e-3f)
                        continue;

                    const float d = sqrtf(dsq);

                    // If max distance is unspecified, compute it ourselves
                    if(_maxDistance <= 0 && d > maxDistanceMeasured)
                        maxDistanceMeasured = d;

                    if(_maxDistance <= 0 || d <= _maxDistance) {
                        pcl::PPFSignature ppf;
                        ppf.f1 = (_maxDistance > 0 ? d / _maxDistance : d);
                        const Eigen::Vector3f rnormalized = r / d;
                        const float n1dotr = n1.dot(rnormalized);
                        const float n2dotr = n2.dot(rnormalized);
                        const float n1dotn2 = n2.dot(n2);
//                        if(1) { // Angle cosines
//                            ppf.f2 = (n1dotr + 1) / 2;
//                            ppf.f3 = (n2dotr + 1) / 2;
//                            ppf.f4 = (n1dotn2 + 1) / 2;
//                        } else {
                            // Angles
                            ppf.f2 = (n1dotr <= -1 ? M_PI : (n1dotr >= 1 ? 0 : acosf(n1dotr) / M_PI));
                            ppf.f3 = (n2dotr <= -1 ? M_PI : (n2dotr >= 1 ? 0 : acosf(n2dotr) / M_PI));
                            ppf.f4 = (n1dotn2 <= -1 ? M_PI : (n1dotn2 >= 1 ? 0 : acosf(n1dotn2) / M_PI));
//                        }

                        _index.push_back(std::make_pair(i, j));
                        result->push_back(ppf);

                        if(_complete) {
                        _index.push_back(std::make_pair(j, i));
                        std::swap(ppf.f2, ppf.f3);
                        result->push_back(ppf);
                    }
                }
            }
            }

            // Normalize by computed max distance if unspecified
            if(maxDistanceMeasured > 0)
                for(size_t i = 0; i < result->size(); ++i)
                    result->points[i].f1 /= maxDistanceMeasured;

            return result;
        }
    }
}

#endif
