// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_PCL_FEATURE_H
#define COVIS_FEATURE_PCL_FEATURE_H

// Own
#include "feature_base.h"
#include "../core/macros.h"

// PCL
#include <pcl/point_types.h>
#include <pcl/features/fpfh_omp.h>
#include <pcl/features/shot_omp.h>
#include <pcl/features/spin_image.h>
#include <pcl/features/usc.h>

// Register the SI for convenience
POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::Histogram<153>, (float[153], histogram, histogram))

namespace covis {
    namespace feature {
        /// FPFH feature type
        typedef pcl::FPFHSignature33 FPFHT;

        /// SHOT feature type
        typedef pcl::SHOT352 SHOTT;

        /// SHOT color feature type
        typedef pcl::SHOT1344 SHOTColorT;

        /// Spin Image feature type
        typedef pcl::Histogram<153> SpinImageT;

        /// USC feature type
        typedef pcl::UniqueShapeContext1960 USCT;

        /// FPFH point cloud
        typedef pcl::PointCloud<FPFHT> FPFHCloudT;

        /// SHOT point cloud
        typedef pcl::PointCloud<SHOTT> SHOTCloudT;

        /// SHOT color point cloud
        typedef pcl::PointCloud<SHOTColorT> SHOTColorCloudT;

        /// Spin Image point cloud
        typedef pcl::PointCloud<SpinImageT> SpinImageCloudT;

        /// Shape Context point cloud
        typedef pcl::PointCloud<USCT> USCCloudT;

        /**
         * @class PCLFeature
         * @ingroup feature
         * @brief Convenience wrapper around a variety of PCL features
         * 
         * Use this class by setting a feature radius and optionally an underlying surface and finally call one of the
         * <b>compute*</b> functions.
         *
         * For the return types (templated PCL point cloud objects), we have also defined convenience typedefs outside
         * this class, such as @ref SHOTCloudT.
         *
         * @sa @ref ECSAD, @ref DistanceNormalHistogram
         * @tparam PointNT input point type, must contain XYZ data and for some features also normal fields
         * @author Anders Glent Buch
         */
        template<typename PointT>
        class PCLFeature : public FeatureBase {
            public:
                /// Empty constructor
                PCLFeature() : _radius(-1.0f) {}

                /**
                 * Constructor: initialize with a search surface
                 * @param surface surface
                 */
                PCLFeature(typename pcl::PointCloud<PointT>::ConstPtr surface) : _surface(surface), _radius(-1.0f) {}

                /**
                 * Constructor: initialize with a search radius
                 * @param radius radius
                 */
                PCLFeature(float radius) : _radius(radius) {}

                /**
                 * Constructor: initialize with a search surface and radius
                 * @param surface surface
                 * @param radius radius
                 */
                PCLFeature(typename pcl::PointCloud<PointT>::ConstPtr surface, float radius) :
                        _surface(surface), _radius(radius) {}

                /// Empty destructor
                virtual ~PCLFeature() {}

                /**
                 * Set an external surface for computing features
                 * @param surface external surface
                 */
                inline void setSurface(typename pcl::PointCloud<PointT>::ConstPtr surface) {
                    _surface = surface;
                }

                /**
                 * Set search radius
                 * @param radius search radius
                 */
                inline void setRadius(float radius) {
                    _radius = radius;
                }
                
                /**
                 * Compute Fast Point Feature Histogram descriptors
                 * @param cloud input point cloud, must contain XYZ data and normal fields
                 * @return FPFH features, which are 33-dimensional histograms
                 */
                inline FPFHCloudT::Ptr computeFPFH(typename pcl::PointCloud<PointT>::ConstPtr cloud) {
                    COVIS_ASSERT(_radius >= 0);
                    
                    pcl::FPFHEstimationOMP<PointT,PointT,pcl::FPFHSignature33> est;
                    est.setRadiusSearch(_radius);
                    if(_surface) {
                        est.setSearchSurface(_surface);
                        est.setInputNormals(_surface);
                    } else {
                        est.setInputNormals(cloud);
                    }
                    est.setInputCloud(cloud);
                    
                    FPFHCloudT::Ptr features(new FPFHCloudT);
                    est.compute(*features);
                    
                    return features;
                }
                
                /**
                 * Compute Signature Histogram of Orientations descriptors
                 * @param cloud input point cloud, must contain XYZ data and normal fields
                 * @return SHOT features, which are 352-dimensional histograms plus a reference frame
                 */
                inline SHOTCloudT::Ptr computeSHOT(typename pcl::PointCloud<PointT>::ConstPtr cloud) {
                    COVIS_ASSERT(_radius >= 0);

                    pcl::SHOTEstimationOMP<PointT,PointT,SHOTT> est;
                    est.setRadiusSearch(double(_radius));
                    if(_surface) {
                        est.setSearchSurface(_surface);
                        est.setInputNormals(_surface);
                    } else {
                        est.setInputNormals(cloud);
                    }
                    est.setInputCloud(cloud);
                    
                    SHOTCloudT::Ptr features(new SHOTCloudT);
                    est.compute(*features);
                    
                    return features;
                }

                /**
                 * Compute Signature Histogram of Orientations descriptors
                 * @param cloud input point cloud, must contain XYZ+RGB data and normal fields
                 * @return SHOT color features, which are 1344-dimensional histograms plus a reference frame
                 */
                inline SHOTColorCloudT::Ptr computeSHOTColor(typename pcl::PointCloud<PointT>::ConstPtr cloud) {
                    COVIS_ASSERT(_radius >= 0);

                    pcl::SHOTColorEstimationOMP<PointT,PointT,SHOTColorT> est;
                    est.setRadiusSearch(_radius);
                    if(_surface) {
                        est.setSearchSurface(_surface);
                        est.setInputNormals(_surface);
                    } else {
                        est.setInputNormals(cloud);
                    }
                    est.setInputCloud(cloud);

                    SHOTColorCloudT::Ptr features(new SHOTColorCloudT);
                    est.compute(*features);

                    return features;
                }
                
                /**
                 * Compute Spin Image descriptors
                 * @param cloud input point cloud, must contain XYZ data and normal fields
                 * @return SI features, which are 153-dimensional histograms
                 */
                inline SpinImageCloudT::Ptr computeSI(typename pcl::PointCloud<PointT>::ConstPtr cloud) {
                    COVIS_ASSERT(_radius >= 0);
                    
                    pcl::SpinImageEstimation<PointT,PointT,SpinImageT > est;
                    est.setImageWidth(8);
                    est.setRadiusSearch(_radius);
                    if(_surface)
                        est.setSearchSurface(_surface);
                    est.setInputNormals(cloud); // For Spin images only we set this to the normals only at the feature points
                    est.setInputCloud(cloud);
                    
                    SpinImageCloudT::Ptr features(new SpinImageCloudT);
                    est.compute(*features);
                    
                    return features;
                }
                
                /**
                 * Compute Unique Shape Context descriptors
                 * @param cloud input point cloud, must contain XYZ data and normal fields
                 * @param minimalRadius minimal radius, if set to 0,
                 * use 1/10 of the search radius as in the original work
                 * @param localRadius local radius, if set to 0,
                 * use the search radius as in the original work
                 * @param pointDensityRadius point density estimation radius, if set to 0,
                 * use twice the mesh resolution as in the original work
                 * @return USC features, which are 1980-dimensional histograms plus a reference frame
                 */
                inline USCCloudT::Ptr computeUSC(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                    double minimalRadius = 0, double localRadius = 0, double pointDensityRadius = 0) {
                    COVIS_ASSERT(_radius >= 0);
                    
                    pcl::UniqueShapeContext<PointT,pcl::UniqueShapeContext1960> est;
                    est.setRadiusSearch(_radius); // 20*mr in paper
                    est.setMinimalRadius(minimalRadius <= 0 ? 0.1 * _radius : minimalRadius); // 0.1*max_radius in paper
                    est.setLocalRadius(localRadius <= 0 ? _radius : localRadius); // RF radius, 20*mr in paper
                    est.setPointDensityRadius(pointDensityRadius <= 0 ? 0.1 * _radius : pointDensityRadius); // 2*mr in paper
                    if(_surface)
                        est.setSearchSurface(_surface);
                    est.setInputCloud(cloud);

                    USCCloudT::Ptr features(new USCCloudT);
                    est.compute(*features);
                    
                    return features;
                }

            private:
                /// External surface
                typename pcl::PointCloud<PointT>::ConstPtr _surface;
                
                /// Feature radius
                float _radius;
        };

        /**
         * @ingroup feature
         * Compute FPFH features for a point cloud
         * @param cloud input point cloud
         * @param radius search radius
         * @return FPFH histogram features
         */
        template<typename PointNT>
        inline FPFHCloudT::Ptr computeFPFH(
                typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                float radius) {
            PCLFeature<PointNT> pclf;
            pclf.setRadius(radius);

            return pclf.computeFPFH(cloud);
        }

        /**
         * @ingroup feature
         * Compute FPFH features for a point cloud with an external search surface
         * @param cloud input point cloud
         * @param surface search surface
         * @param radius search radius
         * @return FPFH histogram features
         */
        template<typename PointNT>
        inline FPFHCloudT::Ptr computeFPFH(
                typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                typename pcl::PointCloud<PointNT>::ConstPtr surface,
                float radius) {
            PCLFeature<PointNT> pclf;
            pclf.setSurface(surface);
            pclf.setRadius(radius);

            return pclf.computeFPFH(cloud);
        }

        /**
         * @ingroup feature
         * Compute SHOT features for a point cloud
         * @param cloud input point cloud
         * @param radius search radius
         * @return SHOT histogram features
         */
        template<typename PointNT>
        inline SHOTCloudT::Ptr computeSHOT(
                typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                float radius) {
            PCLFeature<PointNT> pclf;
            pclf.setRadius(radius);

            return pclf.computeSHOT(cloud);
        }

        /**
         * @ingroup feature
         * Compute SHOT features for a point cloud with an external search surface
         * @param cloud input point cloud
         * @param surface search surface
         * @param radius search radius
         * @return SHOT histogram features
         */
        template<typename PointNT>
        inline SHOTCloudT::Ptr computeSHOT(
                typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                typename pcl::PointCloud<PointNT>::ConstPtr surface,
                float radius) {
            PCLFeature<PointNT> pclf;
            pclf.setSurface(surface);
            pclf.setRadius(radius);

            return pclf.computeSHOT(cloud);
        }

        /**
         * @ingroup feature
         * Compute SHOT color features for a point cloud
         * @param cloud input point cloud
         * @param radius search radius
         * @return SHOT color histogram features
         */
        template<typename PointNT>
        inline SHOTColorCloudT::Ptr computeSHOTColor(
                typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                float radius) {
            PCLFeature<PointNT> pclf;
            pclf.setRadius(radius);

            return pclf.computeSHOTColor(cloud);
        }

        /**
         * @ingroup feature
         * Compute SHOT collor features for a point cloud with an external search surface
         * @param cloud input point cloud
         * @param surface search surface
         * @param radius search radius
         * @return SHOT color histogram features
         */
        template<typename PointNT>
        inline SHOTColorCloudT::Ptr computeSHOTColor(
                typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                typename pcl::PointCloud<PointNT>::ConstPtr surface,
                float radius) {
            PCLFeature<PointNT> pclf;
            pclf.setSurface(surface);
            pclf.setRadius(radius);

            return pclf.computeSHOTColor(cloud);
        }

        /**
         * @ingroup feature
         * Compute Spin Image features for a point cloud
         * @param cloud input point cloud
         * @param radius search radius
         * @return Spin Image histogram features
         */
        template<typename PointNT>
        inline SpinImageCloudT::Ptr computeSI(
                typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                float radius) {
            PCLFeature<PointNT> pclf;
            pclf.setRadius(radius);

            return pclf.computeSI(cloud);
        }

        /**
         * @ingroup feature
         * Compute Spin Image features for a point cloud with an external search surface
         * @param cloud input point cloud
         * @param surface search surface
         * @param radius search radius
         * @return Spin Image histogram features
         */
        template<typename PointNT>
        inline SpinImageCloudT::Ptr computeSI(
                typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                typename pcl::PointCloud<PointNT>::ConstPtr surface,
                float radius) {
            PCLFeature<PointNT> pclf;
            pclf.setSurface(surface);
            pclf.setRadius(radius);

            return pclf.computeSI(cloud);
        }

        /**
         * @ingroup feature
         * Compute USC features for a point cloud
         * @param cloud input point cloud
         * @param radius search radius
         * @param minimalRadius minimal radius, if set to 0,
         * use 1/10 of the search radius as in the original work
         * @param localRadius local radius, if set to 0,
         * use the search radius as in the original work
         * @param pointDensityRadius point density estimation radius, if set to 0,
         * use twice the mesh resolution as in the original work
         * @return USC histogram features
         */
        template<typename PointNT>
        inline USCCloudT::Ptr computeUSC(
                typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                float radius,
                double minimalRadius = 0,
                double localRadius = 0,
                double pointDensityRadius = 0) {
            PCLFeature<PointNT> pclf;
            pclf.setRadius(radius);

            return pclf.computeUSC(cloud, minimalRadius, localRadius, pointDensityRadius);
        }

        /**
         * @ingroup feature
         * Compute USC features for a point cloud with an external search surface
         * @param cloud input point cloud
         * @param surface search surface
         * @param radius search radius
         * @param minimalRadius minimal radius, if set to 0,
         * use 1/10 of the search radius as in the original work
         * @param localRadius local radius, if set to 0,
         * use the search radius as in the original work
         * @param pointDensityRadius point density estimation radius, if set to 0,
         * use twice the mesh resolution as in the original work
         * @return USC histogram features
         */
        template<typename PointNT>
        inline USCCloudT::Ptr computeUSC(
                typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                typename pcl::PointCloud<PointNT>::ConstPtr surface,
                float radius,
                double minimalRadius = 0,
                double localRadius = 0,
                double pointDensityRadius = 0) {
            PCLFeature<PointNT> pclf;
            pclf.setSurface(surface);
            pclf.setRadius(radius);

            return pclf.computeUSC(cloud, minimalRadius, localRadius, pointDensityRadius);
        }
    }
}

#endif
