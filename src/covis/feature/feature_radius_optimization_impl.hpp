// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_FEATURE_RADIUS_OPTIMIZATION_IMPL_HPP
#define COVIS_FEATURE_FEATURE_RADIUS_OPTIMIZATION_IMPL_HPP

// Own
#include "feature_radius_optimization.h"
#include "eigen_feature.h"
#include "../core/random.h"
#include "../core/stat.h"
#include "../detect/point_search.h"

namespace covis {
    namespace feature {
        template<typename PointNT>
        float FeatureRadiusOptimization<PointNT>::compute() {
            COVIS_ASSERT(_cloud && !_cloud->empty());
            COVIS_ASSERT(!_featureName.empty());
            COVIS_ASSERT(_featurePoints > 0);

            if(_radiusStart <= 0 && _radiusInc <= 0 && _radiusStop <= 0) {
                const float diag = detect::computeDiagonal<PointNT>(_cloud);
                _radiusStart = 0.1 * diag;
                _radiusInc = 0.05 * diag;
                _radiusStop = 0.5 * diag;
            }

            COVIS_ASSERT(_radiusStart > 0 && _radiusStart <= _radiusStop);
            COVIS_ASSERT(_radiusInc > 0);
            COVIS_ASSERT(_pcaVariation > 0 && _pcaVariation < 1);

            const size_t numPoints = _cloud->size();
            const std::vector<size_t> idx = core::randidx<size_t>(numPoints, MIN(numPoints, _featurePoints));

            typename CloudT::Ptr keypoints(new CloudT);
            *keypoints = core::extract(*_cloud, idx);

            if(_verbose)
                COVIS_MSG("Performing feature radius optimization using " << _featurePoints <<" feature points and a PCA variation of " << _pcaVariation << "...");

            float maxval = 0;
            float optrad = 0;
            for(float rad = _radiusStart; rad <= _radiusStop + 1e-5f; rad += _radiusInc) {
                const MatrixT f = computeFeature<PointNT>(_featureName, keypoints, _cloud, rad);
                core::PCA pca = core::pcaTrain(f, _pcaVariation);
                const float metric = pca.second.cols();
                if(metric > maxval) {
                    maxval = metric;
                    optrad = rad;
                }

                if(_verbose)
                    COVIS_MSG("\tRadius: " << rad << ", PCA columns: " << metric);
            }

            if(_verbose)
                COVIS_MSG("Optimum achieved with radius: " << optrad);

            return optrad;
        }
    }
}

#endif
