// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_DISTANCE_COLOR_NORMAL_HISTOGRAM_IMPL_HPP
#define COVIS_FEATURE_DISTANCE_COLOR_NORMAL_HISTOGRAM_IMPL_HPP

// Own
#include "../core/stat.h"

// PCL
#include <pcl/search/kdtree.h>
#include <pcl/search/organized.h>

namespace covis {
    namespace feature {
        template<typename PointT, int NDist, int NNorm, int NColor>
        typename pcl::PointCloud<typename DistanceColorNormalHistogram<PointT, NDist, NNorm, NColor>::Histogram>::Ptr
        DistanceColorNormalHistogram<PointT, NDist, NNorm, NColor>::compute(typename pcl::PointCloud<PointT>::ConstPtr cloud) {
            // Check for valid inputs
            COVIS_ASSERT(cloud && !cloud->empty());
            COVIS_ASSERT_MSG(_radius > 0.0f, "Search radius not set!");
            COVIS_ASSERT(NDist > 0);
            COVIS_ASSERT(NNorm > 0 || NColor > 0);
            
            // Create search
            typename pcl::search::Search<PointT>::Ptr s;
            if(cloud->isOrganized() || (_surface && _surface->isOrganized()))
                s.reset(new pcl::search::OrganizedNeighbor<PointT>);
            else
                s.reset(new pcl::search::KdTree<PointT>(false));
            
            // Index input cloud or external surface
            if(_surface)
                s->setInputCloud(_surface);
            else
                s->setInputCloud(cloud);
            
            // Allocate results
            typename pcl::PointCloud<Histogram>::Ptr result(
                    new typename pcl::PointCloud<Histogram>(cloud->width, cloud->height));
            
            /*
             * Main loop over all input points
             */
            for(size_t i = 0; i < cloud->size(); ++i) {
                // Take current source point
                const PointT& pi = cloud->points[i];
                
                // Skip if source is non-finite in XYZ+RGB or normal
                if(!pcl::isFinite(pi) ||
                        !pcl_isfinite(pi.normal_x) || !pcl_isfinite(pi.normal_y) || !pcl_isfinite(pi.normal_z) ||
                        !pcl_isfinite(pi.r) || !pcl_isfinite(pi.g) || !pcl_isfinite(pi.b)) {
                    for(int j = 0; j < Histogram::descriptorSize(); ++j)
                        result->points[i].histogram[j] = 0.0f;
                    continue;
                }

                // Take current output histogram
                float hdot[NDist * NNorm];
//                float hcolor[NDist * NColor];
                float hred[NDist * NColor];
                float hgreen[NDist * NColor];
                float hblue[NDist * NColor];
                
                // Find neighbors
                std::vector<int> idx;
                std::vector<float> distsq;
                s->radiusSearch(pi, double(_radius), idx, distsq);
                
                // Allocate relative distances/normal orientations/colors
                std::vector<float> distances;
                std::vector<float> dots;
                std::vector<float> reds, greens, blues;
//                std::vector<float> colors;
//                colors.reserve(idx.size());

                // Loop over neighbors and compute relative distances, dot products and colors
                for(size_t j = 0; j < idx.size(); ++j) {
                    // Take neighbor
                    const PointT& pj = (_surface ? _surface->points[idx[j]] : cloud->points[idx[j]]);
                    
                    // Skip neighbor if it is the source point
                    if(&pj == &pi)
                        continue;
                    
//                    // Skip neighbor if it does not have a valid normal or color
//                    if(!pcl_isfinite(pj.normal_x) || !pcl_isfinite(pj.normal_y) || !pcl_isfinite(pj.normal_z) ||
//                            !pcl_isfinite(pj.r) || !pcl_isfinite(pj.g) || !pcl_isfinite(pj.b))
//                        continue;

                    if(NNorm > 0) {
                        // Skip neighbor if it does not have a valid normal
                        if(!pcl_isfinite(pj.normal_x) || !pcl_isfinite(pj.normal_y) || !pcl_isfinite(pj.normal_z))
                            continue;

                        // Take dot product and clamp
                        float dotij = pi.normal_x * pj.normal_x + pi.normal_y * pj.normal_y + pi.normal_z * pj.normal_z;
                        COVIS_ASSERT_MSG(dotij >= -1 - 1e-5f && dotij <= 1 + 1e-5f, "Unnormalized normals in data!");
                        if(dotij < -1)
                            dotij = -1;
                        else if(dotij > 1)
                            dotij = 1;

                        // If negative, either skip or negate
                        if(dotij < 0.0f) {
                            if(_skipNegatives)
                                continue;
                            else
                                dotij = -dotij;
                        }

                        // Store
                        dots.push_back(dotij);
                    }

                    // Get color
                    if(NColor > 0) {
                        // Skip neighbor if it does not have a valid color
                        if(!pcl_isfinite(pj.r) || !pcl_isfinite(pj.g) || !pcl_isfinite(pj.b))
                            continue;
//                    colors.push_back(rgb2hue<PointT>(pj));
//                    reds.push_back(fabsf(float(pi.r)-float(pj.r)) / 255.0);
//                    greens.push_back(fabsf(float(pi.g)-float(pj.g)) / 255.0);
//                    blues.push_back(fabsf(float(pi.b)-float(pj.b)) / 255.0);
                        reds.push_back(float(pj.r) / 255.0);
                        greens.push_back(float(pj.g) / 255.0);
                        blues.push_back(float(pj.b) / 255.0);
                    }

                    // Finally store distance
                    distances.push_back(sqrtf(distsq[j]));
                } // End loop over neighbors (j)
                
                // Now compute histogram (will be set to zero if vectors are empty)
                if(NNorm > 0) {
                    core::rhist2<float>(distances, dots, NDist, NNorm, hdot, 0.0f, _radius, 0.0f, 1.0f);
                    std::copy(hdot, hdot + NDist * NNorm, result->points[i].histogram);
                }

                if(NColor > 0) {
                    core::rhist2<float>(distances, reds, NDist, NColor, hred, 0.0f, _radius, 0.0f, 1.0f);
                    std::copy(hred, hred + NDist * NColor, result->points[i].histogram + NDist * NNorm);
                    core::rhist2<float>(distances, greens, NDist, NColor, hgreen, 0.0f, _radius, 0.0f, 1.0f);
                    std::copy(hgreen, hgreen + NDist * NColor, result->points[i].histogram + NDist * (NNorm + NColor));
                    core::rhist2<float>(distances, blues, NDist, NColor, hblue, 0.0f, _radius, 0.0f, 1.0f);
                    std::copy(hblue, hblue + NDist * NColor,
                              result->points[i].histogram + NDist * (NNorm + 2 * NColor));

//                core::rhist2<float>(distances, colors, NDist, NColor, hcolor, 0.0f, _radius, 0.0f, 1.0f);
//                std::copy(hcolor, hcolor + NDist*NColor, result->points[i].histogram + NDist*NNorm);
                }
            } // End main loop over all input points (i)

            return result;
        }
    }
}

#endif
