// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_DISTANCE_COLOR_NORMAL_HISTOGRAM_H
#define COVIS_FEATURE_DISTANCE_COLOR_NORMAL_HISTOGRAM_H

// Own
#include "feature_3d.h"
#include "../core/macros.h"

// PCL
#include <pcl/point_types.h>

// Register the default type for convenience
POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::Histogram<512>, (float[512], histogram, histogram))
POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::Histogram<384>, (float[384], histogram, histogram))

namespace covis {
    namespace feature {
        /**
         * @class DistanceColorNormalHistogram
         * @ingroup feature
         * @brief Simple point cloud feature based on 2D histograms of distances vs. relative normal angles
         * (dot products) and colors
         * 
         * This feature is an extension of @ref DistanceNormalHistogram, but with added color.
         * If you want to use these features for matching purposes, remember to
         * register the chosen output histogram type. The default type <b>pcl::Histogram<512></b> is already registered,
         * but if you want to use other histogram binnings, registration is needed like this:
         * 
         * POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::Histogram<NDist * (NNorm + NColor)>, (float[NDist * (NNorm + NColor)], histogram, histogram))
         *
         * @tparam PointT input point type, must contain XYZ+normal data
         * @tparam NDist output histogram dimension along the distance dimension
         * @tparam NNorm output histogram dimension along the normal dimension
         * @tparam NNorm output histogram dimension along the color dimension
         * @author Anders Glent Buch
         * @sa @ref DistanceNormalHistogram
         */
        template<typename PointT, int NDist = 8, int NNorm = 16, int NColor = 16>
        class DistanceColorNormalHistogram : public Feature3D<PointT, pcl::Histogram<NDist * (NNorm + 3*NColor)> > {
            using Feature3D<PointT,pcl::Histogram<NDist * (NNorm + 3*NColor)> >::_radius;
            using Feature3D<PointT,pcl::Histogram<NDist * (NNorm + 3*NColor)> >::_surface;
            
            public:
                /// Output histogram of this class
                typedef pcl::Histogram<NDist * (NNorm + 3*NColor)> Histogram;
                
                /// Empty constructor
                DistanceColorNormalHistogram() : _skipNegatives(true) {}

                /// Empty destructor
                virtual ~DistanceColorNormalHistogram() {}
                
                /**
                 * Compute relative distance vs. normal orientation and color histograms
                 * @param cloud input point cloud, must contain XYZ+RGB and normal data
                 * @return histograms (zero for invalid points or points with no neighbors)
                 */
                typename pcl::PointCloud<Histogram>::Ptr compute(typename pcl::PointCloud<PointT>::ConstPtr cloud);
                
                /**
                 * Set skip negative flag
                 * @param skipNegatives skip negative flag
                 */
                inline void setSkipNegatives(bool skipNegatives) {
                    _skipNegatives = skipNegatives;
                }
                
            private:
                /// If set to true, disregard anti-parallel neighbor normals in the histogram building
                bool _skipNegatives;
        };

        /**
         * @ingroup feature
         * 
         * Compute relative distance/normal/color orientation histograms using @ref DistanceNormalHistogram
         * 
         * @param cloud input point cloud, must contain XYZ+RGB and normal data
         * @param radius search radius
         * @param skipNegatives set to true to avoid anti-parallel neighbors in histogram building
         * @return histograms (zero for invalid points or points with no valid neighbors)
         */
        template<typename PointT>
        inline typename pcl::PointCloud<typename DistanceColorNormalHistogram<PointT>::Histogram>::Ptr
        computeDistanceColorNormalHistogram(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                float radius,
                bool skipNegatives = true) {
            DistanceColorNormalHistogram<PointT> nh;
            nh.setRadius(radius);
            nh.setSkipNegatives(skipNegatives);
            
            return nh.compute(cloud);
        }

        /**
         * @ingroup feature
         * 
         * Compute relative distance/normal/color histograms using @ref DistanceNormalHistogram
         * based on an external surface
         * 
         * @param cloud input point cloud, must contain XYZ+RGB and normal data
         * @param surface external search surface, must contain XYZ and normal data
         * @param radius search radius
         * @param skipNegatives set to true to avoid anti-parallel neighbors in histogram building
         * @return histograms (zero for invalid points or points with no valid neighbors)
         */
        template<typename PointT>
        inline typename pcl::PointCloud<typename DistanceColorNormalHistogram<PointT>::Histogram>::Ptr
        computeDistanceColorNormalHistogram(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                typename pcl::PointCloud<PointT>::ConstPtr surface,
                float radius,
                bool skipNegatives = true) {
            DistanceColorNormalHistogram<PointT> nh;
            nh.setRadius(radius);
            nh.setSurface(surface);
            nh.setSkipNegatives(skipNegatives);
            
            return nh.compute(cloud);
        }

        template<typename PointT>
        inline float rgb2hue(const PointT& p) {
            const float r = float(p.r) / 255.0;
            const float g = float(p.g) / 255.0;
            const float b = float(p.b) / 255.0;

            int imax;
            float vmax;
            if(r > g) {
                if(r > b) {
                    imax = 0;
                    vmax = r;
                } else {
                    imax = 2;
                    vmax = b;
                }
            } else {
                if(g > b) {
                    imax = 1;
                    vmax = g;
                } else {
                    imax = 2;
                    vmax = b;
                }
            }

            float result;
            if(imax == 0)
                result =  60.0 * (g - b) / (vmax - MIN(MIN(r,g), MIN(g,b)));
            else if(imax == 1)
                result = 120.0 + 60.0 * (b - r) / (vmax - MIN(MIN(r,g), MIN(g,b)));
            else
                result = 240.0 + 60.0 * (r - g) / (vmax - MIN(MIN(r,g), MIN(g,b)));

            while(result < 0)
                result += 360.0;

            if(result > 360.0)
                return 1.0;
            else
                return result / 360.0;
        }
    }
}

#include "distance_color_normal_histogram_impl.hpp"

#endif
