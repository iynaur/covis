// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_PPF_HISTOGRAM_FULL_IMPL_HPP
#define COVIS_FEATURE_PPF_HISTOGRAM_FULL_IMPL_HPP

// Own
#include "../core/stat.h"
#include "../detect/point_search.h"

namespace covis {
    namespace feature {
        template<typename PointNT, int NDist, int NAngle>
        typename pcl::PointCloud<typename PPFHistogramFull<PointNT, NDist, NAngle>::Histogram>::Ptr
        PPFHistogramFull<PointNT, NDist, NAngle>::compute(typename pcl::PointCloud<PointNT>::ConstPtr cloud) {
            // Check for valid inputs
            COVIS_ASSERT(cloud && !cloud->empty());
            COVIS_ASSERT_MSG(_radius > 0.0f, "Search radius not set!");

            // Index input cloud or external surface and return unsorted neighbors
            detect::PointSearch<PointNT> s(_surface ? _surface : cloud, false);

            // Allocate result
            typename pcl::PointCloud<Histogram>::Ptr result(
                    new typename pcl::PointCloud<Histogram>(cloud->width, cloud->height));

            /*
             * Main loop over all input points
             */
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic)
#endif
            for(size_t i = 0; i < cloud->size(); ++i) {
                // Initialize histogram
                std::fill(result->points[i].histogram, result->points[i].histogram + Histogram::descriptorSize(), 0.0f);

                // Take current source point
                PointNT pi = cloud->points[i];

                // Skip if source is non-finite in XYZ or normal
                if(!pcl::isFinite(pi) || !pcl_isfinite(pi.normal_x) || !pcl_isfinite(pi.normal_y) || !pcl_isfinite(pi.normal_z))
                    continue;

                // Find neighbors
                const core::Correspondence nn = s.radius(pi, _radius);

                // Estimate reference axis using an internal radius of 1/10th of the support radius
                const float minrad = 0.1 * _radius;
                Eigen::Vector3f refaxis;
                refaxis << 0, 0, 0;
                for(size_t j = 0; j < nn.size(); ++j) {
                    // Stay within internal radius
                    if(sqrtf(nn.distance[j]) > minrad)
                        continue;
                    // Get neighbor
                    const PointNT &pj = (_surface ? _surface->points[nn.match[j]] : cloud->points[nn.match[j]]);
                    // Skip NaNs
                    if(!pcl_isfinite(pj.normal_x) || !pcl_isfinite(pj.normal_y) || !pcl_isfinite(pj.normal_z))
                        continue;
                    refaxis(0) += pj.normal_x;
                    refaxis(1) += pj.normal_y;
                    refaxis(2) += pj.normal_z;
                }

                // Normalize
                const float refnorm = refaxis.norm();
                if(refnorm > 1e-5f) { // If no neighbors found, we get a zero here
                    refaxis /= refnorm;
                    pi.normal_x = refaxis(0);
                    pi.normal_y = refaxis(1);
                    pi.normal_z = refaxis(2);
                }

                // Relations for 2D histogramming
                std::vector<float> delta, alpha, beta, gamma;

                // Loop over neighbors and compute relations
                for(size_t j = 0; j < nn.size(); ++j) {
                    // Take neighbor
                    const PointNT& pj = (_surface ? _surface->points[nn.match[j]] : cloud->points[nn.match[j]]);

                    // Skip neighbor if it is the source point
                    if(&pj == &pi)
                        continue;

                    // Skip neighbor if it does not have a valid normal
                    if(!pcl_isfinite(pj.normal_x) || !pcl_isfinite(pj.normal_y) || !pcl_isfinite(pj.normal_z))
                        continue;

                    // Compute point distance (delta), skip if too small
                    const float deltaij = sqrtf(nn.distance[j]);
                    if(deltaij < 1e-5f)
                        continue;

                    // If skip negatives enabled, avoid neighbors with opposing normals
                    const float alphaij = pi.normal_x * pj.normal_x + pi.normal_y * pj.normal_y + pi.normal_z * pj.normal_z;
                    COVIS_ASSERT_MSG(alphaij >= -1 - 1e-5f && alphaij <= 1 + 1e-5f, "Unnormalized normals in data!");
                    if(_skipNegatives) {
                        // If negative, skip
                        if(alphaij < 0.0f)
                            continue;
                    }

                    // Compute normalized direction vector used below
                    const float dx = (pj.x - pi.x) / deltaij;
                    const float dy = (pj.y - pi.y) / deltaij;
                    const float dz = (pj.z - pi.z) / deltaij;

                    // Compute angle cosine between direction vector and current normal
                    const float betaij = pi.normal_x * dx + pi.normal_y * dy + pi.normal_z * dz;
                    COVIS_ASSERT_MSG(betaij >= -1 - 1e-5f && betaij <= 1 + 1e-5f, "Unnormalized normals in data!");

                    // Compute angle cosine between direction vector and neighbor normal
                    const float gammaij = pj.normal_x * dx + pj.normal_y * dy + pj.normal_z * dz;
                    COVIS_ASSERT_MSG(gammaij >= -1 - 1e-5f && gammaij <= 1 + 1e-5f, "Unnormalized normals in data!");

                    // Store relations
                    delta.push_back(deltaij);
                    alpha.push_back(alphaij);
                    beta.push_back(betaij);
                    gamma.push_back(gammaij);

                } // End loop over neighbors (j)

                // Put into histograms
                core::rhist2<float>(delta, alpha, NDist, NAngle, result->points[i].histogram,
                                    0, _radius, (_skipNegatives ? 0 : -1), 1);
                core::rhist2<float>(delta, beta, NDist, NAngle, result->points[i].histogram+NDist*NAngle,
                                    0, _radius, -1, 1);
                core::rhist2<float>(delta, gamma, NDist, NAngle, result->points[i].histogram+2*NDist*NAngle,
                                    0, _radius, -1, 1);
            } // End main loop over all input points (i)

            return result;
        }
    }
}

#endif
