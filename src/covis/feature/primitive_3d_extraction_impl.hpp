// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_PRIMITIVE_3D_EXTRACTION_IMPL_HPP
#define COVIS_FEATURE_PRIMITIVE_3D_EXTRACTION_IMPL_HPP

#include "primitive_3d_extraction.h"
#include "texlet_3d_extraction.h"
#include "texlet_2d_extraction.h"

//OpenCV
#include <opencv2/core/core.hpp>

//PLC
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/console/print.h>


namespace covis {
    namespace feature {

        template<typename PointT, class Prim2D, typename Prim3D>
        pcl::PointCloud<pcl::Normal>::Ptr Primitive3DExtraction<PointT, Prim2D, Prim3D>::computeNormals(
                const typename pcl::PointCloud<PointT>::Ptr &cloud, std::vector<int> &indices){

                //eliminate depth = 0 points
                for (size_t i = 0; i < cloud->points.size(); ++i) {
                    PointT pp = cloud->at(i);
                    if (pp.z != 0) {
                        indices.push_back(i);
                    }
                }

                // Create the normal estimation class, and pass the input dataset to it
                pcl::NormalEstimationOMP<PointT, pcl::Normal> ne;
                ne.setInputCloud (cloud);

                //to keep only relavent points from point cloud
                boost::shared_ptr<std::vector<int> > indicesptr (new std::vector<int> (indices));
                ne.setIndices (indicesptr);

                typename pcl::search::OrganizedNeighbor<PointT>::Ptr tree(new pcl::search::OrganizedNeighbor<PointT>());
                ne.setSearchMethod (tree);

                // Use all neighbors in a sphere of radius 1cm
                ne.setRadiusSearch (0.01);

                pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);

                // Compute the features
                ne.compute(*cloud_normals);

                return cloud_normals;
        }

        template<typename PointT, class Prim2D, typename Prim3D>
        std::vector<Prim3D> Primitive3DExtraction<PointT, Prim2D, Prim3D>::extractECVPrimitives3D
        (std::vector<Prim2D> &primitives2d, const typename pcl::PointCloud<PointT>::Ptr &cloud) {

                //to keep the end result
                std::vector<Prim3D> primitives3d;


                //check if point cloud is organized => height > 1
                if (!cloud->isOrganized()) {
                    pcl::console::print_highlight("Point cloud is not organized!");
                }

                //to have point cloud organized, create a copy of point cloud with the normals
                pcl::PointCloud<pcl::PointNormal>::Ptr cloudn (new pcl::PointCloud<pcl::PointNormal>);
                pcl::copyPointCloud(*cloud, *cloudn);

                std::vector<int> indices;
                pcl::PointCloud<pcl::Normal>::Ptr cloud_normals = computeNormals(cloud, indices);

                //copy the computed normals to copied point cloud
                for (size_t i = 0; i < indices.size(); ++i){
                    pcl::Normal n = (*cloud_normals)[i];
                    (*cloudn)[indices[i]].normal_x = n.normal_x;
                    (*cloudn)[indices[i]].normal_y = n.normal_y;
                    (*cloudn)[indices[i]].normal_z = n.normal_z;
                }

                for (typename std::vector<Prim2D>::iterator it = primitives2d.begin();
                        it != primitives2d.end(); ++it){

                    Prim2D t = *it;

                    const int row = t.y;
                    const int col = t.x;

                    pcl::PointNormal p = cloudn->at(col, row);

                    if (p.z != 0)
                    {
                        Prim3D primitive3d(t);
                        primitive3d.x = p.x; primitive3d.y = p.y; primitive3d.z = p.z;
                        primitive3d.normal_x = p.normal_x; primitive3d.normal_y = p.normal_y;
                        primitive3d.normal_z = p.normal_z;
                        primitive3d.r = t.r; primitive3d.g = t.g; primitive3d.b = t.b;
                        primitives3d.push_back(primitive3d);
                    }
                }

                return primitives3d;
        }


        template<typename PointT, class Prim2D, typename Prim3D>
        pcl::PointCloud<Prim3D> Primitive3DExtraction<PointT, Prim2D, Prim3D>::extractECVPrimitives3DtoPointCloud
        (std::vector<Prim2D> &primitives2d, const typename pcl::PointCloud<PointT>::Ptr &cloud) {

                pcl::PointCloud<Prim3D> cl;

                //check if point cloud is organized => height > 1
                if (!cloud->isOrganized()) {
                    pcl::console::print_highlight("Point cloud is not organized!");
                }

                //to have point cloud organized, create a copy of point cloud with the normals
                /**
                 * @todo When this is performed with either @ref covis::core::LineSegment3D or
                 * @ref covis::core::Texlet3D as the @b Prim3D template, warnings are issued because @b memcpy() is
                 * being performed on dynamic classes (because they inherit from @ref covis::core::Primitive3D).
                 */
                pcl::copyPointCloud(*cloud, cl);

                std::vector<int> indices;
                pcl::PointCloud<pcl::Normal>::Ptr cloud_normals = computeNormals(cloud, indices);

                //copy the computed normals to copied point cloud
                for (size_t i = 0; i < indices.size(); ++i){
                    pcl::Normal n = (*cloud_normals)[i];
                    (cl)[indices[i]].normal_x = n.normal_x;
                    (cl)[indices[i]].normal_y = n.normal_y;
                    (cl)[indices[i]].normal_z = n.normal_z;
                }

                // create an image
                cv::Mat_<cv::Vec3f> img(cl.width, cl.height);
                for (int i = 0; i < img.rows; i++)
                    for (int j = 0; j < img.cols; j++){
                        img[i][j][0] = -1;
                        img[i][j][1] = -1;
                        img[i][j][2] = -1;
                    }
                for (typename std::vector<Prim2D>::iterator it = primitives2d.begin(); it != primitives2d.end(); ++it) {

                    Prim2D t = *it;
                    img[t.x][t.y][0] = t.r;
                    img[t.x][t.y][1] = t.g;
                    img[t.x][t.y][2] = t.b;
                }


                for (size_t x = 0; x < cl.width; x++) {
                    for (size_t y = 0; y < cl.height; y++) {
                        Prim3D p = cl.at(x, y);
                        if (img[x][y][0] != -1){
                            p.r = img[x][y][0]; p.g = img[x][y][1]; p.b = img[x][y][2];
                        }
                        else {
                            p.r = 0; p.g = 0; p.b = 0;
                            p.x = 0; p.y = 0; p.z = 0;
                            p.r = 0; p.g = 0; p.b = 0;
                            p.normal_x = 0; p.normal_y = 0; p.normal_z = 0;
                        }
                        cl.at(x, y) = p;
                    }
                }

                return cl;
        }
    }
}

#endif /* COVIS_FEATURE_PRIMITIVE_3D_EXTRACTION_IMPL_HPP */
