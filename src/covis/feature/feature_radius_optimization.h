// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_FEATURE_RADIUS_OPTIMIZATION_H
#define COVIS_FEATURE_FEATURE_RADIUS_OPTIMIZATION_H

// Own
#include "feature_base.h"

// PCL
#include <pcl/point_cloud.h>

namespace covis {
    namespace feature {
        /**
         * @class FeatureRadiusOptimization
         * @ingroup feature
         * @brief Determine the optimal support radius for local features
         *
         * The supported features are saved in the variable @ref FeatureNames.
         *
         * @tparam PointNT input point type, must contain XYZ+normal data
         * @author Anders Glent Buch
         */
        template<typename PointNT>
        class FeatureRadiusOptimization : public FeatureBase {
            public:
                /// Point cloud
                typedef typename pcl::PointCloud<PointNT> CloudT;

                /// Empty constructor
                FeatureRadiusOptimization() {
                    _featurePoints = 100;
                    _radiusStart = 0;
                    _radiusInc = 0;
                    _radiusStop = 0;
                    _pcaVariation = 0.95;
                    _verbose = false;
                }

                /**
                 * Constructor: initialize input point cloud
                 * @param cloud input point cloud
                 * @param verbose print status messages
                 */
                FeatureRadiusOptimization(typename CloudT::ConstPtr cloud, bool verbose = false) :
                        _cloud(cloud),
                        _verbose(verbose){
                    _featurePoints = 100;
                    _radiusStart = 0;
                    _radiusInc = 0;
                    _radiusStop = 0;
                    _pcaVariation = 0.95;
                }

                /**
                 * Constructor: initialize input point cloud
                 * @param cloud input point cloud
                 * @param featureName named feature to optimize
                 * @param verbose print status messages
                 */
                FeatureRadiusOptimization(typename CloudT::ConstPtr cloud,
                                          const std::string& featureName,
                                          bool verbose = false) :
                        _cloud(cloud),
                        _featureName(featureName),
                        _verbose(verbose) {
                    _featurePoints = 100;
                    _radiusStart = 0;
                    _radiusInc = 0;
                    _radiusStop = 0;
                    _pcaVariation = 0.95;
                }

                /// Empty destructor
                virtual ~FeatureRadiusOptimization() {}

                /**
                 * Compute optimal radius
                 * @return optimal radius
                 */
                float compute();

                /**
                 * Set input point cloud
                 * @param cloud input point cloud
                 */
                inline void setCloud(typename CloudT::ConstPtr cloud) {
                    _cloud = cloud;
                }

                /**
                 * Set named feature to optimize
                 * @param featureName named feature to optimize
                 */
                inline void setFeature(const std::string& featureName) {
                    _featureName = featureName;
                }

                /**
                 * Set number of feature points to use
                 * @param featurePoints number of feature points to use
                 */
                inline void setFeaturePoints(size_t featurePoints) {
                    _featurePoints = featurePoints;
                }

                /**
                 * Set the radius range to test for - set all these to zero to use an automatic range of radii
                 * @param radiusStart smallest radius
                 * @param radiusInc radius increment
                 * @param radiusStop largest radius
                 */
                inline void setRadiusInterval(float radiusStart = 0, float radiusInc = 0, float radiusStop = 0) {
                    _radiusStart = radiusStart;
                    _radiusInc = radiusInc;
                    _radiusStop = radiusStop;
                }

                /**
                 * Set the PCA energy (a number from 0 to 1) to use for finding optimal radius
                 * @param pcaVariation PCA variation
                 */
                inline void setPcaVariation(float pcaVariation) {
                    _pcaVariation = pcaVariation;
                }

                /**
                 * Set verbosity
                 * @param verbose verbosity
                 */
                inline void setVerbose(bool verbose) {
                    _verbose = verbose;
                }

            private:
                /// Input point cloud
                typename pcl::PointCloud<PointNT>::ConstPtr _cloud;

                /// Which named feature to optimize - supported features are here: @ref FeatureNames
                std::string _featureName;

                /// Number of feature points to use
                size_t _featurePoints;

                /// Start radius
                float _radiusStart;

                /// Radius increase
                float _radiusInc;

                /// Stop radius
                float _radiusStop;

                /// PCA energy to use
                float _pcaVariation;

                /// Set to true to print status messages
                bool _verbose;
        };

        /**
         * @ingroup feature
         * Optimize the local support radius for a named feature using an object model.
         * To use an automatic range of test radii, set all three radius parameters below to zero.
         * @param cloud input point cloud
         * @param featureName named feature to optimize radius for
         * @param featurePoints number of feature points to use
         * @param radiusStart start radius
         * @param radiusInc radius increment
         * @param radiusStop stop radius
         * @param pcaVariation PCA energy (a number from 0 to 1) to use for finding optimal radius
         * @param verbose verbosity
         * @tparam PointNT point type with normal, required for estimating local features
         * @return
         */
        template<typename PointNT>
        float optimizeFeatureRadius(typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                                    const std::string& featureName,
                                    size_t featurePoints = 100,
                                    float radiusStart = 0,
                                    float radiusInc = 0,
                                    float radiusStop = 0,
                                    float pcaVariation = 0.95,
                                    bool verbose = false) {
            FeatureRadiusOptimization<PointNT> fradopt(cloud, featureName, verbose);
            fradopt.setFeaturePoints(featurePoints);
            fradopt.setRadiusInterval(radiusStart, radiusInc, radiusStop);
            fradopt.setPcaVariation(pcaVariation);

            return fradopt.compute();
        }
    }
}

#ifndef COVIS_PRECOMPILE
#include "feature_radius_optimization_impl.hpp"
#endif

#endif
