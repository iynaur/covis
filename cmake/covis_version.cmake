# Helper script for setting up CoViS version info
#
# The following variables are set
#   COVIS_VERSION
#   COVIS_REVISION
#


##
# CoViS version
##
set(COVIS_VERSION 1.0.0)
string(REPLACE "." ";" COVIS_VERSION_LIST ${COVIS_VERSION})
list(GET COVIS_VERSION_LIST 0 COVIS_VERSION_MAJOR)
list(GET COVIS_VERSION_LIST 1 COVIS_VERSION_MINOR)
list(GET COVIS_VERSION_LIST 2 COVIS_VERSION_PATCH)
unset(COVIS_VERSION_LIST)
math(EXPR COVIS_VERSION_CMP "${COVIS_VERSION_MAJOR}*10000 + ${COVIS_VERSION_MINOR}*100 + ${COVIS_VERSION_PATCH}")

##
# CoViS SVN revision (-1 if SVN not found)
##
set(COVIS_REVISION -1)
find_package(Subversion QUIET)
if(Subversion_FOUND)
  # Check if CMAKE_SOURCE_DIR is an SVN repo - taken from FindSubversion.cmake
  execute_process(COMMAND ${Subversion_SVN_EXECUTABLE} info ${CMAKE_SOURCE_DIR}
                  OUTPUT_VARIABLE dummy
                  RESULT_VARIABLE svn_info_result
                  ERROR_QUIET)
  unset(dummy)

  # Return code 0 means success
  if(svn_info_result EQUAL 0)
    # Get revision
    Subversion_WC_INFO(${CMAKE_SOURCE_DIR} COVIS)
    set(COVIS_REVISION ${COVIS_WC_REVISION})
  endif(svn_info_result EQUAL 0)
  unset(svn_info_result)
endif(Subversion_FOUND)

# If revision is still -1, we found no SVN repo, and we try Git
if(COVIS_REVISION EQUAL -1)
  find_package(Git)
  if(GIT_FOUND)
    # This command just counts the number of commits
    # Taken from: http://stackoverflow.com/questions/4120001
    execute_process(COMMAND ${GIT_EXECUTABLE} rev-list HEAD
                    COMMAND wc -l
                    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
                    OUTPUT_VARIABLE revision)
    string(REPLACE "\n" "" revision "${revision}")
    if(revision)
      set(COVIS_REVISION ${revision})
    endif(revision)
    unset(revision)
  endif(GIT_FOUND)
endif(COVIS_REVISION EQUAL -1)
